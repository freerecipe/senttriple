/*
   XML Term Reader for 386-PROLOG - Brian D Steel - 16 Apr 08 / 09 Sep 08
   ======================================================================

   This file defines a prototype XML term reader, making use of the
   provisional predicates, xml_token/2, xml_entity/4 and xml_reference/3
   (see XML_DEMO.PL for explanations).

   xml_read( Ents, Room, Term )
   ----------------------------
      Ents - a list of (Atom,`String`) pairs naming entities and substitutions
      Room - a flag indicating whether or not to include white space:
             0 - ignore (discard) white space
             1 - return (include) white space
      Term - a variable to return the XML file

   For example, given the file, "foo.xml":

      <foo bar="sux">
         hello &data; world
      </foo>

   consider the following calls:

      | ?- see( 'foo.xml' ), xml_read( [(data,`THERE`)], 0, Term ), seen.
      Term = [foo([(bar,`sux`)],[`hello THERE world`])]

      | ?- see( 'foo.xml' ), xml_read( [(data,`THERE`)], 1, Term ), seen.
      Term = [foo([(bar,`sux`)],[`~M~J   hello THERE world~M~J`])]
*/

:- ensure_loaded( xml_toks ).

%%% XML TERM READER %%%

xml_read( Ents, Room, Term ) :-
   xml_reader( Ents, Room, true, [], Item, [] ),
   (  Item = []
   -> xml_read( Ents, Room, Term )
   ;  Term = Item
   ).

xml_reader( Ents, Room, Mode, Hist, Term, Tail ) :-
   xml_token( Item, Flag ),

   (  Flag = 1
   -> Item = Name(Args),
      Term = [Name(Pars,List)|More],
      xml_arguments( Args, Ents, Pars ),
      xml_reader( Ents, Room, Mode, [Name|Hist], List, [] ),

      (  Hist = [],
         Mode
      -> Tail = More
      ;  xml_reader( Ents, Room, Mode, Hist, More, Tail )
      )

   ;  Flag = 0
   -> (  Item = ?(Name(Args))
      -> Term = ?(Name,Args)

      ;  Item = !(Name(Data))
      -> Term = !(Name,Data)

      ;  Item = Name(Args)
      -> Term = [Name(Pars,[])|More],
         xml_arguments( Args, Ents, Pars )

      ;  type( Item, 4 )
      -> xml_entity( Ents, Item, Exps, Hits ),
         (  Hits = 0
         -> xml_reference( Room, Exps, Norm ),
            (  Norm = ``
            -> Term = More
            ;  Term = [Norm|More]
            )
         ;  xml_reader( Ents, Room, eof, [], Term, More ) <~ Exps
         )
      ),

      (  Hist = [],
         Mode
      -> Tail = More
      ;  xml_reader( Ents, Room, Mode, Hist, More, Tail )
      )

   ;  Flag = -1
   -> (  Item = Name([])
      -> (  Hist = [Name|_]
         -> Term = Tail
         ;  Hist = []
         -> Term = Name
         ;  throw( 42, xml_reader(Ents,Room,Mode,Hist,Term) )
         )
      ;  Item = []
      -> fail
      ;  throw( 42, xml_reader(Ents,Room,Mode,Hist,Term) )
      )
   ).

xml_arguments( [], _, [] ).

xml_arguments( [(Name,Args)|Prev], Ents, [(Name,Pars)|Next] ) :-
   xml_entity( Ents, Args, Exps, _ ),
   xml_reference( 0, Exps, Pars ),
   xml_arguments( Prev, Ents, Next ).
