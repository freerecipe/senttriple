
% sub_systems( SystemName, Configuration)
% this predicate contains information about the system confgiguration
% this file will be used to load and init the system

sub_systems( [(system_name, `Parsi` )],
	[config, % configuration files
	document_manipulation, % document manipulation such as text to section etc
	extract_triples, % triple extraction
	html_print, % html print predicates
	index_repository_interface, % ODBC connection to index_repository
	index_repository_predicates, % data repository predictes
	log_win, % logging window	
%	main_predicates, % main predicate, intended to be used by end user i commented this becuase might be different from one application to another
	matrixes, % matrixes are inverse indexes for dependencies parses
%	misc,
	parsers, % parsers
	penn_parse_analysis, % Penn Parse analyzer / parser
	pretty_print, % pretty print predicates
	sentence_chunking, % sentence chunking predicates
	system_files, % common system files
	tcpip,	% tcpip wrapper
	xml % xml related predicates
	]).		

% this predicate gets the name of a desired system and use the sub_system predicate to load and configure the asked system
load_system(SystemName):-
	nl, write( `log: loading sub systems...` ),
	nl, write( `-------`),
	sub_systems( [(system_name, SystemName )], SubSystems ),
	load_sub_system( SubSystems ),
	nl, write( `-------`),
	nl, write( `log: all necessary files are loaded` ).
	

load_sub_system( [] ).
load_sub_system( [SubSystem|RestOfSubSystems] ):-
	chdir( Current ),
	chdir( SubSystem ),
	cat( ['load_',SubSystem], LoadFile, _ ),
	ensure_loaded( LoadFile ),
	call( LoadFile ),
	chdir( Current ),
	load_sub_system( RestOfSubSystems ).


	


