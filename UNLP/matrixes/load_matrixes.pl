% load files necessary matrixes

load_matrixes :-
	nl, write( `log: loading inverse index (matrix) predicates ...`  ),
	chdir( Current ),
	chdir( 'lexeme_matrix' ),
	ensure_loaded( load_lexeme_matrix ),
	load_lexeme_matrix,
	
	chdir( Current ),
	chdir( phrase_matrix ),
	ensure_loaded( load_phrase_matrix ),
	load_phrase_matrix,

	chdir( Current ),

	nl, write( `done!` ).



	 

