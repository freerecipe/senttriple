% htmnl_print_phrase_matrix_to_file( +SentenceID, -FileName )
% this predicate prints matrix of phrases ( already asserted in the memory) into a file 
html_print_phrase_matrix_to_file( SentenceID, FileName ):-
	cat( ['phrase', SentenceID,'matrix.html'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),

	pretty_print_sentence( SentenceID, LexemeList, SentenceString ), % just for print purpose

	write( `<html>` ),
	write(`<head><title>` ),
	write( `Sentence Phrase Matrix`), 
	write(`</title></head>` ),
	write( `<body style="background-color:white;">` ),
	write( `<p style="font-size:10px;font-family:Constantia;color:black">` ),
	write( `<b>Input sentence: </b>` ), write( SentenceString ),
	write( `</p>` ),
	write( `<table border="1" style="font-size:9px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),

	findall( A, sentence_chunk(SI, A, B, C, D), SentenceChunks ),
	length( SentenceChunks, MatrixSize ),

	phrase_write_position_line_header_for_matrix( MatrixSize ),	% this writes indexes which are numbers from one upto Phrase length
	phrase_write_first_line_header_for_matrix( SentenceID, MatrixSize ),	% this writes the phrases themselves

	dynamic( hrow_counter/1 ),
	assert( hrow_counter( 1 ) ),

	forall( ( retract( hrow_counter( Row ) ), Row =< MatrixSize ),
	(

		dynamic( hcolumn_counter/1 ),
		assert( hcolumn_counter( 1 ) ),
		write( `<tr>` ),
	
		forall( ( retract( hcolumn_counter(Column) ), Column =< MatrixSize ),
			(
				NewColumn is Column + 1,
				assert( hcolumn_counter( NewColumn ) ),

				% write cell
				phrase_print_matrix_header(SentenceID, Row, Column ),	
				phrase_pretty_html_give_dep_cell( SentenceID, Row, Column )
%				write( `<td>` ), write(  String ), write( `</td>` )

			)),
		write( `</tr>` ),
		NewRow is Row + 1,
		assert( hrow_counter( NewRow ) )
	)),

	write( `<table>` ),
	write( `<html>` ),
	dynamic( hrow_counter/1 ),
	dynamic( hcolumn_counter/1 ),
	fclose( html ).



phrase_print_matrix_header(SentenceID, Position , 1):-
	sentence_chunk(SentenceID, [Position], LexemeList, Type, _),
	write( `<th>` ),  write( Position ), write( `</th>` ), 
	write( `<th>` ),  write_lexeme_list( LexemeList ), write( ` `), write( Type ), write( `</th>` ),!.

% otherwise
phrase_print_matrix_header( SenteceID, Row, Col ):- !.
%	Row \= 1,
%	Col \= 1, !.*/
%	!.





phrase_write_position_line_header_for_matrix( Length ):-
	write( `<tr>` ),
	write( `<th style="font-size:5px;font-family:calibri;color:#003300">` ), write( `<b>Phrase Dependencies</b>` ), write( `</th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:red">Regent Phrase Position</th>` ),
	dynamic( xc/1 ), 
	assert( xc(1) ),
	forall( ( retract( xc(Count) ), Count =< Length ), 
		(
		write( `<th>` ), write( Count ), write( `</th>` ),
		NCount is Count + 1,
		assert( xc(NCount) )
		)
		),
	dynamic( xc/1 ),
	write( `</tr>` ).

phrase_write_first_line_header_for_matrix( SentenceID, Length ):-
	write( `<tr>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:red">` ), write( `Govenor Phrase Position` ), write( `</th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:#003300">Phrase/Freq</th>` ),

	dynamic( xc/1 ), 
	assert( xc(1) ),

	forall( ( retract( xc(Count ) ), Count =< Length ), 
		(
		phrase_print_first_row__header(SentenceID, Count ),
		NEWCount is Count + 1,
		assert( xc(NEWCount) ) 
		)
		),
	dynamic( xc/1 ),
	write( `</tr>` ).
	
phrase_print_first_row__header(SentenceID, Position ):-
	sentence_chunk( SentenceID, [Position], LexemeList, PhraseType, _ ),
	write( `<th>` ),  
	write_lexeme_list( LexemeList ), write( ` ` ), write( PhraseType ),
	write( `</th>` ),
	!.


phrase_pretty_html_give_dep_cell( SentenceID, Row, Column ):-
	write( `<td>` ),
	forall( DepType1^pmc( stanford, SentenceID, [Row], [Column], DepType1 ),
	 	( write(`<li type="square">`), write(  DepType1 ), write(`</li>`) )), 

	forall( DepType2^pmc( malt, SentenceID, [Row], [Column], DepType2 ),
		( write(`<li type="circle">`), write(  DepType2 ), write(`</li>`) )),

	forall( DepType3^pmc( biolg, SentenceID, [Row], [Column], DepType3 ),
		(write(`<li type="disk">`), write( DepType3 ), write(`</li>`) )), 

	 write( `</td>` ).










































