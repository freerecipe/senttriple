
% make_matrix_of_sentence_phrase( +SentenceID )
% This predicate makes an Sparse Matrix representation of dependencies between phrases.
% The aim is to have an iinverse index for Exocentric Dependencies, rather than Endro ones.

make_matrix_of_sentence_phrase( SentenceID ):-
	dynamic( sentence_chunk/5 ),
	make_raw_sentence_phrases( SentenceID ), % this will assert sentence_chunk(A, B, C, D) into memory
	findall( A, sentence_chunk(SI, A, B, C, D), SentenceChunks ),
	length( SentenceChunks, MatrixSize ),

	nl, write( `the size of matrix is ` ), write(MatrixSize),

	dynamic( pmc/5 ),  % pmc stand for phrase matrix cell

	dynamic( row/1 ), 
	assert( row(1) ),

	repeat, 
		retract( row(Row) ),
		NewRow is Row + 1,
		assert( row(NewRow) ),
		dynamic( column/1 ),
		assert( column(1) ), 

		(
		repeat,
			retract( column(Column) ),
			NewColumn is Column + 1,
			assert( column(NewColumn) ),


			% nl, write( Row - Column ), 
			make_phrase_matrix_cell_of( SI, Row, Column ),

			NewColumn > MatrixSize   -> true 
		),



	Row  = MatrixSize -> nl, write( done ).
	

% make_phrase_matrix_cell_for( +Parser, +SentenceID, +PhraseRowIndex, +PhraseColumnIndex )
% this predicate make a cell of matrix for a given Row and Column Index for the given Parser
% in the case that Row = Index the cell will be empty
% in the case that no relation is found for the given row and column, no cell will be asserted ( Sparse MATRIX )

make_phrase_matrix_cell_of( SI, Row, Column ):-
	Row = Column 
	-> !
	;
	sentence_chunk(SI, [Row], B1, C1, D1),
	sentence_chunk(SI, [Column], B2, C2, D2),

	give_dependency_list_between_phrases_all( SI, [Row], [Column], D1,D2 ).


% Malt Parser
give_dependency_list_between_phrases_all( SI, [Row], [Column], [],RegList ).
give_dependency_list_between_phrases_all( SI, [Row], [Column],[Gov|GovList],RegList ):-
	give_dependency_between_lexeme_and_lexlist_all( SI, [Row], [Column],Gov, RegList ),
	give_dependency_list_between_phrases_all( SI, [Row], [Column],GovList,RegList ).


give_dependency_between_lexeme_and_lexlist_all( SI, _, _,GovPosition, [] ):-
	 !.
give_dependency_between_lexeme_and_lexlist_all( SI, [Row], [Column], GovPosition, [RegentPosition|RegentList] ):-
	( aux_dependency_malt( SI, GovPosition, RegentPosition, DepType1 )
	->	assert(pmc(malt, SI, [Row], [Column], DepType1)); true ),

	( aux_dependency_stanford( SI, GovPosition, RegentPosition, DepType2 )
	->	assert(pmc(stanford, SI, [Row], [Column], DepType2)); true ),

	( aux_dependency_biolg( SI, GovPosition, RegentPosition, DepType3 )
	->	assert(pmc(biolg, SI, [Row], [Column], DepType3)); true ),

	give_dependency_between_lexeme_and_lexlist_all( SI, [Row], [Column], GovPosition, RegentList ).



aux_dependency_malt( SI, GovPosition, RegentPosition, DepType ):-
	index_sentence_malt_dependencies( SI, DepID, GovPosition, RegentPosition),
	index_malt_dependency_lexemes( DepID, DepType, _, _, _),!.  


aux_dependency_stanford( SI, GovPosition, RegentPosition, DepType ):-
	index_sentence_dependencies( SI, DepID, GovPosition, RegentPosition),
	index_dependency_lexemes( DepID, DepType, _, _, _),!.   


aux_dependency_biolg( SI, GovPosition, RegentPosition, DepType ):-
	index_sentence_links( SI, DepID, GovPosition, RegentPosition),
	index_linked_lexemes( DepID, DepType, _, _, _),!.    


























 
