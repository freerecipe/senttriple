/* 

create_dependency_matrix( SentenceID ):-
	index_sentence( SentenceID, _, Length, _ ),

	dynamic( row_counter/1 ),
	assert( row_counter( 1 ) ),
	

	forall( ( retract( row_counter( Row ) ), Row =< Length ),
	(
		dynamic( column_counter/1 ),
		assert( column_counter( 1 ) ),
		forall( ( retract( column_counter(Column) ), Column =< Length ),
			(
				nl, write( Row ), tab(5), write( Column ),
				NewColumn is Column + 1,
				assert( column_counter( NewColumn ) )
			)),
	NewRow is Row + 1,
	assert( row_counter( NewRow ) )
	
	)).		

code structure for making the matrix
		*/

%	


% create_dependency_matrix( +SentenceID )
% this predicate gets a sentence id and it asserts all the dependencies for a sentence into memory
% for the words with no dependencies the cell will contain na/0 --> the number after dependency type shows the frequency of number

create_dependency_matrix( stanford, SentenceID ):-
	index_sentence( SentenceID, _, Length, _ ),
	dynamic( dmtx_cell/5 ),
	dynamic( row_counter/1 ),
	assert( row_counter( 1 ) ),

	
	forall( ( retract( row_counter( Row ) ), Row =< Length ),
	(
		dynamic( column_counter/1 ),
		assert( column_counter( 1 ) ),
		forall( ( retract( column_counter(Column) ), Column =< Length ),
			(
%				nl, write( Row ), tab(5), write( Column ),
				NewColumn is Column + 1,
				assert( column_counter( NewColumn ) ),
			% Here asserting the data comes into play
			give_dep_cell( SentenceID, cell(Row, Column, (DepType, Freq ) ) ),
			assert( dmtx_cell( stanford, Row, Column, DepType, Freq ) )	

			)),
	NewRow is Row + 1,
	assert( row_counter( NewRow ) )
	
	)),
	( write( SentenceID ), write( ` is matricized into memory!!!! ` ) ) ~> MatrixLog,
	log_to_window( MatrixLog ).		


% findall( (Position, LexemeID), index_sentence_lexeme( SentenceID, LexemeID, Position), Row ).


give_dep_cell( SentenceID, cell(IndexRow, IndexColumn, (DepType, Freq )) ):-
	( index_sentence_dependencies( SentenceID, DepLexID, IndexRow, IndexColumn) 
	->	index_dependency_lexemes( DepLexID, DepType, _, _, Freq );
	DepType = na,Freq = 0, ! ).


create_dependency_matrix( malt, SentenceID ):-
	index_sentence( SentenceID, _, Length, _ ),
%	dynamic( dmtx_cell/5 ),
	dynamic( row_counter/1 ),
	assert( row_counter( 1 ) ),

	
	forall( ( retract( row_counter( Row ) ), Row =< Length ),
	(
		dynamic( column_counter/1 ),
		assert( column_counter( 1 ) ),
		forall( ( retract( column_counter(Column) ), Column =< Length ),
			(
%				nl, write( Row ), tab(5), write( Column ),
				NewColumn is Column + 1,
				assert( column_counter( NewColumn ) ),
			% Here asserting the data comes into play
			give_dep_cell( malt, SentenceID, cell(Row, Column, (DepType, Freq ) ) ),
			assert( dmtx_cell( malt, Row, Column, DepType, Freq ) )	

			)),
	NewRow is Row + 1,
	assert( row_counter( NewRow ) )
	
	)),
	( write( SentenceID ), write( ` malt parse is matricized into memory!!!! ` ) ) ~> MatrixLog,
	log_to_window( MatrixLog ).		


% findall( (Position, LexemeID), index_sentence_lexeme( SentenceID, LexemeID, Position), Row ).


give_dep_cell( malt, SentenceID, cell(IndexRow, IndexColumn, (DepType, Freq )) ):-
	( index_sentence_malt_dependencies( SentenceID, DepLexID, IndexRow, IndexColumn) 
	->	index_malt_dependency_lexemes( DepLexID, DepType, _, _, Freq );
	DepType = na,Freq = 0, ! ).





create_dependency_matrix( biolg, SentenceID ):-
	index_sentence( SentenceID, _, Length, _ ),
%	dynamic( dmtx_cell/5 ),
	dynamic( row_counter/1 ),
	assert( row_counter( 1 ) ),

	
	forall( ( retract( row_counter( Row ) ), Row =< Length ),
	(
		dynamic( column_counter/1 ),
		assert( column_counter( 1 ) ),
		forall( ( retract( column_counter(Column) ), Column =< Length ),
			(
%				nl, write( Row ), tab(5), write( Column ),
				NewColumn is Column + 1,
				assert( column_counter( NewColumn ) ),
			% Here asserting the data comes into play
			give_dep_cell( biolg, SentenceID, cell(Row, Column, (DepType, Freq ) ) ),
			assert( dmtx_cell( biolg, Row, Column, DepType, Freq ) )	

			)),
	NewRow is Row + 1,
	assert( row_counter( NewRow ) )
	
	)),
	( write( SentenceID ), write( ` biolg parse is matricized into memory!!!! ` ) ) ~> MatrixLog,
	log_to_window( MatrixLog ).		


% findall( (Position, LexemeID), index_sentence_lexeme( SentenceID, LexemeID, Position), Row ).


give_dep_cell( biolg, SentenceID, cell(IndexRow, IndexColumn, (DepType, Freq )) ):-
	( index_sentence_links( SentenceID, DepLexID, IndexRow, IndexColumn) 
	->	index_linked_lexemes( DepLexID, DepType, _, _, Freq );
	DepType = na,Freq = 0, ! ).






aux_dependency_cell( biolg, SentenceID, IndexRow, IndexColumn, DepType, Freq ):-
	index_sentence_links( SentenceID, DepLexID, IndexRow, IndexColumn), 
	index_linked_lexemes( DepLexID, DepType, _, _, Freq ).





























	