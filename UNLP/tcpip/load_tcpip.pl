
% load Parsi tcp/ip server
load_tcpip :-	
	nl, write( `log: load Parsi Server Files ` ),
	ensure_loaded( ig_tcp_server ),
	ensure_loaded( ig_tcp_server_handlers ),
	ensure_loaded( init_parsi_server ),
	nl, write( `log: done! ` ).
