% BQ150120091233
% handlers for the IF tcp/ip server are listed here, refer to Appendix P, Winprolog technical reference, page 960

% there are 7 different socket events 
%	sck_read, sck_write, sck_oob, sck_accept, sck_connect, sck_close, sck_error
%
%													



% -----------------
% sck_accept event handler

gateway_handler( Name, sck_accept, 0 ) :-
%	nl, write( `system: IG got a request for establishing a new connection` ), 
%	findall( A, gateway(A,_,XTops,XDivs), ASet ), 
%	length( ASet, NumberofConnection ),
%	nl, write( `system: number of connection up until now is ` ), 
%	write( NumberofConnection ),
% 	any furthur checking or control over the number of connections
%	nl, write( `system: establishing new connection`),
	hide( Sock, 0 ),
	socket_handler( Sock, gateway_handler ),
	screate( Sock, Name ),
	time( Tops, Divs ),
	assert( gateway(Sock,0,Tops,Divs) ).	% the second argument may be used if we want to use a file as a buffer


% -----------------
% sck_read event handler


gateway_handler( Sock, sck_read, 0 ) :-
   	srecv( Sock, String ),
	nl, write( String ),

	% THIS IS THE PLACE TO DO PROCESSES SUCH AS PARSING
	% ( write( `<h1>` ), write( XMLString ), write( `</h1>~M~J` ) ) ~> ProcessResult,
	
	full_analysis_for_setence( String, SentenceID, FileName ), 
	( write( SentenceID  ), write( ` is genertaed as sentenceid for input string` ) )~> LogX,
	log_to_window(LogX),
	( write( FileName ), write( `~M~J` ) )~> ProcessResult , 
	ssend( Sock, ProcessResult ), 
%	ssend( Sock, `~M~J` ), 

	retract( gateway( Sock, 0, Tops, Divs ) ).
 
	


/*
	with the current  xml_read.pl implementation I do not know how to use '?ERROR?' 
	I need to learn this

'?ERROR?'( Number, Goal ) :-
	nl, write( `behi test` ),
	errmsg( Number, Message ),
	output( 0 ),
	writeq( error(Number) - Message - Goal ),
	nl,
	abort.	
			*/ 
	
% ---------------------
% sck_oob event handler

gateway_handler( Sock, sck_oob, 0 ):-
	nl, write( `system: out-of-band message has been recieved` ),
	nl, write( `check socketn `), write( Sock ),
	nl.


%------------------
% socket close event
gateway_handler( Sock, sck_close, 0 ):-
	nl, write( `system: socket close message has been recieved` ),
	findall(  ( Sock, 1, Tops, Divs ), gateway( Sock, 1, Tops, Divs ), RepliedMsg ), length( RepliedMsg, NRMsg ),  
	nl, write( `system: there has been `), write( NRMsg ), write( ` replied messages through this socket`),
	findall(  ( Sock, 0, Topsx, Divsx ), gateway( Sock, 0, Topsx, Divsx ), NotRepliedMsg ), length( NotRepliedMsg , NotNRMsg ),  
	nl, write( `system: there has been `), write( NotNRMsg ), write( ` unreplied or passed away messages through this socket`),
	retractall( gateway( Sock, _, _, _ ) ),
	nl, write( `system: socket closed: ` ), write( Sock ),
	nl.

%------------------
% socket close event

gateway_handler( Sock, sck_write, 0 ):-
	nl, write( `system: socket write event on socket ` ), write( Sock ),
	nl.



%------------------
% socket close event
gateway_handler( Sock, sck_error, Error ):-
	nl, write( `system: error on socket ` ), write( Sock ),
	nl, write( `system: socket error event #` ), write(Error),
	nl. 


 





