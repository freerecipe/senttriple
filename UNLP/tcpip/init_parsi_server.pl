init_parsi_server:-
	server( [ (name, `parsi`), (process_type, _), ('IP', _), ('port', Port )], [] ), 
	ig_tcp_server( Port ),
	( write( `Parsi server initiated at port #` ), write( Port ) )~> Log,
	log_to_window(Log).   

