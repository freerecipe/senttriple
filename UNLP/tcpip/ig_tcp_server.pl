
% ig_tcp_server( + Port ) 

% this predicate initiate a Input Gateway tcp/ip server at the given port number, 
% this server works along with its handlers placed in the accompanied file: gateway_handlers.pl

ig_tcp_server( Port ) :-
	dynamic( gateway/4 ),
	socket_handler( gateway, gateway_handler ),
  	screate( gateway, Port ),
	nl, write( `system: Gateway server is initiated at port #` ), write( Port ).




