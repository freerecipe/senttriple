% paragraph_to_sentence/2
% this predicate gets a paragraph as an input and provide the list of sentences of a pragrpah

% The predicate may need to be changed when manipulating documents in different encoding systems

paragraph_to_sentence( Section, ParagraphList ):-
    atom_chars(Section,Chars),
    sentencize(Chars, ParagraphList ). 

sentencize(L , [Sentence| Out]):-
                        L \==[],
                        sentencize(L ,Rest,Chs),
                        name(Sentence,Chs),
                        sentencize(Rest,Out).
                        % also possible to search for lexicon here

sentencize([],[]):- !.

% separators
% more sentence separators comes here

sentencize([],[],[]):-!.

% separator: "."
sentencize([46|T],T,[46]):-!.

% separator: "?"
sentencize([63|T],T,[63]):-!.

% separator: "!"
sentencize([33|T],T,[33]):-!.


sentencize([H|T],Rest,[H|List]):-
                               sentencize(T,Rest,List).

	