% load all related files

load_document_manipulation :-
	nl,write( `log: loading document manipulation files... ` ),
	ensure_loaded( paragraph_to_sentence ),
	ensure_loaded( section_to_paragraph ), 
	write( `log: DONE! ` ). 


