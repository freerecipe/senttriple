% section_to_paragraph/2
% section_to_paragraph( +Section, ?ParagraphList )
% This predicate gets a section as an input and provides the list of section's paragraphs
% The process is based on what has been introduced in tokenizer
% if moving to use other sort ofndocuemtns in different encodings or different markups then the following should be changed

section_to_paragraph( Section, ParagraphList ):-
    atom_chars(Section,Chars),
    paragraphize(Chars, ParagraphList ). 

paragraphize(L , [Paragraph| Out]):-
                        L \==[],
                        paragraphize(L ,Rest,Chs),
                        name(Paragraph,Chs),
                        paragraphize(Rest,Out).
                        % also possible to search for lexicon here

paragraphize([],[]):- !.

% separators comes here
paragraphize([],[],[]):-!.

% carriage return
paragraphize([13|T],T,[]):-!.

paragraphize([H|T],Rest,[H|List]):-
                               paragraphize(T,Rest,List).

	