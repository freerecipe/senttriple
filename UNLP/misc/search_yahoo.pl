/* Ammendments to Brian codes to get search results from yahoo */
/* The Ultimate Simple Get URL 1 - Brian D Steel - 12 Mar 05 / 19 Sep 06 */
% create socket, wait until ready, send http request, read all data and close


% replace your application code
application_key( 'BDq4ukXV34GTD_kAT3IIt7RFkWcn4kaZGlYfa0BzlHaKzMnx3jVaffVEVnLkyI4pmRc-').


% get_yahoo_search( +Keyword ).
get_yahoo_search( Keyword ):-
	string( Keyword ),

	% get the application key
	application_key(Key),	

	% start to make proper HTTP command 
   ( 	write( `GET /ysearch/web/v1/` ),
     	write( Keyword ),
	write( `?appid=` ),
	write( Key ), 
	write( `&format=xml` ),
     	write( ` ~M~J~M~J` )
   ) ~> SearchCommand,

	% create connection to yahoo server
      screate( lpa, `boss.yahooapis.com` ),	
	nl, write( `log: connected to server ` ),
	nl, write( `log: start to read ` ),

	% check the connection stat and send yourt request when you are ready to do that 
      repeat,
      sstat( lpa, Stat ),
      cmp( 1, Stat, 0 ),
      repeat,
      ssend( lpa, SearchCommand),

	% get the result back
	(
	repeat,
      srecv( lpa, Data ),
      write( Data ),
      Data = ``
   -> sclose( lpa )
   ) ~> Text,

	% show the input
	nl, write( `log: start to parse input xml` ),

	nl, write( `--------------` ),
	nl, write( Text ),nl,
	nl, write( `--------------` ),

	% go fo furhtur processes
	% parse the result --> this needs more development for the future

	input( (Text, 0 ) ),
	xml_read( _, 0, Term ),
%	inpos(  Pos  ),
%	nl, write( Pos ),
	nl, write( `log: read header` ),
%	xml_write( 0, Term ),
	xml_read( _, 0, Term2 ),
	nl, write( `log: start to write xml` ),
	nl, write( `log: furthur analysis should come here` ),
	xml_write( 0, Term2 ).






