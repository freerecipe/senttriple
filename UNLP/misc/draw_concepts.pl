graphic_dialog :- 
   _S1 = [ws_caption,ws_minimizebox,ws_sysmenu],
   _S2 = [ws_child,ws_border,ws_visible],
   wdcreate(  graphic_dialog,        `Concepts`,        183, 56, 746, 562, _S1 ),
   wccreate( (graphic_dialog,10000), grafix, `Grafix1`,  10, 10, 720, 510, _S2 ),
   window_handler( graphic_dialog, graphics_handler ),
   wshow( graphic_dialog, 1 ).

graphics_handler( _, msg_close, _, close ):-
	nl, write( `message close recieved` ),
	nl, write( `Concept GUI closed` ),
	wclose( graphic_dialog ).


% may add some handler based on the position of mouse cursor

draw_graphics( Width, Height, Text ) :-
%   gfx_mapping( 100, 100, Width, Height ),
   gfx_paint( graphic_dialog ),
  
/*   gfx( (  rectangle( 10, 10, 90, 90 ),
           (  brush = stock(white_brush),
              font = stock(prolog_fixed_font)
           -> ellipse( 20, 20, 80, 80 ),
              text( 30, 35, `hello` ),
              text( 30, 50, `world` )
           )
        )
      ),*/

	XBegin is 80 + Width,
	YBegin is 40 + Height,
	XEnd is 200 + Width, 
	YEnd is 80 + Height,
	TextXPos is XBegin + 10,
	TextYBegin is YBegin + 10,
	gfx( 
           (  brush = stock(white_brush),
             font = stock(prolog_fixed_font)
           -> 
		ellipse( XBegin,YBegin, XEnd, YEnd  ),
		text( TextXPos, TextYBegin, Text )
           )
	),

	gfx_end( graphic_dialog ).  


 



