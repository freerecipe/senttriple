draw_dependencies( stanford, SentenceID, GraphGearXML ):-
	dynamic( node_number/1 ),
	assert( node_number(1) ),
	dynamic( node_id/3 ),
	dynamic( xml_node/1 ),
	dynamic( edge/1 ),
	forall( LexemeID^LexemePosition^index_sentence_lexeme( SentenceID, LexemeID, LexemePosition ),
		( 
			index_lexeme( LexemeID, Word, PoS, _, _ ), 
			retract( node_number(NodeNumb) ),
		NewNumb is NodeNumb + 1,
		assert( node_number( NewNumb) ),
		( write(`n`), write( NodeNumb ) )~> NodeID,
		assert( node_id( NodeID, LexemeID, LexemePosition ) ),
		( write( `<node id="`), write( NodeID ), write(`" text="`), write(Word - PoS), write( `" />~M~J`) ) ~> XMLNode,
		assert( xml_node( XMLNode) ) 
		)),
	forall( DepID^GovePos^DepPos^index_sentence_dependencies( SentenceID, DepID, GovePos, DepPos ),
		(
		node_id( SourceNodeID,_,GovePos),   
		node_id( TargetNodeID,_,DepPos),   
		index_dependency_lexemes( DepID, DepType, _GLID, _DLID, _Freq ),
		( write( `<edge sourceNode="`), write( SourceNodeID ), write(`" targetNode="`), write(TargetNodeID ) , write( `" label="`), write( DepType), write( `" textcolor="555555" />~M~J` )  ) ~> Edge,
		assert( edge( Edge) )
		)),
	( 
	write( `<?xml version="1.0" ?>~M~J<graph title="Dependency Graph" bgcolor="ffffff" linecolor="cccccc" viewmode="display" width="725" height="400">~M~J` ),
	forall( xml_node( XMLNode ), (write( XMLNode ) ) ),
	forall( edge( Edge), (write(Edge) ) ),
	write( `</graph>` ) 
	)~> GraphGearXML,

	dynamic( edge/1 ),
	dynamic( node_number/1 ),
	dynamic( node_id/3 ),
	dynamic( xml_node/1 ). 



 draw_dependencies( malt, SentenceID, GraphGearXML ):-
	dynamic( node_number/1 ),
	assert( node_number(1) ),
	dynamic( node_id/3 ),
	dynamic( xml_node/1 ),
	dynamic( edge/1 ),
	forall( LexemeID^LexemePosition^index_sentence_lexeme( SentenceID, LexemeID, LexemePosition ),
		( 
			index_lexeme( LexemeID, Word, PoS, _, _ ), 
			retract( node_number(NodeNumb) ),
		NewNumb is NodeNumb + 1,
		assert( node_number( NewNumb) ),
		( write(`n`), write( NodeNumb ) )~> NodeID,
		assert( node_id( NodeID, LexemeID, LexemePosition ) ),
		( write( `<node id="`), write( NodeID ), write(`" text="`), write(Word - PoS), write( `" />~M~J`) ) ~> XMLNode,
		assert( xml_node( XMLNode) ) 
		)),
	forall( DepID^GovePos^DepPos^index_sentence_malt_dependencies( SentenceID, DepID, GovePos, DepPos ),
		(
		node_id( SourceNodeID,_,GovePos),   
		node_id( TargetNodeID,_,DepPos),   
		index_malt_dependency_lexemes( DepID, DepType, _GLID, _DLID, _Freq ),
		( write( `<edge sourceNode="`), write( SourceNodeID ), write(`" targetNode="`), write(TargetNodeID ) , write( `" label="`), write( DepType), write( `" textcolor="555555" />~M~J` )  ) ~> Edge,
		assert( edge( Edge) )
		)),
	( 
	write( `<?xml version="1.0" ?>~M~J<graph title="Dependency Graph" bgcolor="ffffff" linecolor="cccccc" viewmode="display" width="725" height="400">~M~J` ),
	forall( xml_node( XMLNode ), (write( XMLNode ) ) ),
	forall( edge( Edge), (write(Edge) ) ),
	write( `</graph>` ) 
	)~> GraphGearXML,

	dynamic( edge/1 ),
	dynamic( node_number/1 ),
	dynamic( node_id/3 ),
	dynamic( xml_node/1 ). 



 

 




