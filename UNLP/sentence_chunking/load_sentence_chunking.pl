load_sentence_chunking :-
	nl, write( `log: loading sentence cunking predicates...`  ),
	ensure_loaded( make_raw_sentence_phrases ),
	ensure_loaded( refine_sentence_phrases ),
	ensure_loaded( sentence_chunking ),
	ensure_loaded( sort_the_chunk_in_memory ),
	ensure_loaded( update_sentence_chunk_lexeme_positions ),
	nl, write( `log: done!` ).  




