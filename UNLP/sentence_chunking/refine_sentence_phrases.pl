% refine_sentence_phrases( +SentenceID )
% the next step is to get the primiraly list of chunks and find verb phrases
% this predicate itself uses a set of dependency rules. For this reason we may need to omit or refine the relavant predicates

refine_sentence_phrases( SentenceID ):-
	(index_sentence_dependency_type( SentenceID, _, _, aux ) 
		->	forall(	index_sentence_dependency_type( SentenceID, GovPositionX, RegentPositionX, aux ),
					merge_sentence_chunks( SentenceID, GovPositionX, RegentPositionX, aux ) )
		;
		true ),
	(index_sentence_dependency_type( SentenceID, _, _, auxpass )
		->	forall( 	index_sentence_dependency_type( SentenceID, GovPositionY, RegentPositionY, auxpass ),
					merge_sentence_chunks( SentenceID, GovPositionY, RegentPositionY, auxpass ) );
		true ),

	( index_sentence_malt_dependency_type( SentenceID, _, _, 'VC' )
		->	forall(	index_sentence_malt_dependency_type( SentenceID, GovPositionM, RegentPositionM, 'VC'),
					merge_sentence_chunks( SentenceID, GovPositionM, RegentPositionM, 'VC') );
		true
		),

%	( index_sentence_dependency_type( SentenceID, _, _, pobj )
%		->	forall(	index_sentence_dependency_type( SentenceID, GovPositionM, RegentPositionM, pobj ),
%					merge_sentence_chunks( GovPositionM, RegentPositionM, pobj) );
%		true
%		),
	( rule_condition( rule1, SentenceID,_,_, _)
		->	forall(	rule_condition( rule1, SentenceID,GovPositionM,RegentPositionM, RegentPositionN ),
					merge_sentence_chunks( rule1, SentenceID, GovPositionM, RegentPositionM, RegentPositionN, prep) );
		true
		).

rule_condition( rule1, SentenceID,GovPositionM,RegentPositionM, RegentPositionN ):-
	index_sentence_dependency_type( SentenceID, GovPositionM, RegentPositionM, prep ), 
	index_sentence_dependency_type( SentenceID, RegentPositionM, RegentPositionN, pobj ),
	index_sentence_lexme_position_pos( SentenceID, GovPositionM, PoS ),
	memberance( PoS, ['NN','NNS', 'NNP', 'NNPS', 'PRP', 'JJ', 'NP'] ). % I have removed cut from here

/*	index_sentence_dependency_type( SentenceID, GovPosition, RegentPosition, aux ),
	
%	forall( 
%		sentence_chunk( CellIndexNumber, StringList, PhraseType, Pointer, Span ),
%			(
%	findall( (GovPosition, RegentPosition),
	index_sentence_dependency_type( SentenceID, GovPosition, RegentPosition, aux ),
	
	index_sentence_dependency_type( SentenceID, GovPosition, RegentPosition, auxpass )



			)
		). */

index_sentence_lexme_position_pos( SentenceID, Position, PoS ):-
	index_sentence_lexeme( SentenceID, LexemeID, Position ),
	index_lexeme( LexemeID, _, PoS, _, _).

index_sentence_dependency_type( SentenceID, GovPosition, RegentPosition, DependencyType ):-
	index_sentence_dependencies( SentenceID, DependencyID, GovPosition, RegentPosition ),
	index_dependency_lexemes( DependencyID, DependencyType, _, _, _ ).


index_sentence_malt_dependency_type( SentenceID, GovPosition, RegentPosition, DependencyType ):-
	index_sentence_malt_dependencies( SentenceID, DependencyID, GovPosition, RegentPosition ),
	index_malt_dependency_lexemes( DependencyID, DependencyType, _, _, _ ).


merge_sentence_chunks( SentenceID, GovPosition, RegentPosition, DepType ):-
	sentence_chunk( SentenceID, CellIndexNumber1, Lexemes1, PoS1, PositionList1 ),
	member( GovPosition, PositionList1 ),
	sentence_chunk( SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ),
	member( RegentPosition, PositionList2 ),

	( PositionList1 = PositionList2 -> !;

	retract( sentence_chunk( SentenceID, CellIndexNumber1, Lexemes1, PoS1, PositionList1 ) ),
	retract( sentence_chunk( SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ) ),
	
	( GovPosition > RegentPosition 
		->	append( Lexemes2, Lexemes1, Lexeme ),
			append( CellIndexNumber2, CellIndexNumber1, CellIndex ),
			append( PoS2, PoS1, PoSList), FPoSList = [DepType|PoSList],
			append( PositionList2, PositionList1, PositionList ),
			assert(  sentence_chunk( SentenceID, CellIndex, Lexeme, FPoSList, PositionList ) );
		append( Lexemes1, Lexemes2, Lexeme ),
		append( CellIndexNumber1, CellIndexNumber2, CellIndex ),
		append( PoS1, PoS2, PoSList), FPoSList = [DepType|PoSList],
		append( PositionList1, PositionList2, PositionList ),
		assert(  sentence_chunk( SentenceID, CellIndex, Lexeme, FPoSList, PositionList ) ) 
	)).


merge_sentence_chunks( rule1, SentenceID, GovPosition, InterPositions, RegentPosition, DepType ):-
	sentence_chunk( SentenceID, CellIndexNumber1, Lexemes1, PoS1, PositionList1 ),
	member( GovPosition, PositionList1 ),
	sentence_chunk( SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ),
	member( RegentPosition, PositionList2 ),
	sentence_chunk( SentenceID, CellIndexNumber3, Lexemes3, PoS3, PositionList3 ),
	member( InterPositions, PositionList3 ),
	( 
		CellIndexNumber1 = CellIndexNumber2, CellIndexNumber2 = CellIndexNumber3 
			-> !
			; % all the three words are in the same phrase... nothing to do

		dynamic( inter_list/4 ),

		( sentence_chunk( SentenceID, CellIndexNumber1, Lexemes1, PoS1, PositionList1 ) 
			-> retract( sentence_chunk( SentenceID, CellIndexNumber1, Lexemes1, PoS1, PositionList1 ) ), assert( inter_list(SentenceID, CellIndexNumber1, Lexemes1,PoS1,PositionList1 ) ); true ) ,
		( sentence_chunk( SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ) 
			-> retract( sentence_chunk( SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ) ), assert( inter_list(SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ) ); true ),
		( sentence_chunk( SentenceID, CellIndexNumber3, Lexemes3, PoS3, PositionList3 ) 
			-> retract( sentence_chunk( SentenceID, CellIndexNumber3, Lexemes3, PoS3, PositionList3 ) ), assert( inter_list(SentenceID, CellIndexNumber3, Lexemes3, PoS3, PositionList3 ) ); true ),
		append_inter_list(DepType), dynamic( inter_list/5 ) 
	).

append_inter_list(DepType ) :-
	findall( CellIndexNumber2, inter_list(SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ), List ),
	( length( List, 1 ) 
		->	retract( inter_list(SentenceID, CellIndexNumber, Lexemes, PoS, PositionList ) ),
			assert( sentence_chunk( SentenceID, CellIndexNumber, Lexemes, [DepType|PoS], PositionList ) ), !
		;
		retract( inter_list(SentenceID, CellIndexNumber1, Lexemes1, PoS1, PositionList1 ) ),
		retract( inter_list(SentenceID, CellIndexNumber2, Lexemes2, PoS2, PositionList2 ) ),
		append( CellIndexNumber1, CellIndexNumber2, CellIndex ),
		append( Lexemes1, Lexemes2, Lexeme ),
		append( PoS1, PoS2, PoSList), 
		append( PositionList1, PositionList2, PositionList ),
		assert(  inter_list( SentenceID, CellIndex, Lexeme, PoSList, PositionList ) ),
		append_inter_list(DepType) ).

