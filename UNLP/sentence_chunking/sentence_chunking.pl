% sentence_chunking( +SentenceID ) 
% This predicate loads and assert all the sentence's chunks into memory and refine the chunks by help of rules in
% other predicates listed below

sentence_chunking( SentenceID):-
	make_raw_sentence_phrases( SentenceID ),
	refine_sentence_phrases( SentenceID ),
	sort_the_chunk_in_memory( SentenceID ),
	update_sentence_chunk_lexeme_positions( SentenceID ).

%	pretty_print_sentence_chunks( SentenceID ).
	

	
	

/*
% still uncompleted
pos_verb(['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']).
make_frame_candidates( SentenceID ):-
	pos_verb( VerbalClass ),
	forall( sentence_chunk( SentenceID, CellIndexNumber, Lexemes, PoS, PositionList ),
		(intersection(PoS,VerbalClass,Intersection),
		Intersection = [] -> true;
		nl, write( `**` ), write( sentence_chunk( SentenceID, CellIndexNumber, Lexemes, PoS, PositionList ) ) )).
*/
% relation subj + verb + object

/* donot look possible
find_relation( svo, SentenceID ):-
	% the required condition
	( index_sentence_dependency_type( SentenceID, GovPositionS, RegentPositionS, nsubj ),
	  index_sentence_dependency_type( SentenceID, GovPositionO, RegentPositionO, dobj ) )
		-> % then
		% nl, write( GovPositionS - RegentPositionS - GovPositionO -  RegentPositionO ),
		forall( sentence_chunk( CellIndexNumber, Lexemes, PoS, PositionList ),
			(	( member(  GovPositionS, PositionList ),	% second condition:: both the governors belong to one phrase
		  		  member(  GovPositionO, PositionList ) 
					->	make_relation( svo, SentenceID, CellIndexNumber, RegentPositionS,  RegentPositionO )
				; true )	));
	write( `required condition for svo failed` ) .

:-dynamic( relation/4 ).

make_relation( svo, SentenceID, CellIndexNumber, RegentPositionS,  RegentPositionO ):-
	sentence_chunk( SubjectCellIndexNumber, SubjectLexemes, SubjectPoS, SubjectPositionList ),
	member( RegentPositionS, SubjectPositionList ),
	sentence_chunk( ObjectCellIndexNumber, ObjectLexemes, ObjectPoS, ObjectPositionList ),
	member( RegentPositionO , ObjectPositionList ),
	sentence_chunk( CellIndexNumber, Lexemes, PoS, PositionList ),
	
	( write_lexeme_list( Lexemes) )~> Preicate,
	( write_lexeme_list( SubjectLexemes) )~> Subject,
	( write_lexeme_list( ObjectLexemes ) )~> Objecct,

	assert( relation( SentenceID, Subject, Preicate, Objecct ) ).


process_sentence_relation(SentenceID ):-
	temporary_process_sentence_chunks( SentenceID),
	find_relation( svo, SentenceID ).

*/

% make_sentence_frame( SentenceID ):- you should do that as it will be useful for other applications
	

















