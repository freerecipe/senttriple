% update_sentence_chunk_lexeme_positions
% this predicate sorts lists of words in previously  asserted sentence_chunk
update_sentence_chunk_lexeme_positions(SentenceID):-
	dynamic( usentence_chunk/5 ),
	forall( sentence_chunk(SentenceID, A, B, C, D),
		( give_update_chunk_lexeme( B, D, UpdatedLexemes),
		retract( sentence_chunk(SentenceID, A, B, C, D) ),
		sort_ascending_second_member_of_pair(UpdatedLexemes,Sorted),
		sort_asce(D,SortedD),
		assert( usentence_chunk(SentenceID, A, Sorted, C, SortedD) )
		)),
	forall( retract( usentence_chunk(SentenceID, A, Sorted, C, D) ), assert( sentence_chunk(SentenceID, A, Sorted, C, D) )),
	dynamic( usentence_chunk/5 ).

% this predicate update the lexeme position pair to new position given in sentence_chunk
give_update_chunk_lexeme( [], [], []).
give_update_chunk_lexeme( [(Lid, Pos)|T], [NewPos|D], [(Lid, NewPos)|UpdatedLexemes]):-
	give_update_chunk_lexeme( T, D, UpdatedLexemes).
	
