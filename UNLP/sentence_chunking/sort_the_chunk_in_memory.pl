% sort_the_chunk_in_memory( +SentenceID )
% this predicate sorts the previously asserted sentecnce_chunk/5

sort_the_chunk_in_memory( SentenceID ):-
	index_sentence( SentenceID, _, Length, _ ),
	dynamic( counter/1 ),
	assert( counter(1) ),
	sort_sentence_chunk(Length),
	dynamic( counter/1 ).


sort_sentence_chunk(Length):-
	counter(X), 
	X > Length, !.

sort_sentence_chunk(Length):-
	retract( counter(X) ),
	( sentence_chunk( SentenceID, [X|CellIndexNumber], Lexemes, PoS, PositionList ) 
		->
		retract( sentence_chunk( SentenceID, [X|CellIndexNumber], Lexemes, PoS, PositionList ) ),	% This retracting and asserting in the memory makes the sentence_chunks to be sorted in the memory
	 	assert( sentence_chunk( SentenceID, [X|CellIndexNumber], Lexemes, PoS, PositionList ) );
	true ),

	NewCount is X + 1,
	assert( counter( NewCount ) ),
	sort_sentence_chunk(Length).

