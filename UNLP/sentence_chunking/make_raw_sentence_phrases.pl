
% make_raw_sentence_phrases( +SentenceID )
% this makes a primiraly phrase list that has been identified by constituent parser ( such as charniak parser )


make_raw_sentence_phrases( SentenceID ):-
	index_sentence( SentenceID, _, SentenceLength, _),
	dynamic( counter/1 ),
	assert( counter(1) ),
	dynamic( meta_counter/1 ),
	assert( meta_counter(1) ),
	repeat,
	dynamic( sentence_chunk/5 ),

	retract( counter(Pointer ) ),

	(	
		index_sentence_phrase( SentenceID, PhraseID, Pointer )
		 ->	
%			nl, write( PhraseID - Pointer ),
			index_phrase( PhraseID, PhraseType, PhraseLength, PhraseFreq ),
			pretty_print_phrase( PhraseID, StringList, String ),

			meta_counter( CellIndexNumber ),
%			nl, write( CellIndexNumber ), write( `~I` ), write( String ), 
			index_phrase( PhraseID, PhraseType, PhraseLength, PhraseFreq ),
			NewPointer is Pointer + PhraseLength,

			Span is NewPointer - 1, 
			make_list_number( Pointer, Span, ListNumber ),
			assert( sentence_chunk( SentenceID, [CellIndexNumber], StringList, [PhraseType], ListNumber  )),
%			write( `~I` ), write( sentence_chunk( [CellIndexNumber], StringList, [PhraseType], ListNumber  ) ),

%			NewPointer is XPointer - 1,
			assert(counter(NewPointer))
		;
	
		index_sentence_lexeme( SentenceID, LexemeID, Pointer ),
		index_lexeme( LexemeID, WString, PoS, _, _ ),
		meta_counter( CellIndexNumber ),
%		nl,  write( CellIndexNumber), write( `~I` ), write( WString ), % write( `-->` ), write( `this word be extracted as phrase ` ), write( Pointer ),

		assert( sentence_chunk( SentenceID, [CellIndexNumber], [(LexemeID, 1 )], [PoS], [Pointer] )),

%		write( `~I` ), write( sentence_chunk( [CellIndexNumber], [(LexemeID, 1 )], [PoS], [Pointer] ) ),

		NewPointer is Pointer + 1,
		assert( counter(NewPointer) )
		
	),

	retract( meta_counter( CellIndexNumber ) ),
	NewCellIndexNumber is CellIndexNumber + 1, 
	assert( meta_counter(NewCellIndexNumber) ),

	counter(CurrentPointer),
	CurrentPointer >= SentenceLength 
		-> 
		%nl, write( `the counter went above sentence length ` ), 
		! .   

make_list_number( Pointer, Pointer, [Pointer] ).
make_list_number( Pointer, Span, ListNumber ):-
	Span > Pointer,
	NewPointer is Pointer + 1,
	make_list_number( NewPointer, Span, RestNumber ),
	append( [Pointer],RestNumber, ListNumber ),!.
