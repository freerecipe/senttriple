load_extract_triples :-
	nl, write( `log: loading triple extraction predicates ...`  ),
	chdir( Current ),
	chdir( 'fapproach' ),
	ensure_loaded( load_fapproach ),
	load_fapproach,
	
	chdir( Current ),

	% the predicates for loading the sapprach will be added here

	nl, write( `log: done!` ).

