% the intention of this predicate is a post for extracted triples in variety of ways, in general polishing the output such as
% conversion of phrases to nominal, conversion of verbs into their stem ( morphological balaba)and ommiting the repeated triples
post_process_triples( SentenceID ):-
	deleted_repeated_triples( SentenceID ).


deleted_repeated_triples( SentenceID ):-
	dynamic( dtriple/5 ),
	forall( etriple(  RuleID,SentenceID, Subj, Pred, Object ),
		
	(	dtriple( RuleID1, SentenceID, Subj, Pred, Object ) 
		-> 	retract( dtriple(RuleID1, SentenceID ,  Subj, Pred, Object ) ),
		   	assert( dtriple( [RuleID| RuleID1],SentenceID ,  Subj, Pred, Object ) ); 

		assert( dtriple([RuleID], SentenceID , Subj, Pred, Object ) )
	)),

	dynamic( etriple/5 ),
	forall( dtriple(  RuleID,SentenceID, Subj, Pred, Object ),
		   assert( etriple( RuleID,SentenceID, Subj, Pred, Object )) ),

	dynamic( dtriple/5 ).

%	forall( dtriple( SentenceID, RuleID, Subj, Pred, Object ),
%		(	retract( dtriple( SentenceID, RuleID, Subj, Pred, Object ) ),
%			assert( etriple( SentencID, RuleID, Subj, Pred, Object ) )
%		)).

		


