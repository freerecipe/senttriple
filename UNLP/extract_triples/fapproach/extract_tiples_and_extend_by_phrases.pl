%---------------------------------------------------------------------------------------------------------------%
% extract_tiples_and_extend_phrases( +SentenceID )

% This is the main predicate for extracting triples for a sentence
% It first extract main triples, then makes sentence chunks, and refine the chunks absed on 
% some dependency and Constituent Parsing, and update and sort the sentence chunks
% at the end, extrend main triples based on sentence chunks and the conjunctions
% we may extend that by adding other rules like extending by "or " phrases, etc. 
% or we may remove many of pre and post processing and handle all of them at extracting_main_triple level

%---------------------------------------------------------------------------------------------------------------%

extract_tiples_and_extend_by_phrases( SentenceID ):-
	dynamic( etriple/5 ),	% this might be removed as I we have SentenceID in triple and etriple
	dynamic( triple/5 ),
	extract_main_triples( SentenceID ),
	make_raw_sentence_phrases( SentenceID ),
	refine_sentence_phrases( SentenceID ),
	update_sentence_chunk_lexeme_positions(SentenceID),
	extend_main_triples( SentenceID ),
	extend_triple_for_conjunctions( SentenceID ), % newwwwww
	omit_repeated_words(SentenceID),
	post_process_triples( SentenceID ). % new tooo


% omit_repeated_words( +SentenceID )
% this predicate omits the repeated words in the subject, or object part in the Predicate part
omit_repeated_words(SentenceID) :-
	dynamic( dtriple/5 ),
	forall( retract( etriple( RuleID, SentenceID,ExtendedSubject , ExtendedProperty, ExtendedObject) ),
		( difference(ExtendedObject,ExtendedProperty,NewExtendedObject), 
		assert(dtriple( RuleID, SentenceID,ExtendedSubject , ExtendedProperty, NewExtendedObject)) )),

	forall( retract( dtriple( RuleIDs, SentenceID,ExtendedSubject , ExtendedProperty, NewExtendedObject) ),
		assert(etriple( RuleIDs, SentenceID,ExtendedSubject , ExtendedProperty, NewExtendedObject) )),
	dynamic( dtriple/5 ).

	

 

