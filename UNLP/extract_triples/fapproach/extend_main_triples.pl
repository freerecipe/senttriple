% extend_main_triples( +SentenceID )
% this predicate extend main triples by help of phrase boundaris. 
% The sentence chunks might be modified, any chunking algorithm may be used to provide the system with chunks

extend_main_triples( SentenceID ):-
	forall( triple( RuleID, SentenceID, Subject, Property, Object ),

	( 	retract( triple( RuleID, SentenceID, Subject, Property, Object )), 
		( Subject = [0] -> ExtendedSubject = [] ; ( extend_entity( SentenceID, Subject, ExtendedSubject )-> FinalSubject = ExtendedSubject ; FinalSubject = Subject ) ),
		( Object = [0] -> ExtendedObject = [];  ( extend_entity( SentenceID, Object, ExtendedObject ) -> FinalObject = ExtendedObject ; FinalObject = Object ) ),
		( extend_entity( SentenceID, Property, ExtendedProperty ) -> FinalProperty = ExtendedProperty ; FinalProperty = Property ),
		  assert( etriple( RuleID, SentenceID,FinalSubject, FinalProperty , FinalObject ) )
	)).


extend_entity( SentenceID, Entity, ExtendedEntity ):-
	sentence_chunk( SentenceID, CellIndexNumber, Lexemes, PoS, PositionList ),
	intersection( Entity, PositionList, Intersection ),
	Intersection \= [],
	length( Intersection, L1 ),
	length( Entity, L2 ),
	( L1 >= L2 -> ExtendedEntity = PositionList ; ExtendedEntity = Entity ).
		
