% extract_main_triples( +SentenceID )
% this is the main predicate for extracting triples for a given SentenceID
% this predicate loads dependency matrixes and uses the triple_rules ( combination of dependency parses for extracting main triples )


extract_main_triples( SentenceID ):-
	create_dependency_matrix( stanford, SentenceID ),	% this may not be needed (if we create matrixes before hand we do not need this )
	create_dependency_matrix( malt, SentenceID ),		% this may not be needed
	create_dependency_matrix( biolg, SentenceID ),		% this may not be needed
	
	dynamic( triple/5 ),

	findall( (Columna1,Rowa2,Columna3), triple_rule( 1,SentenceID, Columna1,Rowa2,Columna3), _ ),

%	findall( (Columnb1,Rowb2,Columnb3), triple_rule( 2,SentenceID, Columnb1,Rowb2,Columnb3), _ ),

	findall( (Columnc1,Rowc2,Columnc3), triple_rule( 3,SentenceID, Columnc1,Rowc2,Columnc3), _ ),

	findall( (Columnd1,Rowd2,Columnd3), triple_rule( 4,SentenceID, Columnd1,Rowd2,Columnd3), _ ),

	findall( (Columne1,Rowe2,Columne3), triple_rule( 5,SentenceID, Columne1,Rowe2,Columne3), _ ),

	findall( (Columnf1,Rowf2,Columnf3), triple_rule( 6,SentenceID, Columnf1,Rowf2,Columnf3), _ ),

	findall( (Columng1,Rowg2,Columng3), triple_rule( 7,SentenceID, Columng1,Rowg2,Columng3), _ ),

	findall( (Columnh1,Rowh2,Columnh3), triple_rule( 8,SentenceID, Columnh1,Rowh2,Columnh3), _ ),

	findall( (Columni1,Rowi2,Columni3), triple_rule( 9,SentenceID, Columni1,Rowi2,Columni3), _ ),

	findall( (Columnj1,Rowj2,Columnj3), triple_rule( 10,SentenceID, Columnj1,Rowj2,Columnj3), _ ),

	findall( (Columnk1,Rowk2,Columnk3), triple_rule( 11,SentenceID, Columnk1,Rowk2,Columnk3), _ ),

	findall( (Columnl1,Rowl2,Columnl3), triple_rule( 12,SentenceID, Columnl1,Rowl2,Columnl3), _ ),

	findall( (Columnm1,Rowm2,Columnm3),	triple_rule( 13,SentenceID,Columnm1, Rowm2,Columnm3), _ ),

	findall( (Columnn1,Rown2,Columnn3),	triple_rule( 14,SentenceID, Columnn1,Rown2,Columnn3), _ ),

	findall( (Columno1,Rowo2,Columno3),	triple_rule( 15,SentenceID, Columno1,Rowo2,Columno3), _ ),

	findall( (Columnp1,Rowp2,Columnp3),	triple_rule( 16,SentenceID, Columnp1,Rowp2,Columnp3), _ ),

	findall( (Columnp1,Rowp2,Columnp3),	triple_rule( 17,SentenceID, Columnp1,Rowp2,Columnp3), _ ),

	true.



	
% extend_etriples_to_coordinate_clauses:-



% extract main triples an assign them a level of confidence!
% no talked abou conditions ... this is  really something publishable,for example X boosts why when Z exists! it is very important!!!! conditional rdf!!!!!!!!!!
% Do not forget about Copula .... it is very important
% do not forget to add confidence

% do not forget to replace word with phrases

% preposiiton classes in English are important
% the tesne and mode of verbs are important
% conjunctions are important
% add the frequency of dependencies
% prepositional verbs dictionary
% anaphora for a, one, ones as well as other pronouns
% use sparse matrix instead of full matrix --> this will reduce the search space very much




	




















