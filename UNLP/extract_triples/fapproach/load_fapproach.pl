% load the predicates for the first approach to extracting triples 
% extract main triples
% extract chunks and augment them with some rules
% extend main triples with augmented chunks

load_fapproach :-
	nl, write( `log: loading ifapproach to extracting triples predicates ...`  ),
	ensure_loaded( extend_main_triples ),
	ensure_loaded( extend_triple_for_conjunctions ),
	ensure_loaded( extract_main_triples ),
	ensure_loaded( extract_tiples_and_extend_by_phrases ),
	ensure_loaded( triple_rule ),
	ensure_loaded( post_process_triples ),
	nl, write( `log: done!` ).

	
