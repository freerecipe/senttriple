% extend_triple_for_conjunctions( +SentenceID)
% this predicate extend main triples by help of Conjunctions that has appeared in a sentence

% this should be modified in a way that if a new triple is added then it itself should
%  be checked for conjunctions and if possible to be extended by conjunctions 


extend_triple_for_conjunctions(SentenceID):-
	dynamic( xtriple/5 ),
	forall( etriple( RuleID, SentenceID, B, C, D),
		find_conjunctions(etriple( RuleID, SentenceID, B, C, D))  ),
	forall(  xtriple(RI, X, Y, Z, F ), assert(etriple(RI, X,Y, Z, F)) ),
	dynamic( xtriple/5 ).

find_conjunctions( etriple( RuleID, SentenceID, B, C, []) ).
find_conjunctions( etriple( RuleID, SentenceID, B, C, [ObjectHead|T]) ):-
	findall( ConjColumn, ( dmtx_cell(  stanford,ObjectHead, ConjColumn, conj, _ ); dmtx_cell(  malt,ObjectHead, ConjColumn, 'COORD', _ ) ), ConjunctList ),
%	nl, write( ConjunctList  ),
	( ConjunctList == [] 
	-> 	% nl, write( `no conjunction` ), write( ConjunctList ),
		find_conjunctions( etriple( RuleID, SentenceID, B, C, T) ) ;
%	write( ConjunctList ),
	assert_conjunct_list_triples( RuleID, ConjunctList, SentenceID,  B, C, ObjectHead ),
	find_conjunctions(etriple( RuleID, SentenceID, B, C, T) ) ).


assert_conjunct_list_triples( RuleID, [], SentenceID,  B, C, ObjectHead ).
assert_conjunct_list_triples( RuleID, [ConjColumn|T], SentenceID,  B, C, ObjectHead ):-
%	write( ObjectHead ), write( `is conjuated with `  ),
%	write( ConjColumn ), 
	extend_entity( SentenceID, [ConjColumn], ExtendedEntity ),
	assert( xtriple(RuleID, SentenceID, B, C, ExtendedEntity) ),	% you cannot assert etriple because it may get into infinite resursion
	assert_conjunct_list_triples( RuleID, T, SentenceID,  B, C, ObjectHead ).


