%	Set of rules for extrcting triples from input sentences
%
%
%



% simple Subject Verb Object
triple_rule(1,SentenceID, [Column1],[Row],[Column2] ):-
	% the verb class should be checked
	dmtx_cell( stanford, Row, Column1, nsubj, _ ),
	dmtx_cell( stanford,Row, Column2, dobj, _ ),
	assert( triple(1, SentenceID, [Column1], [Row], [Column2] ) ).

% Simple UnknownSubject PassiveVerb SubectOfPassiveVer
triple_rule( 2,SentenceID, [0],[Row], [Column1] ):-
	dmtx_cell( stanford,Row, Column1, nsubjpass, _ ),
	assert( triple(2, SentenceID, [0], [Row], [Column1] ) ). 

% Simple 
triple_rule( 3,SentenceID, [Column2],[Row,Column3],[Column4] ):-
 % the second argument must show the class of preposition
	dmtx_cell( stanford,Row, Column1, nsubj, _ ),
	dmtx_cell( stanford,Row, Column2, dobj, _ ),
	dmtx_cell( stanford,Row, Column3, prep, _ ),
	dmtx_cell( stanford,Column3, Column4, pobj, _ ),
	% furthur analysis such as checking the class of prepositions
	assert( triple(3, SentenceID,[Column2], [Row, Column3], [Column4] ) ).


triple_rule( 4,SentenceID, [SubColumn], [MainVerbRow, PrepCoulmn], [PrepObjecCoulmn] ):-
	dmtx_cell( malt, AuxRow, SubColumn, 'SBJ', _ ),
	dmtx_cell( stanford,MainVerbRow, AuxRow, aux, _ ),
	dmtx_cell( stanford,MainVerbRow, PrepCoulmn, prep, _ ),
	dmtx_cell( stanford,PrepCoulmn, PrepObjecCoulmn, pobj, _ ),
	
	% furthur analysis such as checking the class of prepositions
	assert( triple(4, SentenceID,[SubColumn], [MainVerbRow, PrepCoulmn], [PrepObjecCoulmn] ) ).

triple_rule( 5,SentenceID, [SubColumn], [MainVerb, ToColumn], [ObjectCol]  ):-
	dmtx_cell( malt, AuxRow, SubColumn, 'SBJ', _ ),
	dmtx_cell( biolg, AuxRow, ToColumn, 'TO', _ ),
	dmtx_cell( biolg, ToColumn, MainVerb, 'I', _ ),
	dmtx_cell( malt, MainVerb, ObjectCol, 'OBJ', _),
	% furthur analysis such as checking the class of prepositions
	assert( triple(5, SentenceID,[SubColumn], [MainVerb, ToColumn], [ObjectCol] ) ).

triple_rule( 6,SentenceID, [OBJColumn], [MainVerb, ByColumn], [BYObject,ByObjectObject]  ):-
	dmtx_cell( biolg, MainVerb, ByColumn, 'MVp', _ ),
	dmtx_cell( malt, MainVerb, OBJColumn, 'OBJ', _ ),
	dmtx_cell( malt, ByColumn, BYObject, 'PMOD', _ ),
	dmtx_cell( malt, BYObject, ByObjectObject, 'OBJ', _),
	% furthur analysis such as checking the class of prepositions
	assert( triple(6, SentenceID,[OBJColumn], [MainVerb, ByColumn], [BYObject,ByObjectObject] ) ).

triple_rule( 7,SentenceID, [SubColumn], [Verb, ToCol,SecondVerb ], [Pred]  ):-
	dmtx_cell( malt, Verb, SubColumn, 'SBJ', _ ),
	dmtx_cell( biolg, Verb, ToCol, 'TOf', _ ),
	dmtx_cell( malt, Verb, SecondVerb, 'OBJ', _ ),
	dmtx_cell( biolg, ToCol, SecondVerb, 'Ix', _ ),
	dmtx_cell( malt, SecondVerb, Pred, 'PRD', _ ),		% this may be changed!!!
	assert( triple(7, SentenceID,[SubColumn], [Verb, ToCol,SecondVerb ], [Pred] ) ).

triple_rule( 8,SentenceID, [SubColumn], [Verb, DObject,Prep], [PrepComp] ):-
	dmtx_cell( stanford,Verb, SubColumn, nsubj, _ ),
	dmtx_cell( malt, Verb, DObject, 'OBJ', _ ),
	dmtx_cell( biolg, Verb, Prep, 'MVp', _ ),
	dmtx_cell( stanford,Verb, Prep, prep, _ ),
	dmtx_cell( stanford,Prep, PrepComp, pcomp, _ ),
	assert( triple(8, SentenceID,[SubColumn], [Verb, DObject,Prep], [PrepComp] ) ).

triple_rule( 9,SentenceID, [SubColumn], [Verb, DObject,Prep], [PrepComp] ):-
	dmtx_cell(  stanford, Verb, SubColumn, nsubj, _ ),
	dmtx_cell( malt, Verb, DObject, 'OBJ', _ ),
	dmtx_cell( biolg, Verb, Prep, 'MVp', _ ),
	dmtx_cell( biolg, Verb, Prep, 'I', _ ),
	assert( triple(9,SentenceID,[SubColumn], [Verb, DObject,Prep], [PrepComp] ) ).

triple_rule( 10,SentenceID, [SubColumn,PartMOD,Prep,PObj], [Verb], [Pred] ):-
	dmtx_cell(  malt, Verb, SubColumn, 'SBJ', _ ),
	dmtx_cell( malt, Verb, Pred, 'PRD', _ ),
	dmtx_cell(  stanford,SubColumn, PartMOD, partmod, _ ),
	dmtx_cell(  stanford,PartMOD, Prep, prep, _ ),
	dmtx_cell(  stanford,Prep, PObj, pobj, _ ),
	assert( triple(10, SentenceID,[SubColumn,PartMOD,Prep,PObj], [Verb], [Pred] ) ).

triple_rule( 11,SentenceID, [SubColumn], [Verb], [Pred] ):-
	dmtx_cell(  malt, Verb, SubColumn, 'SBJ', _ ),
	dmtx_cell( malt, Verb, Pred, 'PRD', _ ),
	assert( triple(11, SentenceID,[SubColumn], [Verb], [Pred] ) ).



triple_rule( 12,SentenceID, [SubColumn], [Verb], [Pred] ):-		% this is for sentence se_leyqitwpoh
	dmtx_cell( stanford, Verb, SubColumn, nsubjpass, _ ),
	dmtx_cell( stanford,Verb, Adverbial,advmod, _ ),
	dmtx_cell( stanford,Verb, Partmod,partmod, _ ),
	dmtx_cell( stanford,Partmod,PartModObj,dobj, _ ),
	assert( triple(12, SentenceID,[SubColumn], [Verb,Adverbial,Partmod ], [PartModObj] ) ).

triple_rule( 13,SentenceID,[SubColumn], [Verb,Object,Prep], [PrepObj] ):-		% this is for sentence se_leyqitwpoh
	dmtx_cell(  stanford,Verb, SubColumn, nsubj, _ ),
	dmtx_cell( stanford,Verb, Object,dobj, _ ),
	dmtx_cell( stanford,Object,Prep, prep, _ ),
	dmtx_cell( stanford,Prep, PrepObj, pobj, _ ),
	assert( triple(13, SentenceID,[SubColumn], [Verb,Object,Prep], [PrepObj] ) ).


triple_rule( 14,SentenceID, [SubColumn],[Verb,Object],[PRED] ):-		% this is for sentence se_leyqitwpoh
	dmtx_cell( stanford,Verb, SubColumn, nsubj, _ ),
	dmtx_cell( stanford,Verb, Object,dobj, _ ),
	dmtx_cell( biolg,Object,That, 'TH', _ ),
	dmtx_cell( biolg,That, ThatObject, 'Cet', _ ),
	dmtx_cell( biolg,ThatObject, SPX, 'Spx', _ ),
	dmtx_cell( malt,SPX, PRED, 'PRD', _ ),
	assert( triple(14, SentenceID,[SubColumn],[Verb,Object],[PRED] ) ).


triple_rule( 15,SentenceID, [SubColumn], [Aux,Verb,Prep],[PrepObject] ):-		% this is for sentence se_leyqitwpoh
	dmtx_cell( stanford,Verb, SubColumn, nsubjpass, _ ),
	dmtx_cell( stanford,Verb, Aux, auxpass, _ ),
	dmtx_cell( stanford,Verb, Prep, prep, _ ),
	dmtx_cell( stanford,Prep, PrepObject, pobj, _ ),
	assert( triple(15, SentenceID, [SubColumn], [Aux,Verb,Prep],[PrepObject] ) ).


triple_rule( 16,SentenceID,[SubColumn], [XCOMP], [XCOMPObj]  ):-		% this is for sentence se_leyqitwpoh
	dmtx_cell( malt,Verb, SubColumn, 'SBJ', _ ),
	dmtx_cell( stanford,Verb, XCOMP, xcomp, _ ),
	dmtx_cell( stanford,XCOMP, XCOMPObj, dobj, _ ),
	assert( triple(16, SentenceID,[SubColumn], [XCOMP], [XCOMPObj]  ) ).


triple_rule( 17,SentenceID, [SubjColumn], [Verb1,Verb2,Obj,PrepMVp], [PrepObj]  ):-		% this is for sentence se_leyqitwpoh
	dmtx_cell( malt,Verb1, SubjColumn, 'SBJ', _ ),
	dmtx_cell( stanford,Verb2, SubjColumn, nsubj, _ ),
	dmtx_cell( stanford,Verb2, Verb1, aux, _ ),
	dmtx_cell( stanford,Verb2, Obj, dobj, _ ),
	dmtx_cell( biolg,Verb2, PrepMVp, 'MVp', _ ),
	dmtx_cell( stanford,PrepMVp, PrepObj, pobj, _ ),
%	dmtx_cell( stanford,Verb, XCOMP, xcomp, _ ),
%	dmtx_cell( stanford,XCOMP, XCOMPObj, dobj, _ ),
	assert( triple(17, SentenceID,[SubjColumn], [Verb1,Verb2,Obj,PrepMVp], [PrepObj]  ) ).


% S V O
% S VP PO
% S C Pred
% S C-Pred Pred-PObj












