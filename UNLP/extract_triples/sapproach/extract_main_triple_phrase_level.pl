extract_main_triple_phrase_level( SentenceID ):-	
	dynamic( ptriple/5 ),
	findall( [Column1],[Row],[Column2], phrase_triple_rule(1, SentenceID, [Column1],[Row],[Column2] ), _ ).
	

phrase_triple_rule(1, SentenceID, Subj, Verb, Obj  ):-
	% the verb class should be checked
	pmc( stanford, SentenceID, Verb, Subj, nsubj, _ ),
	pmc( stanford, SentenceID, Verb, Obj, obj, _ ),
	assert( triple(1, SentenceID, Subj, Verb, Obj ) ).
