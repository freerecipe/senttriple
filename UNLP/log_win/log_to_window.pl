log_to_window( Text ):-
	rich_format((log_dialog,9000), selection, [size = 160,color = (255,16,7),bold = 1, face = `Calibri`]),
	wrchtxt( (log_dialog,9000),2,`~M~Jlog time: `),
	time( 1, Time),
	Time = ( _, Offset ),
	time(Offset ,Hour,Minute,Second,Millisecond),
	( write(Hour), write(`:`), write( Minute) , write( `:` ), write( Second ) , write( `.` ), write( Millisecond ), write( `~I| ` ) )~> TimeText,
	rich_format((log_dialog,9000), selection, [size = 140,color = (25,29,112),bold = 1, face = `Calibri`]),
	wrchtxt( (log_dialog,9000),2,TimeText ),
	rich_format((log_dialog,9000), selection, [size = 210,color = (155,69,72),bold = 1, face = `Calibri`]),
	wrchtxt( (log_dialog,9000),2,`Log: `),
	rich_format((log_dialog,9000), selection, [size = 220,color = (55,69,72),bold = 1, face = `vrinda`]),
	wrchtxt( (log_dialog,9000),2,Text). 


 
