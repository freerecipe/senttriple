dialog :- 
   _S1 = [ws_caption,ws_minimizebox,ws_sysmenu,dlg_ownedbyprolog],
   _S2 = [ws_child,ws_border,ws_hscroll,ws_tabstop,ws_visible,ws_vscroll,es_left,es_multiline,es_autohscroll,es_autovscroll],
   wdcreate(  log_dialog,       `Log Window`,  183, 56, 646, 482, _S1 ),
   wccreate( (log_dialog,9000), rich, `Rich1`,  10, 10, 620, 430, _S2 ).


dialog_handler:-
   window_handler(log_dialog, dialog_handler ).

dialog_show:-
	wshow( log_dialog,1 ).


dialog_handler( log_dialog,WindowsMessage  , _, _ ):-
	WindowsMessage = msg_close,
	wclose( log_dialog ).




