load_penn_parse_analysis :-
	nl, write( `log: load penn parse analysis files... ` ),
	ensure_loaded( get_penn_style_parse ),
	ensure_loaded( load_penn_parse_analysis ),
	ensure_loaded( look_for_penn_token ),
	ensure_loaded( look_for_simple_penn_struct ),
	ensure_loaded( penn_tree_parse_rules ),
	ensure_loaded( tokenize_penn_style_string ),
	ensure_loaded( uncompleted_penn_tree_parse_rules ).
