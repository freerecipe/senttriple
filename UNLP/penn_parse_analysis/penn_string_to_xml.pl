% `(ROOT (S (NP (DT this)) (VP (VBZ is) (NP (DT the) (NN test)))))`
% [list([],[`ROOT`,list([],[`S`,list([],[`NP`,list([],[`DT this`])]),list([],[`VP`,list([],[`VBZ is`]),list([],[`NP`,list([],[`DT the`]),list([],[`NN test`])])])])])]
% this predicate repolaces open and close pranthesis with list to make it parsable by xml_read
penn_string_to_xml(String, OutList):-
	string_chars( String, Chars ),
	rc_penn_string_to_xml( Chars, XMLChars ),
	string_chars( XML, XMLChars ),
	input( ( XML, 0 ) ),
	xml_read( _, 0, OutList).

		

rc_penn_string_to_xml([], []).
rc_penn_string_to_xml([H|String], XML):-
	H = 40 -> 
		rc_penn_string_to_xml(String, XML2),
		string_chars( `<list>`, Chars ),
		append( Chars, XML2, XML ),!;
	
	H = 41 -> 
		rc_penn_string_to_xml(String, XML2),
		string_chars( `</list>`, Chars ),
		append( Chars, XML2, XML ), !;

	rc_penn_string_to_xml(String, XML2),
	XML = [H|XML2],!.

