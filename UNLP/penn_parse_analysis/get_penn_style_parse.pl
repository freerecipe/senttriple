
% get_penn_style_parse( +XML, ?PennParse )
% extract the penn style part of the stanford parser, if it could not find any penn style parse then the PennParse will be an empty string otherwise
% it will be a string represent the penn style parse. This predictae depends on the scheme of input xml and for the time being it only gets stanford
get_penn_style_parse( XML, PennParse ):-
	input( (XML, 0 ) ),
	repeat,
	xml_token( A, B ),
	( A = tree([(style,`penn`)]) 
	->	xml_read( _, _, [PennParse] ), !;
	( (A = [], B = -1 )-> write( `end of file` ),PennParse = ``, !)
		).  



