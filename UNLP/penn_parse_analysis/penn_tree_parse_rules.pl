
token( [(pos, SPoS)], [Sword] ) -->
	['(', PoS, ' ', Word, ')'],
	{ 
	atom_string( PoS, SPoS ),
	atom_string( Word, Sword ) }.


token_list( [], TokenList ) -->
	token( [(pos, PoS)], [Word] ),
	{TokenList = [token( [(pos, PoS)], [Word] )]}
	;
	token( [(pos, PoS)], [Word] ),[' '],
	token_list( [], TokenListX ),
	{ append( [token( [(pos, PoS)], [Word] )],TokenListX, TokenList) }.


struct( [(type, SType)], TokenList ) -->
	% only a token list
	['(', Type, ' '], token_list( [], TokenList ), [')'],
	{ atom_string( Type, SType )}
	;
	% a list of tokens in addition to struct inside
	['(', Type, ' '], token_list( [], TokenListZ ), struct( [(type, TypeZ)], TokenListX ), [')'],
	{ 
	append( TokenListZ , [struct( [(type, TypeZ)], TokenListX )], TokenList ),
	atom_string( Type, SType )	}.
/*


*/

struct_list( [], B ) -->
	struct( [(type, Type)], TokenList ),
	{ A = [], B = [struct( [(type, Type)], TokenList )] };

	struct( [(type, Type)], TokenList ), [' '],struct_list( [], D ),
	{ A = [], 
	append( [struct( [(type, Type)], TokenList )], D, B ) }.







parse_struct( [(type, penn )], TokenList ) -->
	['(', 'ROOT', ' '], struct_list( [(type, Type)], TokenList ), [')'].


