% look_for_simple_penn_struct( +List, ?StructList )
% this predicate gets a list contains combination of tokens and other charecters and extracts simples structure
% Here simple structure is a structure comprises of only words and no other struct
% this predicate may be generalized to find some certain sort of structure (or phrases ), 
% even this can be replaced by any order of words that might be of interest, for example the
% rules that someone may define to extract claims

look_for_simple_penn_struct( List, StructList ):-
%	dynamic( aux_struct_counter/1 ),		% note I have changed the position of phrase to the position of the first word in the phrase
%	assert( aux_struct_counter( 1 ) ),	
	rc_look_for_simple_penn_struct( List, StructList ) %,
	.
%	dynamic( aux_struct_counter/1 ).

rc_look_for_simple_penn_struct( List, StructList ):-
	( phrase( simple_struct(A, B), List, Rest ) ->
%		retract( aux_struct_counter( Position ) ),
%		NewPos is Position + 1,
%		number_string( Position, SPosition ),
%		assert( aux_struct_counter( NewPos ) ),
		B = [token( TA, TB )|_],
		member( (position,FirstTokenPosition), TA ),
		rc_look_for_simple_penn_struct( Rest, List2 ),
%		append( A, [(phrase_position, SPosition )], AC ),
		append( A, [(phrase_position, FirstTokenPosition)], AC ),
		StructList = [ struct(AC, B)| List2 ], !
		;
		List = [H|T],
		rc_look_for_simple_penn_struct( T, List2 ),
		StructList = [H | List2 ], ! ).

rc_look_for_simple_penn_struct( [], [] ).


simple_struct([(type, SType)], B) -->
	['(',Type,' '],list_of_tokens( [], B ), [')'],
	{atom_string( Type, SType)}.

list_of_tokens([], List )-->
	[token(X,Y)],
	{List = [token(X,Y)] };
	[token(X,Y), ' '], list_of_tokens( [], List2 ),
	{append([token(X,Y)], List2, List)}.  


