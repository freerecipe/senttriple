% this predicate gets a tokenized string penn parse and replace tokens in the list 
% look_for_token( +TokenizedList, ?ReplacedTokenList )

look_for_penn_token( List, TokenList ):-
	init_penn_token_counter_in_sentence,
	rc_look_for_penn_token( List, TokenList ),
	dynamic( penn_token_counter/1 ).

init_penn_token_counter_in_sentence :-
	dynamic( penn_token_counter/1 ),
	assert( penn_token_counter( 1 ) ).

get_penn_token_number( SX ):-
	retract(penn_token_counter( X )),
	number_string( X, SX ),
	New is X + 1,
	assert( penn_token_counter( New ) ).

% recursive bit of work
rc_look_for_penn_token( List, TokenList ):-
	( phrase( token( A, B ), List, Rest ) ->
		get_penn_token_number( X ),
%		nl, write( `log: Assigined position in sentence ` ) , write( X ), write( ` to ` ), write( B ),
		append(A, [(position,X)], NA ),
		rc_look_for_penn_token( Rest, TokenList2 ),
		TokenList = [ token( NA, B )| TokenList2 ], !
		;
		List = [H|T],
		rc_look_for_penn_token( T, TokenList2 ),
		TokenList = [H | TokenList2 ], ! ).
		
rc_look_for_penn_token( [], [] ).	


token( [(pos, SPoS)], [Sword] ) -->
	['(', PoS, ' ', Word, ')'],
	{ 
	atom_string( PoS, SPoS ),
	atom_string( Word, Sword ) }.
