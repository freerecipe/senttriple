% this read an xml file contains information about servers, currently only parse servers
servers_config :-
	log_to_window( `loading parsers addresses` ),
	dynamic( working_directory/1 ),
	dynamic( server/2 ),
	fcreate( server_xml,'config\server.xml', 0, 2048, 1 ),	
	input( server_xml ),
	xml_read( _, 0, ServerList ),
	ServerList = [servers([],Servers)],
	assert_servers( Servers ),
	fclose( server_xml),
	log_to_window( `set working directory` ),
	fcreate( public_folder,'config\public_folder.conf', 0, 2048, 1 ),	
	input( public_folder ),
	fread(a,0,0,Path),
	fclose( public_folder ),
	trim( Path, TrimmedPath ),
	assert( working_directory( TrimmedPath ) ).

assert_servers( [] ).
assert_servers( [ServerList|T] ):-
	ServerList = server([(name,Name),(process_type,Type),('IP',IP),(port,Port)],[]),
	number_string( NPort, Port ),
	assert( server([(name,Name),(process_type,Type),('IP',IP),(port,NPort)],[]) ),
	assert_servers( T ).
