% trim( CharList, TrimmedList )
% trim( +CharList, ?TrimmedList )
% simple trim
trim( Atom, TAtom ):-
	atom( Atom ),
	atom_chars( Atom, CharList ),
	ntrim( CharList, NCharList ),
	rtrim( NCharList, TCharList ),
	atom_chars( TAtom, TCharList ).

trim( String, TString ):-
	string( String ),
	string_chars( String, CharList ),
	ntrim( CharList, NCharList ),
	rtrim( NCharList, TCharList ),
	string_chars( TString, TCharList ).

ntrim( [H|T], T2 ):-
	H =< 32,
	ntrim( T, T2 ).

ntrim( [H|T], [H|T] ):-
	H > 32.

rtrim( List, TList ):-
	reverse( List, RList ),
	ntrim( RList, RTList ),
	reverse( RTList, TList ).
	