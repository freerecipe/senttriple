% sort_ascending_second_member_of_pair(+List,-Sorted)
% this predicate sort a list of tuples based on the second member of the tuple
sort_ascending_second_member_of_pair(List,Sorted):-
		asort(List,[],Sorted).

asort([],List,List).

asort([H|T],List,Sorted):-
	apivoting(H,T,L1,L2),
	asort(L1,List,Sorted1),
	asort(L2,[H|Sorted1],Sorted).


apivoting(H,[],[],[]).

apivoting(H,[X|T],[X|L],G):-	
	H = (_, HPoS ),
	X = (_, XPos ),
	(compare( '>=', XPos, HPoS ); compare( '>', XPos, HPoS )),
	apivoting(H,T,L,G),!.

apivoting(H,[X|T],L,[X|G]):-	
	H = (_, HPoS ),
	X = (_, XPos ),
	compare( '>', HPoS, XPos ),
	apivoting(H,T,L,G),!.  
