% unique IDs generators

% here are several predicate to generate unique ids

genid( String ):-
	time( 0, Time ),
	hide( Atom, 0 ), ( write( Atom ), write(Time ) )~> String.

gensym2( Atom ) :-
	var( Atom ),
	time( 0, Time ),
	Time = ( A, B),
	number_atom( A, AA),
	number_atom( B, BB),
	hide( Atom1, 0 ),
	assert( in_use(Atom1) ),
	( write(`bq`),write(Atom1), write(AA), write( BB))~> STR,
	atom_string( Atom,STR). 

gensym( Atom ) :-
	var( Atom ),
	hide( Atom, 0 ),
	assert( in_use(Atom) ). 	%%% should be retract

gen_temporal_unique_id( String ):-
	time( 0, Time ), time( Fine, Divs ),
	( write(Time), write(Fine), write(Divs ) )~> String.   


