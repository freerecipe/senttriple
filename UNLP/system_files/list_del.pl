% list_del(+A, +B, ?C)
% delet member A from List a B
list_del(X, [X|Tail], Tail).
list_del(X, [Y|Tail], [Y|Tail1]) :-
	list_del(X, Tail, Tail1).




delete_a_list_member(X, [], []) :-
  !.

delete_a_list_member(X, [X|Tail], Tail1) :-
  delete_a_list_member(X, Tail, Tail1),
  !.

delete_a_list_member(X, [Y|Tail], [Y|Tail1]) :-
  delete_a_list_member(X, Tail, Tail1).  




