
% intersection( PotentialAnswer1, PotentialAnswer2, FinalPotentialAnswer ):-

intersection([X|Y],M,[X|Z]) :- member(X,M), intersection(Y,M,Z).
intersection([X|Y],M,Z) :- \+ member(X,M), intersection(Y,M,Z).
intersection([],M,[]).

