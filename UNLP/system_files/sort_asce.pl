% sort_asce(+List,-Sorted)
% sort a list ascending
sort_asce(List,Sorted):-
		sasort(List,[],Sorted).

sasort([],List,List).

sasort([H|T],List,Sorted):-
	sapivoting(H,T,L1,L2),
	sasort(L1,List,Sorted1),
	sasort(L2,[H|Sorted1],Sorted).


sapivoting(H,[],[],[]).

sapivoting(H,[X|T],[X|L],G):-	
	(compare( '>=', X, H ); compare( '>', X, H )),
	sapivoting(H,T,L,G),!.

sapivoting(H,[X|T],L,[X|G]):-	
	compare( '>', H, X ),
	sapivoting(H,T,L,G),!.  
