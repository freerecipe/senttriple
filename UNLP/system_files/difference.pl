difference([],_,[]):-!.
difference([X|L],K,M) :- member(X,K), !,difference(L,K,M).
difference([X|L],K,[X|M]) :- not(member(X,K)), difference(L,K,M).
