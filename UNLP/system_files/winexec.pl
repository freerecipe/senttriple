% Run a process and wait for Timeout * Count millisecs.
% eg.
% create_process_and_wait( 'c:\windows\notepad.exe', '-f test.txt', 1000, 100, Result ).
% Result returns the result of the call.

create_process_and_wait( Command, CommandLine, Count, Timeout, Result ) :-
    create_process( Command, CommandLine, Handle ),
    (  repeat( Count ),
       wait_for_object( Handle, Timeout, Result ),
       (  Result = true
       -> true
       ;  Result = fail
       -> true
       ;  Result = timeout
       -> fail
       )
    -> true
    ;  Result = timeout
    ),
    close_handle( Handle ).
    

% eg.
% create_process( 'c:\windows\notepad.exe', '-f test.txt', Handle ).
% Fails if CreateProcessA returns 0.
% Any valid handles must be closed when finished with.

create_process( Command, CommandLine, Handle ) :-
   NPC = 16'20,
   fcreate( sinfo, [], -2, 68, -5 ),
   fcreate( pinfo, [], -2, 16, -5 ), 
   write( Command ) ~> CommandString,
   write( CommandLine ) ~> CommandLineString,
   winapi( (kernel32,'CreateProcessA'), [CommandString,CommandLineString,0,0,0,NPC,0,0,sinfo,pinfo], 0, R ),
   wintxt( pinfo, 4, 3, Str ),
   fclose( pinfo ),
   fclose( sinfo ),
   (  R \= 0
   -> strchr( Str, Pinfo ),
      Pinfo = [Handle|_]
   ).

% Tidy up used handles

close_handle( Handle ) :-
   winapi( (kernel32,'CloseHandle'), [Handle], 0, _ ).

% Wait for an object TimeOut in MilliSeconds, result will be one of
% true    if object finished
% fail    if error
% timeout if the timeout triggered.

wait_for_object( Handle, TimeOut, Result ) :-
   winapi( (kernel32,'WaitForSingleObject'), [Handle,TimeOut], 0, R ),
   ( (R,Result) = (0,true)
   ; (R,Result) = (16'102,timeout)
   ; (R,Result) = (-1,fail)
   ),
   !.

