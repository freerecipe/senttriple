% init_system_files
% this predicate inits all the necessary common predicates that are placed within system_files
% an example of system file is a predicate that generates a unique key

load_system_files :-
	nl, write( `log: load system files...` ),
	ensure_loaded( sort_ascending_second_member_of_pair ),
	ensure_loaded( genid ),
	ensure_loaded( trim ),
	ensure_loaded( intersection ),
	ensure_loaded( list_del ),
	ensure_loaded( difference ),   
	ensure_loaded( sort_asce ),
	ensure_loaded( memberance ).  



  
 

