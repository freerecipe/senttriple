
cat_with_space( [], SWord):-
	SWord = ``.
cat_with_space( [H], SWord):-
	SWord = H.
cat_with_space( StringList, SWord):-
	StringList = [ H1, H2|T],
	cat( [ H1, ` `, H2], SHWord, _),
	cat_with_space( [SHWord|T], SWord).

