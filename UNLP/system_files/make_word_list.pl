make_word_list( [], [] ):-!.
make_word_list( [H|T], [HH|TT] ):-
	H = 'Word'( A, B ),  
	member( (value, X), A ),
	atom_string( AX, X), 
	HH = AX,
	make_word_list( T, TT ).


  
