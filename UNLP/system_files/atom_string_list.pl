atom_string_list( [H],[SH]):-
	atom_string(H, SH).

atom_string_list( List, SList ):-
	List = [H|T],
	SList = [SH|ST],
	atom_string(H, SH),
	atom_string_list( T, ST ).  


