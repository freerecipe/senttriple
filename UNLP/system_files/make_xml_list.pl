% make_xml_list( -List )
% this predicate looks for the previously assereted facts in the form of parsed(A,B) and make a list out of that

make_xml_list( List ):-
	( parsed(A, B) )->
	( retract( parsed(A, B) ),
	List = [H|T],
	H = B,
	make_xml_list( T ) );
	List = [],
	!.  





