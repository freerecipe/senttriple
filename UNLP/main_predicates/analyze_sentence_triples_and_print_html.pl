analyze_sentence_triples_and_print_html( SentenceID ):-
	pretty_print_sentence( SentenceID, SentenceList, Sentence ),
	extract_main_triples( SentenceID ),
	extend_triples( SentenceID ),
	write( `<div>` ),
	write( `<p style="font-size:11px;color:black">` ),
	write( Sentence ),
	write( `</p>` ),
	write( `<p>` ),
	html_print_extended_triples,
	write( `</p>` ),
	write( `</div>` ).