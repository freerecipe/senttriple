all_parsers_parse_and_assert_sentence( Sentence, SentenceID ):-
	trim(Sentence,TrimmedSentence ),
	parse( TrimmedSentence, `stanford_parser`, SentenceParse ),
	assert_sentence_parse( SentenceParse, SentenceID, _, _ ,  _ ),
	biolg_parse( SentenceID ),
	malt_parse( SentenceID ), 
	extract_tiples_and_extend_phrases( SentenceID ),
	forall(  etriple( RuleID, SentenceID, Subject, Property, Object ), ( nl, write_tripel(RuleID, SentenceID, Subject, Property, Object ) ) ), !.

%	forall( etriple(A, B, C, D), html_write_tripel(A, B, C, D) ).
/*

	extract_tiples_and_extend_phrases( SentenceID ),
	write( `<table border="1" style="font-size:9px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	write( `<tr>` ),
	write( `<th style="font-size:5px;font-family:calibri;color:#003300"><b>Subject</b></th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:#003300"><b>Predicate</b></th>` ),
	write( `<th style="font-size:5px;font-family:calibri;color:#003300"><b>Object</b></th>` ),
	write( `<tr>` ),
	forall( etriple(A, B, C, D), html_write_tripel(A, B, C, D) ),


*/

	% edame dastan
	

% will be completed by adding other parsers
