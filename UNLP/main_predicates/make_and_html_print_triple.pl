% make_and_html_print_triple( +SentenceID, -FileName )
% this predicate extracts triples for a sentence and writes it down in a html file 

make_and_html_print_triple( SentenceID, FileName ):-
	cat( [trelex, SentenceID, '.html'], FileName, _ ),
	fcreate( out,FileName, -1, 2048, 1 ),
	output( out ),
%	extract_tiples_and_extend_phrases( SentenceID ),

	%%% NOTE: I commented the above predicate because in this way the system do sentence chunking twice which is not good
	dynamic( etriple/5 ),	% this might be removed as I we have SentenceID in triple and etriple
	dynamic( triple/5 ),

	extract_main_triples( SentenceID ),
	extend_main_triples( SentenceID ),
	extend_triple_for_conjunctions( SentenceID ), % newwwwww
	omit_repeated_words(SentenceID), % new tooo
	post_process_triples( SentenceID ),

	write( `<table border="1" style="font-size:9px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	write( `<tr>` ),
	write( `<th style="font-size:5px;font-family:calibri;color:#003300"><b>RuleID</b></th>` ),
	write( `<th style="font-size:5px;font-family:calibri;color:#003300"><b>Subject</b></th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:#003300"><b>Predicate</b></th>` ),
	write( `<th style="font-size:5px;font-family:calibri;color:#003300"><b>Object</b></th>` ),
	write( `<tr>` ),
	forall( etriple(RuleID,A, B, C, D), html_write_tripel(RuleID, A, B, C, D) ),
	write( `<table>` ),
	fclose( out ).  


% html_write_tripel( +RuleID, +SentenceID, +Subject, +Predicate, +Object )
% this predicate writes a row of an html table consists of RuleID, Subject, Predicate, and Object

html_write_tripel( RuleID, SentenceID, Subject, Predicate, Object ):-
	write( `<tr>` ),
	write( `<td>` ),
		write( RuleID ),
	write( `</td>` ),
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Subject ), 
	write( `</td>` ),
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Predicate ),
	write( `</td>` ), 
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Object ),
	write( `</td>` ),
	write( `</tr>` ).
