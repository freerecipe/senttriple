% anlyze_and_assert_section( +SectionString, +SectionType, -SectionID, -SectionParagraphLength, -SectionSentenceLength, -SectionTokenLength, -SectionFrequency )
% predicate gets SectionString, SectionType as inputs and provide an ID as a result of indexing

anlyze_and_assert_section( StringSection, SectionType, SectionID, ParaphLength ):-
	atom_string( SectionString, StringSection ),
	% trim( SectionString, TrimmedSection ),  --> there is trimming insode the paragraph to sentence
	section_to_paragraph( SectionString, ParagraphList ), 
	length( ParagraphList, ParaphLength ),

	dynamic( paragraph_position_mem/1 ),
	assert( paragraph_position_mem( 1 ) ),
	analyze_and_assert_paragraph_list( ParagraphList, ParagraphIDPositionList ),

	log_to_window(`done analyzing all paragraphs!` ),
	nl,write( ParagraphIDPositionList ),

	(
	is_indexed_section( SectionID, ParagraphIDPositionList, SectionType  )
	->	( write( `section is alrady indexed; section id is ` ), write( SectionID ) ) ~> Log,
		log_to_window( Log ),
		index_section( SectionID, _, _, Freq ),
		NewFreq is Freq + 1,
		(
		write( `UPDATE dbo_section SET section_frequency=` ), write(NewFreq), 
		write( ` WHERE section_id='` ), write( SectionID ), write( `'` ) 
		) ~> SQLUpdateQuery, 
		db_sql( SQLUpdateQuery )
	;

% do the assertion
	gen_section_id( SectionID ), 
	( write( `section is new; assigned id is ` ), write( SectionID ) ) ~> LogX,
	log_to_window( LogX ),
	db_add_record( dbo_section, [SectionID, SectionType, ParaphLength, 1 ] ),
	add_section_paragraph_records( SectionID, ParagraphIDPositionList ) 
	),
	log_to_window( `done section analyzing and asserting!` ),
	dynamic( paragraph_position_mem/1 ),!.





% analyze_and_assert_paragraph_list( +ParagraphList, -ParagraphIDPositionList )
% predicate index paragraph list and provide a list of tuples, each tuple is paragrpah id and its position in a given section

analyze_and_assert_paragraph_list( [], [] ).
analyze_and_assert_paragraph_list( [Paragprah|Rest], [(ParagraphID, PrargraphPosition)|RestIDPosition] ):-
	trim( Paragprah, TrimmedParagraph ),
	retract( paragraph_position_mem( PrargraphPosition ) ),
	NewParaphPosition is PrargraphPosition  + 1,
	assert( paragraph_position_mem( NewParaphPosition ) ),
	analyze_and_assert_paragraph( TrimmedParagraph, ParagraphID ),
	analyze_and_assert_paragraph_list( Rest, RestIDPosition).




% look if a section is indexed before



gen_section_id( SectionID ):-
	hide( Atom, 0 ),
	cat( ['sect_', Atom], SectionID, _ ).  






% is_indexed_section( -SectionID, +ParagraphIDPositionList, +SectionType, +ParaphLength  )
% look into index to see if the a section with the same spec has been previously asserted into the system

is_indexed_section( SectionID, LookupList , SectionType  ):-

	length( LookupList , Length ),	% Length of Paragraph --> How many sentences are in the Paragraph 
	(
	Length = 1	
		->
		LookupList = [(ParagraphID, 1)],
		findall( SectionID, index_section_paragraph( SectionID,ParagraphID, 1), SectionIDList ),
		choose_id_section( SectionIDList , 1, FinalID, SectionType ),
		SectionID = FinalID, 
		nl, write( `section has only one paragraph, and it is found in index repository` ), 
		!
		;

	LookupList = [(FParagraphID, FParagraphPos)|RestLookupList],
	reverse( RestLookupList, ReverseLookupList),
	ReverseLookupList = [(LastParagraphID, LastParagraphPos)|RemainList],

	% findall the IDs for paragrpahs with the sentences above in the right position
	findall( XSectionID, index_paragraph_sentence( XSectionID, FParagraphID, FParagraphPos ), List1 ),
	findall( YSectionID, index_paragraph_sentence( YSectionID, LastParagraphID, LastParagraphPos), List2 ),
	intersection( List1, List2, SectionIntersectList ),

	check_index_the_rest_section( RemainList, SectionIntersectList, CandidIDList ),
	choose_id_section( CandidIDList, Length, SectionID,SectionType )

	).


% this will do the above process recursively to get to the final intersect list
check_index_the_rest_section( [], CandidAnswerList, CandidAnswerList).
check_index_the_rest_section( [(ParagraphID, ParagraphPosition )|T], PreviousIntersectionList, IDList ):-
	findall( SectionID, index_section_paragraph( SectionID, ParagraphID, ParagraphPosition ), SectionList ),
	intersection( SectionList, PreviousIntersectionList, NewCandidList ),
	(
	NewCandidList = [] 
	->	nl, 
		write( `log: section is new!` ), 
		fail,!;
	check_index_the_rest_section( T, NewCandidList, IDList )
	).


% choode_id(paragraph, +CandidIDList,+LengthofParagraph, -FinalID)
choode_id_section( [],_, _, SectionType) :- fail.
choose_id_section( [H|T],Length, ID, SectionType ):-
	index_section( H, SectionType, Length,_Freq ),
	H = ID,
	!;
	choose_id_section( T, Length, ID, SectionType ).



add_section_paragraph_records(SectionID,[]).
add_section_paragraph_records( SectionID, [(ParphID, ParaphPosition)|ParagraphIDPositionList] ):-
	index_section_paragraph( SectionID, ParphID, ParaphPosition )
	-> add_section_paragraph_records( SectionID, ParagraphIDPositionList );
	db_add_record( dbo_section_paragraph, [SectionID, ParphID,ParaphPosition] ),
	add_section_paragraph_records( SectionID, ParagraphIDPositionList ).
















