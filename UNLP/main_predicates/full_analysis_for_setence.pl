% all_parsers_parse_and_assert_sentence_and_html_print( +Sentence, -SentenceID, -FileName )
% this predicate parse a sentence for all three available parsers, and extract relations, and provide an html file as a result of analysis 
full_analysis_for_setence( Sentence, SentenceID, FileName ):-
	trim(Sentence,TrimmedSentence ),
	parse( TrimmedSentence, `stanford_parser`, SentenceParse ),
	assert_sentence_parse( SentenceParse, SentenceID, _, _ ,  _ ),
	biolg_parse( SentenceID ),
	malt_parse( SentenceID ), !,
	extract_tiples_and_extend_by_phrases( SentenceID ),
	html_print_to_file_of_all_analysis2( SentenceID, FileName ).   


  
