%
% this predicate prints out html representation of sentence in html format
% html_to_file_print_of_comparative_malt_stanford_biolg_matrix( SentenceID ):-

% html_print_to_file_of_all_analysis( +SentenceID, -FileName )
%
make_and_html_print_to_file_of_all_analysis( SentenceID, FileName ):-
%	pretty_print_sentence( SentenceID, _, SentenceString ),
	html_to_file_print_of_comparative_malt_stanford_biolg_matrix( SentenceID, MatrixFileName ),
	make_and_pretty_print_html_sentence_chunks( SentenceID, ChunksFileName ),
	make_and_html_print_triple( SentenceID, TripleFileName ),
	cat( ['hmn',SentenceID,'rst.html'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),
%	write( `<html>` ),
%	write(`<head><title>` ),
%	write( ` Sentence Analysis (id:`), write( SentenceID ),write(`)`),
%	write(`</title></head>` ),
	write( `<frameset rows="60%, 20%,20%">` ),
	write( `<frame src="`), write( MatrixFileName ), write( `" />` ),
	write( `<frame src="`), write( ChunksFileName ), write( `" />` ),
	write( `<frame src="`), write( TripleFileName ), write( `" />` ),
	write( `</frameset>` ),
%	write( `</html>` ),
	fclose( html ),!.  



