% make_and_pretty_print_html_sentence_chunks( +SentenceID, -FileName )
% this predicate prints extracts and prints sentence chunks for a given sentenceID into a given html file
make_and_pretty_print_html_sentence_chunks( SentenceID, FileName ):-
	sentence_chunking( SentenceID),
	cat( ['chnk',SentenceID,'y.html'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),
	write( `<table border="1" style="font-size:10px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	write( `<tr>` ),
	write( `<th style="font-size:7px;font-family:calibri;color:#003300">` ), write( `Index List` ), write( `</th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:red">Phrase</th>` ),
	write( `<th style="font-size:7px;font-family:calibri;color:#003300">` ), write( `Grammatical` ), write( `</th>` ),
	write( `<th style="font-size:7px;font-family:calibri;color:#003300">` ), write( `Position List` ), write( `</th>` ),
	write( `<tr>` ),
	forall( sentence_chunk( SentenceID,CellIndexNumber, Lexemes, PoS, PositionList ),
	( write(`<tr>`), write(`<td>` ), write( CellIndexNumber ),  write(`</td>` ),  write(`<td>` ), write_lexeme_list( Lexemes),  write(`</td>` ),  write(`<td>` ),write(PoS),  write(`</td>` ), write(`<td>` ), write( PositionList ), write(`</td>` ),write(`</tr>`) )),
	write( `</table>` ),
	close(html).
