% init for doc_index system
init_parsi :-
%	ensure_loaded( init_mem_size ), % to be moved to consulting files
%	init_mem_size,	
	xinit(256,256,256,1024,8192,32768,256,1024,1024),
	start_dialogue,
	connect_index_repository,
	servers_config,
	init_parsi_server,
	working_directory( Path ), 
	chdir( Path ),
	( write( `working directory set to ` ), write( Path ) )~> LogX,
	log_to_window(LogX).
  



 


