% malt_parse_server( +SentenceID )
% this predicate parses an input sentence (indicated by its given ID ) using the maltParser
% the maltParser is run remotely

malt_parse( SentenceID ):-
	prepare_malt_parser_input(SentenceID, MaltInputFile), 
	server( [ (name, `malt_parser`), (process_type, `parser`), ('IP', IP), ('port', Port )], [] ), 
	screate( malt_parser, (IP, Port ) ),
      repeat,
      sstat( malt_parser, Stat ),
      cmp( 1, Stat, 0 ),
      repeat,
      ssend( malt_parser, MaltInputFile),
	% get the result back
	log_to_window( `waiting for parse result from Malt Parser ... This is trivial implementation ... Sorry, will take some time!` ),
	(
	repeat,
      srecv( malt_parser, Data ),
      write( Data ),
      Data = ``
   	-> sclose( malt_parser )
   	) ~> Text,
	( write( `Malt parsing done for ` ), write( SentenceID ) )~> Log,
	log_to_window( Log ),
	input( (Text, 0) ),
	!,
	read_assert_malt_input(SentenceID),
	input( 0 ),
	( write( `Done asserting parse results for ` ), write( SentenceID ) )~> Log2,
	log_to_window( Log2 ).


