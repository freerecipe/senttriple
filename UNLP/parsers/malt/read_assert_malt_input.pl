% read_assert_malt_input( SentenceID )
% this predicate assert the parse result into database
% the input stream should be set beforehand

read_assert_malt_input( SentenceID ) :-
	fread( s, 0, 0, Line ),
	( ( Line = `~J`; Line = `~M~J`)
	->	log_to_window( `Output analysis of MaltParser; end of generated CoNLL String` ),!;
	tokenize_conll_row( Line , fact( GovernerPosition, RegentPosition, DependencyType) ),
	( GovernerPosition = 0 
		->	true; 
		assert_malt_output( SentenceID, fact(GovernerPosition, RegentPosition, DependencyType)) 
	),
	read_assert_malt_input( SentenceID )
	).



assert_malt_output( SentenceID, fact( GovernerPosition, RegentPosition, DependencyType) ):-
	index_sentence_lexeme( SentenceID, GovLexemeID, GovernerPosition ),
	index_sentence_lexeme( SentenceID, RegLexemeID, RegentPosition),
	
	assert_malt_dependency_lexemes( DependencyLexemesID, DependencyType, GovLexemeID, RegLexemeID ),
	assert_sentence_malt_dependencies( SentenceID, DependencyLexemesID, GovernerPosition, RegentPosition ).





% assert_sentence_malt_dependencies( -DependencyLexemesID, +DependencyType, +GovLexemeID, +RegLexemeID )
assert_malt_dependency_lexemes( DependencyLexemesID, DependencyType, GovLexemeID, RegLexemeID ):-
	index_malt_dependency_lexemes( DependencyLexemesID, DependencyType, GovLexemeID, RegLexemeID, Freq )
	->	NewFreq is Freq + 1,
		(
		write( `UPDATE dbo_malt_dependency_lexemes SET dependency_frequency=` ), write(NewFreq), 
		write( ` WHERE dependency_lexemes_id='` ), write( DependencyLexemesID ), write( `'` ) 
		) ~> SQLUpdateQuery, 
		db_sql( SQLUpdateQuery ),
		!;
	gen_malt_dep_lex_id( DependencyLexemesID ),
	db_add_record( dbo_malt_dependency_lexemes, [DependencyLexemesID, DependencyType,GovLexemeID, RegLexemeID, 1] ).


gen_malt_dep_lex_id( ID ):-
	hide( Atom, 0 ),
	cat( ['dlmlt_', Atom], ID, _ ). 

	
assert_sentence_malt_dependencies( SentenceID, DependencyLexemesID, GovernerPosition, RegentPosition ):-
	index_sentence_malt_dependencies( SentenceID, DependencyLexemesID, GovernerPosition, RegentPosition )
	-> !;
	db_add_record( dbo_sentence_malt_dependencies, [SentenceID, DependencyLexemesID, GovernerPosition, RegentPosition] ).

