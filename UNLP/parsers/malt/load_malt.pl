% this predicate loads files related to malt parser
load_malt :-
%	log_to_window( `load malt parser files... ` ),
	nl, write( `load malt parser files... ` ),
	ensure_loaded( prepare_malt_parser_input ),
	ensure_loaded( tokenize_conll_row ),
	ensure_loaded( malt_parse ),
	ensure_loaded( read_assert_malt_input ),
	nl, write( `done loading malt parser...` ).

%	log_to_window( `done loading malt parser...` ).
