% prepare_malt_parser_input(+SentenceID, MaltInputFile)
% This predicate gets a sentence ID and provides an output in the form of tabbed CoNLL that can be used for parse
prepare_malt_parser_input(SentenceID, MaltInputFile):-
(	forall( index_sentence_lexeme( SentenceID, LexemeID, LexemePos ),
	(	index_lexeme( LexemeID, String, PennPoS, _, _),
		write(LexemePos), write( `~I`), write( String), write( `~I`), write( `_`), write( `~I`), write( PennPoS ), write( `~I`), write( PennPoS ), write( `~I`), write( `_~M~J`)
	)) ) ~> MaltInputFile.
	