% tokenize_conll_row( +String, -fact( GovernerPosition, RegentPosition, DependencyType) )
% this predicate tokenize an input string from CoNLL row to a fact as shown above

tokenize_conll_row( String, fact( GovernerPosition, RegentPosition, DependencyType) ):-
	conll_tokenize(String, Out),
	Out = [ARegentPosition,_,_,_,_,_,AGovernerPosition,DependencyType,_,_],
	number_atom( RegentPosition, ARegentPosition),
	number_atom( GovernerPosition, AGovernerPosition).
%	nl, write( Out ).
	



conll_tokenize(String, Out):-
                                string_chars(String,Chars),
                                conll_tokenize1(Chars, Out). 

conll_tokenize1(L , [Word | Out]):-
                        L \==[],
                        conll_tokenize1(L ,Rest,WordChs),
                        atom_chars(Word,WordChs),
                        conll_tokenize1(Rest,Out).
                        % also possible to search for lexicon here

conll_tokenize1([],[]):- !.

% separators comes here
conll_tokenize1([],[],[]):-!.
conll_tokenize1([9|T],T,[]):-!.

conll_tokenize1([H|T],Rest,[H|List]):-
                               conll_tokenize1(T,Rest,List).





conll_tokenize_atom_input(Atom, Out):-
                                atom_chars(Atom,Chars),
                                conll_tokenize1(Chars, Out). 
