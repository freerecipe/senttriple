generate_xml_input_for_biolg( SentenceID, InputXML ):-
	(
	write( `<sentences>` ),
	write( `<sentence>` ),
	% CAUTION:  this is very important; I have asssumed that lexemes are index based on their position in the sentence!!!!
	% CAUTION:
	forall( index_sentence_lexeme( SentenceID, LexemeID, Pos ),
			(
			index_lexeme( LexemeID, String, PoS, _,_),
			nl, write( `<w c="`), write( PoS),write( `">`), write( String ), write( `</w>` )
			)
		),
	write( `</sentence>` ),
	write( `</sentences>~M~J` ) )~> InputXML, !.  


/*
	This predicate wont be used any more!

% generate_to_file_xml_input_for_biolg
% this is an auxiliary predicate for generating all xml inputs to BioLG
generate_to_file_xml_input_for_biolg :-
	forall( index_sentence( SentenceID, _, _,_),
	(
		generate_xml_input_for_biolg( SentenceID, InputXML ),
		cat( [SentenceID, '.xml'], FileName,_ ),
		fcreate( biolg_xml,FileName, -1, 2048, 1 ),
		output( biolg_xml ),
		write( InputXML ),
		fclose( biolg_xml )
	)),
	nl, write( `done` ).  

 */


