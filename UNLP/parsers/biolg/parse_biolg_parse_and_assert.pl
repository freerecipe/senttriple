% parse_biolg_parse_and_assert( +SentenceID, +Parse )
% this predicate gets SentenceID and provided parse by BioLg (in XML format ) and assert it into index repository; this is only done for links and not for constituents

parse_biolg_parse_and_assert( SentenceID, Parse ):-
	input( (Parse, 0 ) ), % redirect input stream
	xml_read( _, 0, XML ),
	( 
	XML = [sentences( SentencesAttrib, SentencesElement )]
	->	% more parse commands
		
		SentencesElement = [sentence(_SentenceAttrib, SentenceElement)|_RestofSentences],	% recursion on RestofSentences may be necessary when we pass more than one sentence to parser (this also should be checked for some other circumstances)
		SentenceElement = [linkages(_LinkagesAttrib, LinkagesElement)|_RestofLinkages ],	% recursion on linkages looks unnecessary
		LinkagesElement = [linkage(_LinkageAttrib, LinkageElement)|_RestOfLinkage],	% If the parser be asked to provide more than one parse
		LinkageElement = [ Words, _Terms, links([],Links), _Constituents],				% This is not going to be changed and can be consider as the condition for the end of recursion
	
		% for the moment I discar Link Constituents as they are simmilar information to Charniak and Penn Style Parse
		% even I think it is possible to discard words bit! as I am going to provide XML input tokenized to BioLG
		% nl, write( Words ),
		process_and_assert_biolg_links( SentenceID, Links ),
		nl, write( `done asseerting links` ), !
	;
	log_to_window( `invalid biolg xml input!` ),
	 ! ),
	input(0).
	
%	split_biolg_xml( Words, Links ),
%	provide_list_of_lexemes(


% process_and_assert_biolg_links( +SentenceID, +LinkList )
% this predicate assert all links for a sentence and between lexemes respectivly into dbo_sentence_links and dbo_linked_lexemes
process_and_assert_biolg_links( SentenceID, [] ):-
	( write( `End of assertion of BioLG parse facts for the sentence #` ), write( SentenceID ) )~> Log,
	log_to_window( Log ).

process_and_assert_biolg_links( SentenceID, [H|T] ):-
	H = l([(from, SFrom), (to, STo ), (t, Type )],[]),
	number_string( NFrom, SFrom ),
	number_string( NTo, STo ),

	% normalizing index posittion ( BioLG starts from 0 while we have that from 1 in our system
	From is NFrom + 1,
	To is NTo + 1,

	index_sentence_lexeme( SentenceID, FromLexemeID, From ), 
	index_sentence_lexeme( SentenceID, ToLexemeID, To ), !,

	assert_linked_lexemes( LinkID, Type, FromLexemeID, ToLexemeID ),
	assert_sentence_links( SentenceID, LinkID, From, To ),
	process_and_assert_biolg_links( SentenceID, T ).



% assert_linked_lexemes( -LinkID, +Type, +FromLexemeID, +ToLexemeID )
% This predicate assert a Link between two lexemes; if the link is already exists then update the frequency and give back the id

assert_linked_lexemes( LinkID, Type, FromLexemeID, ToLexemeID ):-
	index_linked_lexemes( LinkID, Type, FromLexemeID, ToLexemeID, Freq )
	-> 	log_to_window( `updating Frequency for a link between lexemes` ),
		NewFreq is Freq + 1,
		(
		write( `UPDATE dbo_linked_lexemes SET dependency_frequency=` ), write(NewFreq), 
		write( ` WHERE dependency_lexemes_id='` ), write( LinkID), write( `'` ) 
		) ~> SQLUpdateQuery, 
		db_sql( SQLUpdateQuery ),
		!;
	gen_linked_lexemes_id( LinkID ),
	db_add_record( dbo_linked_lexemes, [LinkID , Type,FromLexemeID, ToLexemeID , 1] ).


gen_linked_lexemes_id( ID ):-
	hide( Atom, 0 ),
	cat( ['dbiolnk', Atom], ID, _ ). 

	
assert_sentence_links( SentenceID, DependencyLexemesID, GovernerPosition, RegentPosition ):-
	index_sentence_links( SentenceID, DependencyLexemesID, GovernerPosition, RegentPosition )
	-> !;
	db_add_record( dbo_sentence_links, [SentenceID, DependencyLexemesID, GovernerPosition, RegentPosition] ).   












