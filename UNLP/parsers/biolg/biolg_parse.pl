biolg_parse( SentenceID ):-
	generate_xml_input_for_biolg( SentenceID, InputXML ),
	log_to_window( `proper input for BioLG is created!` ),
	server( [ (name, `biolg`), (process_type, `parser`), ('IP', IP), ('port', Port )], [] ), 
	screate( biolg, (IP, Port ) ),
      repeat,
      sstat( biolg, Stat ),
      cmp( 1, Stat, 0 ),
      repeat,
      ssend( biolg, InputXML ),
	% get the result back
	log_to_window( `Parse request were sent!` ),
	(
	repeat,
      srecv( biolg, Data ),
      write( Data ),
      Data = ``
   	-> sclose( biolg )
   	) ~> Text,
	( write( `BioLG parse done for sentence ID: ` ), write( SentenceID ) )~> Log,
	log_to_window( Log ),
	parse_biolg_parse_and_assert( SentenceID, Text ),
	( write( `Done asserting BioLg parse results for ` ), write( SentenceID ) )~> Log2,
	log_to_window( Log2 ).

