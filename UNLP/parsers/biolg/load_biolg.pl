% load files necessary for bioLG parsing!
load_biolg:-
	nl, write( `load bioLG parser files... ` ),
	ensure_loaded( generate_xml_input_for_biolg ),
	ensure_loaded( parse_biolg_parse_and_assert ),
	ensure_loaded( biolg_parse ),
	nl, write( `done loading bioLG parser...` ).
