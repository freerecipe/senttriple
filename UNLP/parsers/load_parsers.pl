% load files necessary for bioLG parsing!

load_parsers :-
	nl, write( `log: loading parse predicates...`  ),
	chdir( Current ),
	chdir( 'biolg' ),
	ensure_loaded( load_biolg ),
	load_biolg,
	
	chdir( Current ),
	chdir( malt ),
	ensure_loaded( load_malt ),
	load_malt,

	chdir( Current ),
	chdir( stanford ),
	ensure_loaded( load_stanford ),
	load_stanford,
	
	chdir( Current ),

	nl, write( done ).


	 

