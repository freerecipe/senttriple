% depend( Dependency ID, Dependency Super Cleass,DependencyNameSpace).
depend( 0, 0, stanford_dependencies, `http://nlp.stanford.edu/software/dependencies_manual.pdf` ).
depend( 1, 0, aux,  `auxiliary` ).
	depend( 101, 1, auxpass, `passive auxiliary` ).
	depend( 102, 1, cop, `copula` ).
depend( 2, 0, arg, `argument` ).
	depend( 201, 2, agent, `agent` ).
	depend( 202, 2, comp, `complement` ).
		depend( 2021, 202, acomp, `adjectival complement` ).
		depend( 2022, 202, attr, `attributive` ).
		depend( 2023, 202, ccomp, `clausal complement with internal subject` ).
		depend( 2024, 202, xcomp, `clausal complement with external subject` ).
		depend( 2025, 202, compl, `complementizer` ).
		depend( 2026, 202, obj, `object` ).
			depend( 20261, 2026, dobj, `direct object ` ).
			depend( 20262, 2026, iobj, `indirect object` ).
			depend( 20263, 2026, pobj, `object of preposition` ).
		depend( 2027, 202, mark, `marker (word introducing an advcl)` ).
		depend( 2028, 202, rel, `relative (word introducing a rcmod)` ).
	depend( 203, 2, subj, `subject` ).
		depend( 2031, 203, nsubj, `nominal subject` ).
			depend( 20311, 2031, nsubjpass,  `passive nominal subject` ).
		depend( 2032, 203, csubj, `clausal subject` ).
			depend( 20321, 2032, csubjpass, `passive clausal subject` ).
depend( 3, 0, cc, `coordination` ).
depend( 4, 0, conj, `conjunct` ).
depend( 5, 0, expl, `expletive` ).
depend( 6, 0, mod, `modifier` ).
	depend( 601,6, abbrev, `abbreviation modifier` ).	depend( 602,6, amod, `adjectival modifier` ).	depend( 603,6, appos, `appositional modifier` ).	depend( 604,6, advcl, `adverbial clause modifier` ).	depend( 605,6, purpcl, `purpose clause modifier` ).	depend( 606,6, det, `determiner` ).
	depend( 607,6, predet, `predeterminer` ).
	depend( 608,6, preconj, `preconjunct` ).
	depend( 609,6, infmod, `innitival modifier` ).
	depend( 610,6, partmod, `participial modifier` ).
	depend( 611,6, advmod, `adverbial modifier` ).		depend( 6111,611, neg, `negation modifier` ).
	depend( 612,6,rcmod, `relative clause modifier` ).	depend( 613,6,quantmod, `quantifier modifier` ).	depend( 614,6,tmod, `temporal modifier` ).	depend( 615,6,measure, `measure-phrase modifier` ).	depend( 616,6,nn, noun, `compound modifier` ).	depend( 617,6,num, `numeric modifier` ).	depend( 618,6,number, `element of compound number` ).
	depend( 619,6,prep, `prepositional modifier` ).	depend( 620,6,poss, `possession modifier` ).	depend( 621,6,possessive, `possessive modifier('s)` ).
	depend( 622,6,prt, `phrasal verb particle` ).
depend( 7, 0, parataxis, `parataxis` ).
depend( 8, 0, punct, `punctuation` ).
depend( 9, 0, ref, `referent` ).
depend( 10, 0, sdep, `semantic dependent` ).  
	depend( 1001, 10, xsubj, `controlling subject` ).  

 
dump_dependencies_to_db :-
	forall( depend( A, B, C, D ),
	(
		nl, write( `adding/updating ` ), write( [A, B, C, D] ),
		( dependencies( A, B, C, D ) -> true; db_add_record( dbo_dependencies, [A, B, C, D] ) )
	)
	).
 

