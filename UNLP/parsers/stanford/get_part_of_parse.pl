
% get_penn_style_parse( +XML, ?PennParse )
% extract the penn style part of the stanford parser, if it could not find any penn style parse then the PennParse will be an empty string otherwise
% it will be a string represent the penn style parse. This predictae depends on the scheme of input xml and for the time being it only gets stanford
get_part_of_parse( penn_style, XML, PennParse ):-
	input( (XML, 0 ) ),
	repeat,
	xml_token( A, B ),
	( A = tree([(style,`penn`)]) 
	->	xml_read( _, _, [PennParse] ), !;
	( (A = [], B = -1 )-> write( `end of file` ),PennParse = ``, !)
		).  



% get_part_of_parse( typed_dependencies, +ParseXML, -Dependencies )
% the predicate gets the parse results and look for typed dependencies of stanford in the given parse
% and gives back a list of dependencies
get_part_of_parse( typed_dependencies, XML, Dependencies ):-
	input( (XML, 0 ) ),
	repeat,
	inpos( Position ),
	xml_token( A, B ),
	( A = dependencies([(style,`typed`)]) 
	->	inpos( Position ), xml_read( _, _, RootDependencies ),
		RootDependencies = [dependencies([(style,`typed`)],Dependencies )],  !;
	( (A = [], B = -1 )-> write( `end of file` ),Dependencies= ``, !)
		).  
