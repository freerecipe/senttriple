parse( Sentence, Parser, XML ):-
	log_to_window( `requesting parse from Stanford Parser` ),
	% prepare socket at client
	screate( cp, 43431 ),

	% nl, write( `NLP agent initiated at 43431`), 

	% prepare input to parse server --> for stanford, input must be ended by carriage return charecter
	% this may be more complicated process such as preparing XML for a parser
	( write( Sentence ), write( ` ~M~J`) ) ~> Input,

	% make connection to server

	% find proper server
	server( [ (name, Parser), (process_type, `parser`), ('IP', IP), ('port', Port )], [] ),

	screate( cp, (IP, Port) ),
	% nl, write( `successful connection to ` ), write( Parser ), write( ` `), write( IP ), write( ` -> #` ), write( Port ), 
	 
	% ->


	% wait for stable socket connection	
      repeat,
      sstat( cp, Stat ),
      cmp( 1, Stat, 0 ),

	% send the input or propoer request to server	
      repeat,
      ssend( cp, Input ),

	% recieve the parse result from the socket
      
	(
	repeat,
	srecv( cp, Data ),
	write( Data ),
	Data = ``-> sclose( cp ) 	% close connection

      )
	~> XML,
	!.  

  


