
% note: before running the predicate db should be connected via ODBC and the named odbc link: index_repository
connect_index_repository:-
	ensure_loaded( system(dblink) ),
	db_connect(access(index_repository)),
	db_col_spaces(access, `"`, `"`),
	attach_index_repository_tables,
	db_flag( show_sql, _, off ),
	log_to_window( `log: successfuly connected to index repository` ).  


 





  
