load_index_repository_interface :-
	nl, write( `log: loading index repository interface files...` ), 
	ensure_loaded( attach_index_repository_tables ),
	ensure_loaded( connect_index_repository ),
	write( `log: DONE! ` ). 