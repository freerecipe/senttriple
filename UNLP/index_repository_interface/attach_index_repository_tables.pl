% attach_index_repository_tables
% This predicate provides an interface to index repository presented by odbc layer
% we may replace the db_attach predicate with facts within prolog when it is approciate

attach_index_repository_tables :-
	db_attach(index_section, 'dbo_section' ),
	db_attach(index_paragraph, 'dbo_paragraph' ),
	db_attach(index_sentence, 'dbo_sentence'),	% all sentences, meant to be stored once 
	db_attach(index_phrase, 'dbo_phrase'),	% all the phrases: phrases are stored only once
	db_attach(index_lexeme, 'dbo_lexeme'), %  all the lexemes: lexemes are stored only once
	db_attach(index_section_paragraph, 'dbo_section_paragraph' ),
	db_attach(index_paragraph_sentence, 'dbo_paragraph_sentence' ),
	db_attach(index_sentence_phrase, 'dbo_sentence_phrase' ), % connect phrases to sentences, tells what are phrases within one sentence
	db_attach(index_sentence_lexeme, 'dbo_sentence_lexeme'),	% connect sentence to lexemes, tell what are the words(lexemes) within is a sentence and what is the location of sentence
	db_attach(index_sentence_dependencies, 'dbo_sentence_dependencies' ), % dependncy instances for a given sentence
	db_attach(index_phrase_lexeme, 'dbo_phrase_lexeme'),	% connect lexemes and phrases, tell that each phrase is composed of what lexemes and what is the position lexeme
	db_attach(index_dependency_lexemes, 'dbo_dependency_lexemes' ), % depndencies that has been generated between two lexemes in different situations
	db_attach(index_dependencies, 'dbo_dependencies' ),	% the hierarchy of stanford dependencies
	db_attach(index_malt_dependency_lexemes, 'dbo_malt_dependency_lexemes' ),	% malt dependencies between lexemes
	db_attach(index_sentence_malt_dependencies, 'dbo_sentence_malt_dependencies' ),	% malt dependencies for a sentence
	db_attach(index_linked_lexemes, 'dbo_linked_lexemes' ),	% links (link-grammar) between lexemes
	db_attach(index_sentence_links, 'dbo_sentence_links' ).	% links for sentences
	


  




	