% write_lexeme_position_list( +SentenceID, +LexemePostionList )
% this predicate writes a string of provided LexemePositionList for a SentencID
write_lexeme_position_list( SentenceID, [] ).
write_lexeme_position_list( SentenceID, [H] ):-
	aux_index_sentence_position_word( SentenceID, H, String ),
	write( String ).

write_lexeme_position_list( SentenceID, [H|T] ):-
	aux_index_sentence_position_word( SentenceID, H, String ),
	write( String ), write(` `), write_lexeme_position_list( SentenceID, T ).


aux_index_sentence_position_word( SentenceID, Position, String ):-
	index_sentence_lexeme( SentenceID, LexemeID, Position ),
	index_lexeme( LexemeID, String, _, _,_).
