% pretty_print_list( +StringList, -String )
% this predicate generates a string from a list
% all sort of provision for writing a list can be applied here! for example, treating the punctuations, etc. 
pretty_print_list([],`` ).
pretty_print_list([(H, _)|T],String ):-
	index_lexeme( H, NString, _PoS, _Ana, _Freq ),
%	number( NString ) 
%	-> number_atom( NString, String ); String = NString ).
	( write( NString ), pretty_print_list( T ) ) ~> String.	


/*
pretty_print_list([(H, _)|T], Sentence ):-
	index_lexeme( H, String, _PoS, _Ana, _Freq ),
	pretty_print_list( T, TString ),
	( number( String )
	 ->	number_atom( String, AString ),
		cat( [AString, ' ', TString], Sentence );
	cat( [String, ' ',TString], Sentence, _ ) ).

*/

pretty_print_list( [] ).
pretty_print_list( [(H, _)|T] ):-
	index_lexeme( H, String, _PoS, _Ana, _Freq ),
	( String = '-LRB-' -> write(`(`), pretty_print_list( T ),!;
	  String = '-RRB-' -> write(`)`), pretty_print_list( T ),!;
	  write(` `), write( String ), pretty_print_list( T ) ). 


	



	
