% write_lexeme_list/1
% write the lexeme strings in the standard output from privded LexemeIDs
write_lexeme_list( [] ).
write_lexeme_list( [(LexemeID, _)|LexemeList] ):-
	index_lexeme( LexemeID, String, _, _,_),
	write( String ), write( ` ` ),
	write_lexeme_list( LexemeList ).



