% simple print of triples in the console
% previously asserted triples in the memory will be printed out in standard output
print_triples:-
	forall( triple(RuleID, SentenceID, A, B,C), 
	(	
		print_triple_bits( SentenceID, A, String1 ),
		print_triple_bits( SentenceID, B, String2 ),
		print_triple_bits( SentenceID, C, String3 ),
		nl, write( `simple triple:: ` ), write( RuleID ), write(`~I` ), write( String1 ), write( `,` ), write( String2 ), write( `,` ), write( String3 ), write( ` ` )
	)).  



