% pretty_print_phrase( +PhraseID, -PhraseList, -String )
% this predicate provides list of lexemes and String for a given phrase id
pretty_print_phrase( PhraseID, Sorted, String ):-
	findall( (LexemeID, LexemePosition ), index_phrase_lexeme( PhraseID, LexemeID, LexemePosition ), PhraseList ),
	sort_ascending_second_member_of_pair(PhraseList,Sorted),
	pretty_print_list(Sorted, String ).
