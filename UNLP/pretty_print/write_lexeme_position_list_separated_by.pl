% write_lexeme_position_list_separated_by( +SeparateCharecter, +SentenceID, +[] )
% this predicates writes a lexeme list into current output stream, with the SeparateCharecter between them

write_lexeme_position_list_separated_by( SeparateCharecter, SentenceID, [] ).
write_lexeme_position_list_separated_by( _, SentenceID, [H] ):-
	aux_index_sentence_position_word( SentenceID, H, String ),
	write( String ).

write_lexeme_position_list_separated_by( SeparateCharecter, SentenceID, [H|T] ):-
	aux_index_sentence_position_word( SentenceID, H, String ),
	write( String ), write(SeparateCharecter), write_lexeme_position_list_separated_by( SeparateCharecter, SentenceID, T ).

