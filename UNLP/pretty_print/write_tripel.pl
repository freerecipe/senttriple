% write_tripel( +RuleID, +SentenceID, +Subject, +Predicate, +Object )
% This predicate write lists of position lexemes provided as Subject, Predicate, Object into console

%	forall(  etriple( SentenceID, Subject, Property, Object ), (nl, write(triple(SentenceID, Subject, Property, Object )) ) ).
write_tripel( RuleID, SentenceID, Subject, Predicate, Object ):-
	nl, write( RuleID ), write( `~I` ),
	write_lexeme_position_list( SentenceID, Subject ), write( `~I` ),
	write_lexeme_position_list( SentenceID, Predicate ), write( `~I` ),
	write_lexeme_position_list( SentenceID, Object ).   



