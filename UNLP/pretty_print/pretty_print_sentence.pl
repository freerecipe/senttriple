% pretty_print_sentence( +SentenceID, -SentenceList, -Sentence )
% this predicate gets a sentence ID and it provides the list of (lexeme, positions) in addition to a string equvalent to sentence

pretty_print_sentence( SentenceID, SentenceList, Sentence ):-
	aux_find_all_lexeme_position( SentenceID, SentenceList ),
	sort_ascending_second_member_of_pair(SentenceList ,SortedList),
	pretty_print_list(SortedList, Sentence ),!.

aux_find_all_lexeme_position( SentenceID, SentenceList ):-
	findall( (Lexeme, Position), index_sentence_lexeme( SentenceID, Lexeme, Position ), SentenceList ),!.

