% print_triple_bits( +SentenceID,+ListofWordsPositions, -String )
% this prints a list of given word positions for a given sentence ID

print_triple_bits( _,[], '' ).
print_triple_bits( SentenceID, [H|List], String ):-
	give_string( SentenceID, H, String1 ),
	print_triple_bits( SentenceID, List, String2 ),
	cat( [String1, ' ', String2], String, _ ).


% give string form of word based on position in sentence
give_string( SentenceID, WordPosition, String ):-
	WordPosition = 0 
	-> String = 'X';
	index_sentence_lexeme( SentenceID, LexmeID, WordPosition ),
	index_lexeme( LexmeID, String, _,_,_).

