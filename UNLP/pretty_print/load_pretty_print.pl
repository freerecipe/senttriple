load_pretty_print:-
	nl, write( `load pretty print files...` ),
	ensure_loaded( pretty_print_list ),
	ensure_loaded( pretty_print_paragraph ),
	ensure_loaded( pretty_print_phrase ),
	ensure_loaded( pretty_print_sentence ),
	ensure_loaded( pretty_print_sentence_chunks ),
	ensure_loaded( pretty_print_sentence_dependencies ),
	ensure_loaded( print_extended_triples ),
	ensure_loaded( print_triples ),
	ensure_loaded( print_triple_bits ),
	ensure_loaded( write_lexeme_list ),
	ensure_loaded( write_lexeme_position_list ),
	ensure_loaded( write_tripel ),
	ensure_loaded( write_lexeme_position_list_separated_by ),
	nl, write( `done!` ).  


