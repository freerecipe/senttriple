% pretty_print_sentence_chunks( +SentenceID )
% this predicate writes sentences chunks in standard console for a given sentence ID (previously asserted in the memory)

pretty_print_sentence_chunks( SentenceID ):-
	forall( sentence_chunk( SentenceID, CellIndexNumber, Lexemes, PoS, PositionList ),
	( nl, write( CellIndexNumber ), write( `~I` ), write_lexeme_list( Lexemes), write( `~I` ),write(PoS), write(`~I`), write( PositionList ) )).
