% print_extended_triples
% simple print of previously asserted facts in the memory. 
% exetended_triple/5 will be printed in standard output


print_extended_triples:-
	forall( extended_triple(RuleID, SentenceID, A, B,C), 
	(	
		print_triple_bits( SentenceID, A, String1 ),
		print_triple_bits( SentenceID, B, String2 ),
		print_triple_bits( SentenceID, C, String3 ),
		nl, write( `Extended triple:: ` ), write( RuleID), write( `~I` ), write( String1 ), write( `,` ), write( String2 ), write( `,` ), write( String3 ), write( ` ` )
	)).	



 

