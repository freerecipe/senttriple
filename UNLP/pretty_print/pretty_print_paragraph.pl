pretty_print_paragraph( ParagrpahID, ParagpraStringListhList, Paragraph ):-
	dynamic( aux_list/1 ),
	assert( aux_list([]) ),
	( forall( SentenceID^index_paragraph_sentence( ParagrpahID, SentenceID, _ ),
			( pretty_print_sentence( SentenceID, SentenceList, Sentence ),
			  retract( aux_list(PrevList) ),
			  append( PrevList, SentenceList, NewList ),
			  assert( aux_list( NewList ) ),
			  write( Sentence )
			 )) )~> Paragraph,

	retract( aux_list(ParagpraStringListhList) ),
	dynamic( aux_list/1 ), !.
 
