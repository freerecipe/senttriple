% pretty_print_sentence_dependencies( +SentenceID, +Source, -SentenceDependencyList, -DependencyPrettyPrint )
% This predicate provides list of dependencies and a pretty print based for a given Sentence ID and source of dependency
% The source of dependency can be either Stanford, Malt and ...

pretty_print_sentence_dependencies( SentenceID, malt, DependencyList , DependencyPrettyPrint ):-
	findall( ( DepLexID, GovPosition), index_sentence_malt_dependencies( SentenceID, DepLexID, GovPosition, DepPosiiton ), DependencyList ),
%	findall( DepLexID, index_sentence_malt_dependencies( SentenceID, DepLexID, GovPosition, DepPosiiton ), SentenceDependencyList ),
	sort_ascending_second_member_of_pair(DependencyList,Sorted),	% this is to have dependencies based on the position of governor rather than regent
	pretty_print_malt_dependencies( Sorted, DependencyPrettyPrint ).
		

pretty_print_malt_dependencies( [], `` ).
pretty_print_malt_dependencies( [(H, _GovPoS)|T], DependencyPrettyPrint ):-
	aux_pretty_print_malt_dep_1( H, DepType, GovString, RegString),
	( nl,write( GovString ), write( `~I` ), write(DepType), write( `~I` ), write( RegString)  ) ~> HPrettyList,
	pretty_print_malt_dependencies( T, DependencyPretty ),
	( write( HPrettyList ), write( DependencyPretty ) )~> DependencyPrettyPrint.

	
	
pretty_print_sentence_dependencies( SentenceID, stanford, SentenceDependencyList, DependencyPrettyPrint ):-
	findall( ( DepLexID, GovPosition), index_sentence_dependencies( SentenceID, DepLexID, GovPosition, DepPosiiton ), DependencyList ),
	findall( DepLexID, index_sentence_dependencies( SentenceID, DepLexID, GovPosition, DepPosiiton ), SentenceDependencyList ),

	sort_ascending_second_member_of_pair(DependencyList,Sorted),	% this is to have dependencies based on the position of governor rather than regent
	pretty_print_dependencies( Sorted, DependencyPrettyPrint ).
		

pretty_print_dependencies( [], `` ).
pretty_print_dependencies( [(H, _GovPoS)|T], DependencyPrettyPrint ):-
	index_dependency_lexemes( H, DepType, GovID, RegID, Freq ),
	index_lexeme( GovID, GovString, GovPoS, GovAna, GovFreq ),
	index_lexeme( RegID, RegString, RegPoS, RegAna, RegFreq ),
	( nl,write( GovString ), write( `~I` ), write(DepType), write( `~I` ), write( RegString)  ) ~> HPrettyList,
	pretty_print_dependencies( T, DependencyPretty ),
	( write(HPrettyList), write( DependencyPretty ) )~> DependencyPrettyPrint.


aux_pretty_print_malt_dep_1( H, DepType, GovString, RegString):-
	index_malt_dependency_lexemes( H, DepType, GovID, RegID, _ ),
	index_lexeme( GovID, GovString, _, _, _ ),
	index_lexeme( RegID, RegString, _, _, _ ),!.



















