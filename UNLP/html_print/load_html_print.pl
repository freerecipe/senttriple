% load_html_print
% this predicate loads all relavant predicates to html print
% all of the following processes needs the analysis be done before calling them and they read from memory and write to a html file


load_html_print :-
	nl, write( `load html print files... ` ),
	ensure_loaded( html_pretty_print_all_paragraph ),
	ensure_loaded( html_pretty_print_all_sentence_analysis_results ),
	ensure_loaded( html_pretty_print_comparative_dependencies_columnar ),
	ensure_loaded( html_pretty_print_paragraph_analysis_results ),
	ensure_loaded( html_pretty_print_paragraph_parse_for_each_parser ),
	ensure_loaded( html_print_sentence_biolg_matrix ),
	ensure_loaded( html_print_sentence_chunks ),
	ensure_loaded( html_print_sentence_malt_dependency_matrix ),
	ensure_loaded( html_print_sentence_stanford_dependency_matrix ),
	ensure_loaded( html_print_triples ),
	ensure_loaded( html_to_file_print_of_all_analysis ),
	ensure_loaded( html_to_file_print_of_comparative_malt_stanford_biolg_matrix ),
	ensure_loaded( html_to_file_print_of_comparative_malt_stanford_matrix ),

	ensure_loaded( html_to_file_print_of_comparative_malt_stanford_biolg_matrix_parsi ),	% this predicate and the next two are for Parsi site
	ensure_loaded( html_print_sentence_chunks_parsi ),	%
	ensure_loaded( html_print_triples_parsi ),	%
	ensure_loaded( html_print_to_file_of_all_analysis2 ),

	nl, write( `done.. ` ).


