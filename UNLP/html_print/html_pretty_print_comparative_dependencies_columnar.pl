% This predficate extract all the dependencies from all the available parses (now Malt and Stanford )
% This is based on the position of governer in sentences
html_pretty_print_comparative_dependencies_columnar( SentenceID ):-
%	chdir( Current ),
%	chdir( lexica ),
	cat( ['comp_', SentenceID,'.html'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),
	pretty_print_sentence( SentenceID, LexemeList, Sentence ),
	log_to_window( `start to dump html` ),
	write( `<html>` ),
	write(`<head><title>` ),
	write( ` Sentence Dependency parse `), 
	write(`</title></head>` ),
	write( `<body style="background-color:white;">` ),
	write( `<p style="font-size:12px;font-family:Constantia;color:green">` ),
	write( `<b>Input sentence: </b>` ), write( Sentence),
	write( `</p>` ),
	write( `<table frame="hside" style="font-size:11px;font-family:calibri;color:black" cellspacing="4" cellpadding="4">` ),
	write( `<tr><th>Position</th><th>Lexeme</th><th>Stanford Dependencies</th><th>MaltParser Dependencies</th></tr>` ),
	pretty_write_dependencies_of_sentence( SentenceID, LexemeList ),
	write( `<caption style="font-size:8px;font-family:verdana;color:green">` ), write( `Dependency Parse for the sentence: `), write( SentenceID ), write( `</caption>` ),
	write( `</table>` ),
	write( `</body></html>` ),
	log_to_window( `end dumping html` ),
	fclose( html ).
	



pretty_write_dependencies_of_sentence( SentenceID, [] ):-!.
pretty_write_dependencies_of_sentence( SentenceID, [H|T] ):-
	H = (LexemeID, Position),
	write( H ) ~> Log,
	log_to_window( Log ),
	findall( DepLexIDM, index_sentence_malt_dependencies( SentenceID, DepLexIDM, Position, _ ), MaltDependencyList ),
	findall( DepLexIDS, index_sentence_dependencies( SentenceID, DepLexIDS, Position, _ ), StanfordDependencyList ),
	

	index_lexeme( LexemeID, String, PoS, _, _ ),

	
			% writing table row
			
			write( `<tr>` ),
			write( `<td>` ), write( Position ),write( `</td>` ), 
			write( `<td>` ), write( String - PoS), write( `</td>` ),

			write( `<td>` ),
%			log_to_window( StanfordDependencyList ),
	log_to_window( `start to print stanford` ),
	write( StanfordDependencyList ) ~> Log2, log_to_window( Log2 ),
	pretty_print_single_stanford_dependency(  StanfordDependencyList ),
			write( `</td>` ),	

			write( `<td>` ),
%			log_to_window( MaltDependencyList ),
	write( MaltDependencyList ) ~> Log3, log_to_window( Log3 ),
	log_to_window( `start to print malt` ),
			pretty_print_single_malt_dependency(  MaltDependencyList ), 
			write( `</td>` ),	

			write( `</tr>` ),
	
	pretty_write_dependencies_of_sentence( SentenceID, T ).


	


pretty_print_single_stanford_dependency( [] ):-!.
pretty_print_single_stanford_dependency( [H|T] ):-
	aux_stanford_sql_pred( H, DepType, GovString, RegString ),
	write( `<li>` ),write(DepType), write( `( ` ), write( GovString ), write( `, ` ),  write( RegString), write( ` )` ), write( `</li>` ),
	pretty_print_single_stanford_dependency( T ).

aux_stanford_sql_pred( H, DepType, GovString, RegString ):-
	index_dependency_lexemes( H, DepType, GovID, RegID, _ ),
	index_lexeme( GovID, GovString, _, _, _ ),
	index_lexeme( RegID, RegString, _, _, _ ),!.


pretty_print_single_malt_dependency( [] ):-!.
pretty_print_single_malt_dependency( [H|T] ):-
	aux_malt_sql_pred( H, DepType, GovString, RegString ),
	write( `<li>` ),write(DepType), write( `( ` ), write( GovString ), write( `, ` ),  write( RegString), write( ` )` ), write( `</li>` ),
	pretty_print_single_malt_dependency( T ).

aux_malt_sql_pred( H, DepType, GovString, RegString ):-
	index_malt_dependency_lexemes( H, DepType, GovID, RegID, _ ),
	index_lexeme( GovID, GovString, _, _, _ ),
	index_lexeme( RegID, RegString, _, _, _ ),!.





















