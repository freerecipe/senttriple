% pretty_print_sentence_dependency_matrix(SentenceID ):-
% this predicate prints out html representation of sentence in html format

% modified to be used for Parsi site
html_to_file_print_of_comparative_malt_stanford_biolg_matrix_parsi( SentenceID, FileName ):-
	(write( `generating comparative table for sentence ID: ` ), write( SentenceID ) )~> LogX,
	log_to_window( LogX ),
	cat( ['smb',SentenceID,'matrix.shtml'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),

	index_sentence( SentenceID, _, Length, _ ),
	pretty_print_sentence( SentenceID, _, SentenceString ),
	dynamic( hrow_counter/1 ),
	assert( hrow_counter( 1 ) ),
	
	write( `<!--#include virtual="/parsi/index.html" -->` ), %% this is the header file
	write( `<html>` ),
	write(`<head><title>` ),
	write( `Sentence Dependency/Link parse `), 
	write(`</title></head>` ),
	write( `<body style="background-color:white;">` ),
	write( `<center>` ),
	write( `<p style="font-size:11px;font-family:Constantia;color:black">` ),
	write( `<b>Input sentence: </b>` ), write( SentenceString ),
	write( `</p>` ),
	write( `</center>` ),

	write( `<center>` ),

	write( `<table border="1" style="font-size:9px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	write_position_line_header_for_matrix( Length ),
	write_first_line_header_for_matrix( SentenceID, Length ),
	forall( ( retract( hrow_counter( Row ) ), Row =< Length ),
	(

		dynamic( hcolumn_counter/1 ),
		assert( hcolumn_counter( 1 ) ),
%		print_matrix_header(SentenceID, Row, 1),
		write( `<tr>` ),
%		print_matrix_header(SentenceID, Row, 1),
	
		forall( ( retract( hcolumn_counter(Column) ), Column =< Length ),
			(
				NewColumn is Column + 1,
				assert( hcolumn_counter( NewColumn ) ),

				% write cell
				print_matrix_header(SentenceID, Row, Column),
				pretty_html_give_dep_cell( SentenceID, cell(Row, Column, (DepType, Freq ) )),
				malt_pretty_html_give_dep_cell( SentenceID, cell(Row, Column, (MaltDepType, MaltFreq )) ),
				biolg_pretty_html_give_dep_cell( SentenceID, cell(Row, Column, (BioDepType, BioFreq )) ),

				write( `<td>` ), 
				( DepType =`` -> true; write(`<li type="square">`), write(  DepType ), write( `/` ), write( Freq ),write(`</li>`) ), 
				( MaltDepType = `` -> true; write(`<li type="circle">`), write(  MaltDepType ), write( `/` ), write( MaltFreq ), write(`</li>`) ),
				( BioDepType = `` -> true; write(`<li type="disk">`), write(  BioDepType ), write( `/` ), write( BioFreq ), write(`</li>`) ),

 				write( `</td>` )

			)),
		write( `</tr>` ),
	NewRow is Row + 1,
	assert( hrow_counter( NewRow ) )
	)),

	write( `</table>` ),
	write( `</center>` ),

	write( `<center>` ),
	write( `<p style="font-size:9px;font-family:calibri;color:black;">` ),
	write( `<ul>` ),
		write(`<li style="font-size:9px;font-family:calibri;color:black;" type="square">Stanford Dependencies</li>`), 
		write(`<li style="font-size:9px;font-family:calibri;color:black;" type="circle">MaltParser Dependencies</li>`),
		write(`<li style="font-size:9px;font-family:calibri;color:black;" type="disk">BioLG Links</li>` ),
	write( `</ul>` ),
	write( `</p>` ),
	write( `</center>` ),

	write( `<center>` ),
	
	write( `<p style="font-size:12px;font-family:calibri;color:#660033">` ),
	cat( ['hme',SentenceID,'prdz.shtml'], BackLink, _ ),

	write( `<a href="http://parsi.deri.ie/parsi/prs/`), write(BackLink), write(`"><b>Back</b></a>` ),
	write( `</p>` ),
	write( `</center>` ),


	write( `<html>` ),
	dynamic( hrow_counter/1 ),
	dynamic( hcolumn_counter/1 ),
	fclose( html ),

	!.

