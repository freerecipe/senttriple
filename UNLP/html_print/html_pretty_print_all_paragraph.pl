% html_pretty_print_all_paragraph
% this should print analysis results for all the paragraph
% however the proposed approach might be changed 
% The predicate should call 
html_pretty_print_all_paragraph:-
	cat( ['para_', all,'.html'], FileName, _ ),
	fcreate( parahtml,FileName, -1, 2048, 1 ),
	output( parahtml ),

	write( `<html>` ),
	write(`<head><title>` ),
	write( ` Parse Analysis of Biomed texts `), 
	write(`</title></head>` ),
	write( `<body style="background-color:white;">` ),
	write( `<p style="font-size:14px;font-family:Constantia;color:black">Parse Analysis of Selected Biomedical Abstracts</p>` ),

	forall( index_paragraph( ParagrpahID , _,_,_,_),
	(
	write( `<h4 style="font-size:14px;font-family:Constantia;color:#003300">Paragraph #` ), write( ParagrpahID ), write( `</h4>` ),
	write( `<p style="font-size:11px;font-family:Constantia;color:black">` ),
	forall( SentenceID^index_paragraph_sentence( ParagrpahID, SentenceID, _ ),
			( pretty_print_sentence( SentenceID, _, Sentence ),
				write( `~I` ),  write( Sentence ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#CC9900" href="sd_` ), write( SentenceID ),  write( `matrix.html" target="_blank"> -Stanford- </a>` ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#006600" href="ma_` ), write( SentenceID ),  write( `matrix.html" target="_blank"> -MaltParser-</a>` ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#006600" href="bg_` ), write( SentenceID ),  write( `matrix.html" target="_blank"> -BioLG-</a>` ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#993333" href="smb` ), write( SentenceID ),  write( `matrix.html" target="_blank">-Compare Table-</a>` )

			 )),
	write( `</p>` )
	)),
	write( `</body></html>` ),
	fclose( parahtml ),
	!.  




