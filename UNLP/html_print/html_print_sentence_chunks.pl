% html_print_sentence_chunks( +SentenceID, -FileName )
% this predicate prints previously extracts sentence chunks for a given sentenceID ang provides the file name 

html_print_sentence_chunks( SentenceID, FileName ):-
	cat( ['chnk',SentenceID,'y.html'], FileName, _ ),
	fcreate( chunky,FileName, -1, 2048, 1 ),
	output( chunky ),
	write( `<table border="1" style="font-size:10px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	write( `<tr>` ),
%	write( `<th style="font-size:9px;font-family:calibri;color:#003300">` ), write( `Index List` ), write( `</th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:red">Sentence Chunks</th>` ),
	write( `<th style="font-size:9px;font-family:calibri;color:#003300">` ), write( `Grammatical Category` ), write( `</th>` ),
%	write( `<th style="font-size:9px;font-family:calibri;color:#003300">` ), write( `Position List` ), write( `</th>` ),
	write( `<tr>` ),
	forall( sentence_chunk( SentenceID,CellIndexNumber, Lexemes, PoS, PositionList ),
		( 
		write(`<tr>`), 
%		write(`<td>` ), write( CellIndexNumber ),  write(`</td>` ),  
		write(`<td>` ), write_lexeme_list( Lexemes),  write(`</td>` ),  
		write(`<td>` ),write(PoS),  write(`</td>` ), 
%		write(`<td>` ), write( PositionList ), write(`</td>` ),
		write(`</tr>`) 
		)),
	write( `</table>` ),
	fclose( chunky ).
