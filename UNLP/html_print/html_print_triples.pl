% html_print_triples( +SentenceID, -FileName )
% this predicate print the previously extracted triples for a sentence and as a result generates a html file and give the file name

html_print_triples( SentenceID, FileName ):-
	cat( [trelex, SentenceID, '.html'], FileName, _ ),
	fcreate( out,FileName, -1, 2048, 1 ),
	output( out ),

	write( `<table border="1" style="font-size:9px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	write( `<tr>` ),
	write( `<th style="font-size:8px;font-family:calibri;color:#003300"><b>RuleID</b></th>` ),
	write( `<th style="font-size:8px;font-family:calibri;color:#003300"><b>Subject</b></th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:#003300"><b>Predicate</b></th>` ),
	write( `<th style="font-size:8px;font-family:calibri;color:#003300"><b>Object</b></th>` ),
	write( `<tr>` ),
	forall( etriple(RuleID,A, B, C, D), html_write_tripel_table_row(RuleID, A, B, C, D) ),
	write( `<table>` ),
	fclose( out ).  


% html_write_tripel( +RuleID, +SentenceID, +Subject, +Predicate, +Object )
% this predicate writes a row of an html table consists of RuleID, Subject, Predicate, and Object

html_write_tripel_table_row( RuleID, SentenceID, Subject, Predicate, Object ):-
	write( `<tr>` ),
	write( `<td>` ),
		write( RuleID ),
	write( `</td>` ),
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Subject ), 
	write( `</td>` ),
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Predicate ),
	write( `</td>` ), 
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Object ),
	write( `</td>` ),
	write( `</tr>` ).
