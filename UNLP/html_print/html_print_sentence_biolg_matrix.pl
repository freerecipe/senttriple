% pretty_print_sentence_dependency_matrix(SentenceID ):-
% this predicate prints out html representation of sentence in html format
html_print_sentence_biolg_matrix(SentenceID ):-
	cat( ['big', SentenceID,'matrix.html'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),

	index_sentence( SentenceID, _, Length, _ ),
	pretty_print_sentence( SentenceID, LexemeList, SentenceString ),
	dynamic( hrow_counter/1 ),
	assert( hrow_counter( 1 ) ),

	write( `<html>` ),
	write(`<head><title>` ),
	write( `Sentence Dependency parse (BioLG parser)`), 
	write(`</title></head>` ),
	write( `<body style="background-color:white;">` ),
	write( `<p style="font-size:10px;font-family:Constantia;color:black">` ),
	write( `<b>Input sentence: </b>` ), write( SentenceString ),
	write( `</p>` ),


	write( `<table border="1" style="font-size:9px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	biolg_write_position_line_header_for_matrix( Length ),
	biolg_write_first_line_header_for_matrix( SentenceID, Length ),
	forall( ( retract( hrow_counter( Row ) ), Row =< Length ),
	(

		dynamic( hcolumn_counter/1 ),
		assert( hcolumn_counter( 1 ) ),
%		print_matrix_header(SentenceID, Row, 1),
		write( `<tr>` ),
%		print_matrix_header(SentenceID, Row, 1),
	
		forall( ( retract( hcolumn_counter(Column) ), Column =< Length ),
			(
				NewColumn is Column + 1,
				assert( hcolumn_counter( NewColumn ) ),

				% write cell
				biolg_print_matrix_header(SentenceID, Row, Column),
				biolg_pretty_html_give_dep_cell( SentenceID, cell(Row, Column, (DepType, Freq ) )),
				write( `<td>` ), write(  DepType ), write( `/` ), write( Freq ), write( `</td>` )

			)),
		write( `</tr>` ),
	NewRow is Row + 1,
	assert( hrow_counter( NewRow ) )
	)),

	write( `<table>` ),
	write( `<html>` ),
	dynamic( hrow_counter/1 ),
	dynamic( hcolumn_counter/1 ),
	fclose( html ).


%print_matrix_header(SentenceID, 1 , 1):-
%	get_header( SentenceID, Position, String, PoS, Freq ),
%	write( `<th>` ),  write( String ), write(`/`),write( PoS ), write(`/`),write( Freq ), write( `</th>` ),!.


biolg_print_matrix_header(SentenceID, Position , 1):-
	biolg_get_header( SentenceID, Position, String, PoS, Freq ),
	write( `<th>` ),  write( Position ), write( `</th>` ), 
	write( `<th>` ),  write( String ), write(`/`),write( PoS ), write(`/`),write( Freq ), write( `</th>` ),!.

% otherwise
biolg_print_matrix_header( SenteceID, Row, Col ):- !.
%	Row \= 1,
%	Col \= 1, !.*/
%	!.




biolg_get_header( SentenceID, LexemePosition, String, PoS, Freq ):-
	index_sentence_lexeme( SentenceID, LexemeID, LexemePosition ),
	index_lexeme( LexemeID, String, PoS, _, Freq ).


biolg_write_position_line_header_for_matrix( Length ):-
	write( `<tr>` ),
	write( `<th style="font-size:5px;font-family:calibri;color:#003300">` ), write( `<b>MaltParser Dependencies</b>` ), write( `</th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:red">Regent Position</th>` ),
	dynamic( xc/1 ), 
	assert( xc(1) ),
	forall( ( retract( xc(Count ) ), Count =< Length ), 
		(
		write( `<th>` ), write( Count ), write( `</th>` ),
		NEWCount is Count + 1,
		assert( xc(NEWCount) ) )
		),
	dynamic( xc/1 ),
	write( `</tr>` ).

biolg_write_first_line_header_for_matrix( SentenceID, Length ):-

	write( `<tr>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:red">` ), write( `Govenor Position` ), write( `</th>` ),

	write( `<th style="font-size:10px;font-family:calibri;color:#003300">Word/PoS/Freq</th>` ),

	dynamic( xc/1 ), 
	assert( xc(1) ),

	forall( ( retract( xc(Count ) ), Count =< Length ), 
		(
		biolg_print_first_row__header(SentenceID, Count ),
		NEWCount is Count + 1,
		assert( xc(NEWCount) ) )
		),
	dynamic( xc/1 ),
	write( `</tr>` ).
	
biolg_print_first_row__header(SentenceID, Position ):-
	biolg_get_header( SentenceID, Position, String, _, _ ),
	write( `<th>` ),  write( String ), write( `</th>` ),!.



biolg_pretty_html_give_dep_cell( SentenceID, cell(IndexRow, IndexColumn, (DepType, Freq )) ):-
	( index_sentence_links( SentenceID, DepLexID, IndexRow, IndexColumn) 
	->	index_linked_lexemes( DepLexID, DepType, _, _, Freq );
	DepType = ``,Freq = ``, ! ).

















