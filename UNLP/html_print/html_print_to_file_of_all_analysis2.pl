% html_print_to_file_of_all_analysis( +SentenceID, -FileName )
% this predicate prints out html representation of sentence in html format
% all the analysis should have been done beofre hand
%
html_print_to_file_of_all_analysis2( SentenceID, FileName ):-
%	FileName = 'parsi_results.html'
%	pretty_print_sentence( SentenceID, _, SentenceString ),
	html_to_file_print_of_comparative_malt_stanford_biolg_matrix_parsi( SentenceID, MatrixFileName ),
	html_print_sentence_chunks_parsi( SentenceID, ChunksFileName ),
	html_print_triples_parsi( SentenceID, TripleFileName ),
	pretty_print_sentence( SentenceID, SentenceList, String ),
	cat( ['hme',SentenceID,'prdz.shtml'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),
	write( `<!--#include virtual="/parsi/index.html" -->` ), %% this is the header file
	write( `<html>` ),
	write( `<head><title>` ),
	write( `Analysis Results` ),
	write(`</title></head>` ),
	write( `<div style="margin: 30 70 10 100;font-family:tahoma;size:10pt;color:006633"><p><b>Analysis Results for:</b>` ), 
	write( `</p></div> <div style="margin: 0 70 20 120;font-family:arial;size:10pt;color:black"><p>`),write(String),write(`</p></div>`),
	write( `<div style="margin: 20 250 150 150;font-family:Arial;size:10pt;color:006633"><ul><li>For viewing the extracted <b>triples</b> please click <a href="`),
	write( `http://parsi.deri.ie/parsi/prs/` ), % this should be set as an enviromntal variable
	write( TripleFileName ),
	write(`"><b>here</b></a>.</li>`),
	write( `<li>If you like to see parse results for input sentence please click <a href="`), 
	write( `http://parsi.deri.ie/parsi/prs/` ), % this should be set as an enviromntal variable 
	write( MatrixFileName ), 
	write(`"><b>here</b></a>.</li>`),
	write( `<li>To see the input <b>sentence's chunks</b>click <a href="`),
	write( `http://parsi.deri.ie/parsi/prs/` ), % this should be set as an enviromntal variable
	write( ChunksFileName ), 
	write(`"><b>here</b></a>.</li></div>`),
	write( `</html>` ),
	fclose( html ),!.  



