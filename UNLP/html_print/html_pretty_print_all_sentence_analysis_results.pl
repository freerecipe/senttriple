% this should be removed probably
html_pretty_print_all_sentence_analysis_results(  SentenceID, FileName ):-
	cat( ['alnls', SentenceID,'.html'], FileName, _ ),
	fcreate( index,FileName, -1, 2048, 1 ),
	output( index ),
	write( `<h4 style="font-size:14px;font-family:Constantia;color:#003300">Sentence #` ), write( SentenceID ), write( `</h4>` ),
	write( `<p style="font-size:11px;font-family:Constantia;color:black">` ),
	 pretty_print_sentence( SentenceID, _, Sentence ),
	 html_print_to_file_of_all_analysis( SentenceID, AnalysisFileName ),
	 output( index ),
	 write( Sentence ),
	 write( `  <a style="font-size:8px;font-family:Arial;color:#993333" href="` ), write( AnalysisFileName ),  write( `">-Anlysis-</a>` ),
		
	output( index ),
	write( `</p>` ),

	fclose( index ),
	!.   
