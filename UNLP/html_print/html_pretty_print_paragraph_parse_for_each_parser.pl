% this predicate prints parse analysis results for a paragraph
% this predicate was the first version
html_pretty_print_paragraph_parse_for_each_parser( ParagrpahID, FileName ):-
	cat( ['paraallprs', ParagrpahID,'.html'], FileName, _ ),
	fcreate( parahtml,FileName, -1, 2048, 1 ),
	output( parahtml ),
	write( `<h4 style="font-size:14px;font-family:Constantia;color:#003300">Paragraph #` ), write( ParagrpahID ), write( `</h4>` ),
	write( `<p style="font-size:11px;font-family:Constantia;color:black">` ),
	forall( SentenceID^index_paragraph_sentence( ParagrpahID, SentenceID, _ ),
			( pretty_print_sentence( SentenceID, _, Sentence ),
				write( `~I` ),  write( Sentence ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#CC9900" href="sa_` ), write( SentenceID ),  write( `matrix.html" target="_blank"> -Stanford- </a>` ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#006600" href="ma_` ), write( SentenceID ),  write( `matrix.html" target="_blank"> -MaltParser-</a>` ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#993333" href="comp_` ), write( SentenceID ),  write( `.html" target="_blank">-Compare Table-</a>` )

			 )),
	write( `</p>` ),
	fclose( parahtml ),
	!.  




