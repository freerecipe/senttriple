% html_print_triples( +SentenceID, -FileName )
% this predicate print the previously extracted triples for a sentence and as a result generates a html file and give the file name

html_print_triples_parsi( SentenceID, FileName ):-
	cat( [trelex, SentenceID, '.shtml'], FileName, _ ),
	fcreate( out,FileName, -1, 2048, 1 ),
	output( out ),
	write( `<!--#include virtual="/parsi/index.html" -->` ), %% this is the header file

	pretty_print_sentence( SentenceID, _, SentenceString ),
	write( `<center>` ),
	write( `<p style="font-size:11px;font-family:Constantia;color:black">` ),
	write( `<b>Input sentence: </b>` ), write( SentenceString ),
	write( `</p>` ),
	write( `</center>` ),

	write( `<center>` ),
	write( `<table border="1" style="font-size:9px;font-family:calibri;color:black;border:1px solid black;border-collapse:collapse;">` ),
	write( `<tr>` ),
	write( `<th style="font-size:8px;font-family:calibri;color:#003300"><b>RuleID</b></th>` ),
	write( `<th style="font-size:8px;font-family:calibri;color:#003300"><b>Subject</b></th>` ),
	write( `<th style="font-size:10px;font-family:calibri;color:#003300"><b>Predicate</b></th>` ),
	write( `<th style="font-size:8px;font-family:calibri;color:#003300"><b>Object</b></th>` ),
	write( `<tr>` ),
	forall( etriple(RuleID,A, B, C, D), html_write_tripel_table_row_parsi(RuleID, A, B, C, D) ),
	write( `<table>` ),
	write( `</center>` ),
	cat( ['hme',SentenceID,'prdz.shtml'], BackLink, _ ),
	
	write( `<center>` ),
	write( `<p style="font-size:12px;font-family:calibri;color:#660033">` ),
	write( `<a href="http://parsi.deri.ie/parsi/prs/`), write(BackLink), write(`"><b>Back</b></a>` ),
	write( `</p>` ),
	write( `</center>` ),

	fclose( out ).  


% html_write_tripel( +RuleID, +SentenceID, +Subject, +Predicate, +Object )
% this predicate writes a row of an html table consists of RuleID, Subject, Predicate, and Object

html_write_tripel_table_row_parsi( RuleID, SentenceID, Subject, Predicate, Object ):-
	write( `<tr>` ),
	write( `<td>` ),
		write( RuleID ),
	write( `</td>` ),
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Subject ), 
	write( `</td>` ),
	write( `<td>` ),
		write_lexeme_position_list_separated_by( `^`, SentenceID, Predicate ), 
	write( `</td>` ), 
	write( `<td>` ),
		write_lexeme_position_list( SentenceID, Object ),
	write( `</td>` ),
	write( `</tr>` ). 	

	

