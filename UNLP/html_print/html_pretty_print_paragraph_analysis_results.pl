% html_pretty_print_paragraph_analysis_results( +SentenceID, -FileName )
% this predicate prints all the analysis resutls for a paragraph and generates index file name for that

html_pretty_print_paragraph_analysis_results( ParagrpahID, FileName ):-
	cat( ['pars', ParagrpahID,'.html'], FileName, _ ),
	fcreate( parahtml,FileName, -1, 2048, 1 ),
	output( parahtml ),
	write( `<h4 style="font-size:14px;font-family:Constantia;color:#003300">Paragraph #` ), write( ParagrpahID ), write( `</h4>` ),
	write( `<p style="font-size:11px;font-family:Constantia;color:black">` ),
	forall( SentenceID^index_paragraph_sentence( ParagrpahID, SentenceID, _ ),
			( pretty_print_sentence( SentenceID, _, Sentence ),
			  html_print_to_file_of_all_analysis( SentenceID, AnalysisFileName ),
				output( parahtml ),
				write( Sentence ),
				write( `  <a style="font-size:8px;font-family:Arial;color:#993333" href="` ), write( AnalysisFileName ),  write( `">-Anlysis-</a>` )
			 )),
	output( parahtml ),
	write( `</p>` ),
	fclose( parahtml ),
	!.  




























