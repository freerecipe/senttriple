% html_print_to_file_of_all_analysis( +SentenceID, -FileName )
% this predicate prints out html representation of sentence in html format
% all the analysis should have been done beofre hand
%
html_print_to_file_of_all_analysis( SentenceID, FileName ):-
%	pretty_print_sentence( SentenceID, _, SentenceString ),
	html_to_file_print_of_comparative_malt_stanford_biolg_matrix( SentenceID, MatrixFileName ),
	html_print_sentence_chunks( SentenceID, ChunksFileName ),
	html_print_triples( SentenceID, TripleFileName ),
	cat( ['hme',SentenceID,'prdz.html'], FileName, _ ),
	fcreate( html,FileName, -1, 2048, 1 ),
	output( html ),
%	write( `<html>` ),
%	write(`<head><title>` ),
%	write( ` Sentence Analysis (id:`), write( SentenceID ),write(`)`),
%	write(`</title></head>` ),
	write( `<frameset rows="60%, 20%,20%">` ),
	write( `<frame src="`), write( MatrixFileName ), write( `" />` ),
	write( `<frame src="`), write( ChunksFileName ), write( `" />` ),
	write( `<frame src="`), write( TripleFileName ), write( `" />` ),
	write( `</frameset>` ),
%	write( `</html>` ),
	fclose( html ),!.  



