% write_phrase_from_index( +PhraseID, -PhraseString )
% this predicate generates string for a given phrase id
write_phrase_from_index( PhraseID, PhraseString ):-
	findall( (LexemeID, LexemePosition), index_phrase_lexeme( PhraseID, LexemeID, LexemePosition), LexemeList ),
	sort_ascending_second_member_of_pair(LexemeList ,SortedLexemeList ),
	(
	forall( 
		member( (LexemeID, Position), SortedLexemeList ),
		(
		index_lexeme(LexemeID, LexemeString, _, _,_ ),
		write( LexemeString ),
		write( ` ` ) 
		))
	)~> PhraseString, !.   

  
