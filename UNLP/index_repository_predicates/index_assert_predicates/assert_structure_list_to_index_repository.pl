% assert_token_list_to_index_repository( +TokenList, ?LexemeIDList )
% this predicate assert all the tokens in the list into db and it provide the list of IDs for the asserted or found Tokens
	

% new development
assert_structure_list_to_index_repository( [], [] ).
assert_structure_list_to_index_repository( [H|StructureList], StructureIDList ):-
	(
	H = struct(A,B) -> 
	assert_phrase( PhraseID, PhraseType, PhraseLength, PhrasePosition, struct(A,B) ),
	assert_structure_list_to_index_repository( StructureList, RestStructureIDList ),
	append( [(PhraseID, PhrasePosition)], RestStructureIDList, StructureIDList )
	;
	assert_structure_list_to_index_repository( StructureList, StructureIDList )
	).



/* Just to make sure of member! predicte
	assert_structure_list_to_index_repository( StructureList, StructureIDList ):-
	dynamic( struct_id_asserted/1 ),
	forall( 
		member( struct(A,B),StructureList ),
		( 
		assert_phrase( PhraseID, PhraseType, PhraseLength, PhrasePosition, struct(A,B) ),	% the first three will be returned as the result of process
		assert(struct_id_asserted( (PhraseID, PhrasePosition) ) ) )),
	findall( Memorized, struct_id_asserted(Memorized), StructureIDList ),
	dynamic( struct_id_asserted/1 ).

*/



