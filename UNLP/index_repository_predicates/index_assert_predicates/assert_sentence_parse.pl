% assert_sentence_parse( +SentenceParse, -SentenceID, -StructureIDList, -TokenIDPositionList ,  -DependencyIDs )
% this predicate assert the parse results for a sentence
assert_sentence_parse( SentenceParse, SentenceID, StructureIDList, TokenIDPositionList ,  DependencyIDs ):-
	nl, write( `log: start to extract and assert PENN style results...` ),
	assert_penn_parse_result_for_a_sentence( SentenceParse, SentenceID, StructureIDList, TokenIDPositionList  ),
	nl, write( `log: done, start to extract and assert SD results...` ),
	assert_dependency_parse_result_for_a_sentence( SentenceParse, SentenceID, DependencyIDs ),
	nl, write( `log: done` ), !. 
