% is_sentence_indexed( -SentenceID, +TokenIDPositionList )
% this predicate simmilar to is_phrase_indexed/2 look for wheather the sentence is previously indexed ( it may come from another document )
% You need to think about is_indexed predicateS!!!!

% new implementation 

% is_indexed_sentence( +LookupList, -IndexID )
% this will get a list of TokenID and their position and provides the IndexID if the sentence previously indexed
is_indexed_sentence( LookupList, IndexID ):-
	length( LookupList, Length ),	% Length of Sentence --> How many tokens are in the sentence 
	(
	Length = 1	
		->
		LookupList = [(LexemeID, 1)],
		findall( SentenceID, index_sentence_lexeme( SentenceID,LexemeID, 1), SentenceIDIDList ),
		choose_id_sentence( SentenceIDIDList, 1, FinalID ),
		IndexID = FinalID, nl, write( `Sentence has only one Lexeme, and it is found in index repository` ), !
		;

	LookupList = [(FTokenID, FTokenPos)|RestLookupList],
	reverse( RestLookupList, ReverseLookupList),
	ReverseLookupList = [(LastTokenID, LastTokenPos)|RemainList],

	% findall the IDs for sentences with the lexemes above in the right position
	findall( XSentID, index_sentence_lexeme( XSentID, FTokenID, FTokenPos ), List1 ),
	findall( YSentID, index_sentence_lexeme( YSentID, LastTokenID, LastTokenPos), List2 ),
	intersection( List1, List2, SentencdIDIntersectList ),

	check_index_the_rest_sentence( RemainList, SentencdIDIntersectList, CandidIDList ),
	choose_id_sentence( CandidIDList, Length, IndexID )

	).


% this will do the above process recursively to get to the final intersect list
check_index_the_rest_sentence( [], CandidAnswerList, CandidAnswerList).
check_index_the_rest_sentence( [(LexemeID, LexemePosition )|T], PreviousIntersectionList, IDList ):-
	findall( SentenceID, index_sentence_lexeme( SentenceID, LexemeID, LexemePosition ), SentenceList ),
	intersection( SentenceList, PreviousIntersectionList, NewCandidList ),
	(
	NewCandidList = [] -> nl, write( `log: sentence is new!` ), fail,!;
	check_index_the_rest_sentence( T, NewCandidList, IDList )
	).


% choode_id(sentnece, +CandidIDList,+LengthofSentence, -FinalID)
choode_id_sentence( [],_, _) :- fail.
choose_id_sentence( [H|T],Length, ID ):-
	index_sentence( H, _, Length, _Freq ),
	H = ID,
	!;
	choose_id_sentence( T, Length, ID ).






































