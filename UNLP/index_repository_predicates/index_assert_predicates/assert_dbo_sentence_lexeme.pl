% assert_sentence_lexeme( -SentenceID, +LexemeIDPositionList )
% this predicate gets and list of (LexemeID, Position) and assert the sentence's lexeme in the dbo_sentence_lexeme table



assert_dbo_sentence_lexeme( SentenceID, [] ).
assert_dbo_sentence_lexeme( SentenceID, [(LexemeID, Position)|T] ):-
	db_add_record( dbo_sentence_lexeme, [SentenceID, LexemeID, Position] ),
	assert_dbo_sentence_lexeme( SentenceID, T ), !.

 

