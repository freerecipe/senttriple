% assert_dependency_parse_result_for_a_sentence( +SentenceParse, +SentenceID, -DependencyIDs )
% This predicate extract dependeency parse and assert it into DB, SentenceID and SentencePasrse must be provided to the predicate
assert_dependency_parse_result_for_a_sentence( SentenceParse, SentenceID, DependencyIDs ):-
	get_part_of_parse( typed_dependencies, SentenceParse, Dependencies ),
	( write( `start to assert dependencies for sentence; id:` ), write( SentenceID ) )~> Log,
	log_to_window(Log),

	( write( `dependency list:` ), writeq( Dependencies ) )~> Log2,
	log_to_window(Log2),

	assert_dependencies( Dependencies, SentenceID, DependencyIDs).

% assert_dependencies( +DependenciesList, +SentenceID, -DependencyLexemesIDList ).
% this predicate assert dependencies to the index repository. 
% Two main task are done by this predicate; first it extracts dependencies between lexemes and assert new ones to dbo_dependency_lexemes (or update the frequency)
% and (2) it assert dependencies related to a sentence into dbo_sentence_dependencies

assert_dependencies( [], _, [] ).
assert_dependencies( [dep([(type,DepType)],[governor([(idx,SGovPosition)],_),dependent([(idx,SDependentPosition)],_)])|Rest], SentenceID, [DepIDH| DepIDRest] ):-
	% first get the lexeme ids based on the location of words --> this is because of different senses and make sure that we have chosen the right word
	% please remember we may have to words book one is labled as a verb, the other as a noun so we cannot get to the right lexeme by looking up
	% the word; instead we use the previous Penn Parse Result ( if we do not have the Penn PoS Taggs then it will be problamatic! )
	number_string( GovPosition , SGovPosition ),
	number_string( DependentPosition, SDependentPosition ),
	
	index_sentence_lexeme( SentenceID, GovLexemeID, GovPosition ), % because of the mentioned reason above we use dbo_sentence_lexeme to retrieve the lexeme id
	index_sentence_lexeme( SentenceID, DepLexemeID, DependentPosition), % because of the mentioned reason above we use dbo_sentence_lexeme to retrieve the lexeme id

	( index_dependency_lexemes( DependencyLexemeID, DepType, GovLexemeID, DepLexemeID, DepFreq )  % is dependency defined before?!
		->	% if yes then
			% only update frequency
			NewFreq is DepFreq + 1,
			(
				write( `UPDATE dbo_dependency_lexemes SET dependency_frequency=` ), write(NewFreq), 
				write( ` WHERE dependency_lexemes_id='` ), write( DependencyLexemeID ), write( `'` ) 
			) ~> SQLUpdateQuery, 
			db_sql( SQLUpdateQuery ),
			DepIDH = DependencyLexemeID, 
			assert_dbo_sentence_dependencies( SentenceID, DependencyLexemeID,GovPosition,DependentPosition ),
			assert_dependencies( Rest, SentenceID, DepIDRest), !
			
		;	% otherwise
			% add dependency
		gen_dep_lex_id( DepID ),
		db_add_record( dbo_dependency_lexemes, [DepID, DepType,GovLexemeID, DepLexemeID, 1] ),
		assert_dbo_sentence_dependencies( SentenceID, DepID,GovPosition,DependentPosition ), 
		DepIDH = DepID,
		assert_dependencies( Rest, SentenceID, DepIDRest)
	 ).





gen_dep_lex_id( ID ):-
	hide( Atom, 0 ),
	cat( ['dlt_', Atom], ID, _ ). 

	

assert_dbo_sentence_dependencies( SentenceID, DepID,GovPosition,DependentPosition ):-
	index_sentence_dependencies( SentenceID, DepID,GovPosition,DependentPosition ) -> !;	% this is here to make sure of integrity, the logical way of asserting the dependencies make the presence of senttence dependencies impossible
	db_add_record( dbo_sentence_dependencies, [SentenceID, DepID,GovPosition,DependentPosition] ).






