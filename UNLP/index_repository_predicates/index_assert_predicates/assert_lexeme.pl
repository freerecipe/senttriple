%
% assert_lexeme/4
% assert_lexeme( ?LexemeIDd,+LexemeString,+PennPos,+Ana )
% 	predicate used to assert a lexeme into db.
% 	if the lexeme be found in the repository, 
%	the system shows the available id to lexeme in other case it generates a unique id and assert the lexeme into db
%

assert_lexeme( LexemeID,LexemeString,PennPos,Ana ):-
	( len( LexemeString, 0 ) -> nl, write( `log: error! string of length 0 is not allowed!` ),!
	; 
	index_lexeme( AvailableID,LexemeString,PennPos,Ana, Freq ) 
	->	LexemeID = AvailableID, 
		NewFreq is Freq + 1,
		% creating SQL query to update the table with new frequency
		(
			write( `UPDATE dbo_lexeme SET lexeme_frequency=` ), write(NewFreq), 
			write( ` WHERE lexeme_id='` ), write( AvailableID ), write( `'` ) 
		) ~> SQLUpdateQuery, 
		db_sql( SQLUpdateQuery ),
		! ;
	gen_lexeme_id( LexemeID ),	% auxiliary predicate to generate unique id
	db_add_record( dbo_lexeme, [LexemeID, LexemeString,PennPos,Ana, 1] ) ).


% auxiliary predicate to geenrate unque ids for lexemes
gen_lexeme_id( LexemeID ) :-
	hide( Atom, 0 ),
	cat( ['li_', Atom], LexemeID, _ ).
	
	
	
	