load_index_assert_predicate :-
	nl, write( `log: load asserting predicates...` ),
	ensure_loaded( assert_dbo_sentence_lexeme ),
	ensure_loaded( assert_dbo_sentence_phrase ),
	ensure_loaded( assert_lexeme ),
	ensure_loaded( assert_phrase ),
	ensure_loaded( assert_structure_list_to_index_repository ),
	ensure_loaded( assert_token_list_to_index_repository ),
%	ensure_loaded( complex_index_of_phrase ),
	ensure_loaded( is_phrase_indexed ),
	ensure_loaded( is_indexed_sentence ),
	ensure_loaded( assert_sentence_parse ),
	ensure_loaded( assert_dependency_parse_result_for_a_sentence ),
	ensure_loaded( assert_penn_parse_result_for_a_sentence ),
	ensure_loaded( analyze_and_assert_paragraph ),
	ensure_loaded( is_indexed_paragraph ). 


  


  

