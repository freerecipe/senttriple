% the genral indexing predicate
% this predicate looks if a paragraph, sentence or phrase has been already indexed in the data repository
% this is not an efficient way of implementation, probably far better solution can be find in the papers

%
% LookupList = List of SentenceID + SentencePosition
% IndexID = Paragraph ID with that sentences and positions

is_indexed_paragraph( LookupList, IndexID ):-
	length( LookupList , Length ),	% Length of Paragraph --> How many sentences are in the Paragraph 
	(
	Length = 1	
		->
		LookupList = [(SentenceID, 1)],
		findall( ParagraphID, index_paragraph_sentence( ParagraphID,SentenceID, 1), ParagrpahIDList ),
		choose_id_paragraph( ParagrpahIDList, 1, FinalID ),
		IndexID = FinalID, nl, write( `Pargraph has only one Sentence, and it is found in index repository` ), !
		;

	LookupList = [(FSentenceID, FSentencePos)|RestLookupList],
	reverse( RestLookupList, ReverseLookupList),
	ReverseLookupList = [(LastSentenceID, LastSentencePos)|RemainList],

	% findall the IDs for paragrpahs with the sentences above in the right position
	findall( XParaphID, index_paragraph_sentence( XParaphID, FSentenceID, FSentencePos ), List1 ),
	findall( YParaphID, index_paragraph_sentence( YParaphID, LastSentenceID, LastSentencePos), List2 ),
	intersection( List1, List2, ParagraphIntersectList ),

	check_index_the_rest_paragraph( RemainList, ParagraphIntersectList, CandidIDList ),
	choose_id_paragraph( CandidIDList, Length, IndexID )

	).


% this will do the above process recursively to get to the final intersect list
check_index_the_rest_paragraph( [], CandidAnswerList, CandidAnswerList).
check_index_the_rest_paragraph( [(SentenceID, SentencePosition )|T], PreviousIntersectionList, IDList ):-
	findall( ParagraphID, index_paragraph_sentence( ParagraphID, SentenceID, SentencePosition ), ParphList ),
	intersection( ParphList , PreviousIntersectionList, NewCandidList ),
	(
	NewCandidList = [] -> nl, write( `log: paragraph is new!` ), fail,!;
	check_index_the_rest_paragraph( T, NewCandidList, IDList )
	).


% choode_id(paragraph, +CandidIDList,+LengthofParagraph, -FinalID)
choode_id_paragraph( [],_, _) :- fail.
choose_id_paragraph( [H|T],Length, ID ):-
	index_paragraph( H, _, Length,_WLength,  _Freq ),
	H = ID,
	!;
	choose_id_paragraph( T, Length, ID ).














































