% assert_token_list_to_index_repository( +TokenList, ?LexemeIDList )
% this predicate assert all the tokens in the list into db and it provide the list of IDs for the asserted or found Tokens

	

assert_token_list_to_index_repository( [], [] ).
assert_token_list_to_index_repository( [H|TokenList], LexemeIDPoistionList ):-
	(
	H = token([(pos,PoS),(position,Position)],[Lexme] )
	->	assert_lexeme( LexemeID,Lexme,PoS,`ana-n\a` ),
		number_string( NPosition, Position ), 
		assert_token_list_to_index_repository( TokenList, RestLexemeIDPoistionList ),
		append( [(LexemeID, NPosition)], RestLexemeIDPoistionList , LexemeIDPoistionList ),!;
	assert_token_list_to_index_repository( TokenList, LexemeIDPoistionList )
	).	

/* old development

	assert_token_list_to_index_repository( TokenList, LexemeIDPoistionList ):-
	dynamic( lexeme_id_asserted/1 ),
	forall( 
		member( token([(pos,PoS),(position,Position)],[Lexme] ),TokenList ),
		( assert_lexeme( LexemeID,Lexme,PoS,`ana-n\a` ),
		  number_string( NPosition, Position ), 
		  assert(lexeme_id_asserted((LexemeID,NPosition)) ) )),
	findall( LexemeIDPositionPair, lexeme_id_asserted(LexemeIDPositionPair), LexemeIDPoistionList ),
	dynamic( lexeme_id_asserted/1 ).
*/
