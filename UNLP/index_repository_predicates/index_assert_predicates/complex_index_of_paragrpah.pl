
% compelex_index_of_paragraph( ParagraphID, ParagraphSentenceLength, ParagraphWordLength, ParagraphSentencePositionList )
compelex_index_of_paragraph( ParagraphID, ParagraphSentenceLength, ParagraphWordLength, ParagraphSentencePositionList ) :-
	index_paragraph( ParagraphID, _Excerpt, ParagraphSentenceLength, ParagraphWordLength, _Freq ),
	findall( (SentenceID, SentencePosition), index_paragraph_sentence( ParagraphID,SentenceID,SentencePosition), UnsortedList ),
	sort_ascending_second_member_of_pair(UnsortedList,ParagraphSentencePositionList).

