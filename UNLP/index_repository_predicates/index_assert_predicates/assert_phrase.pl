%
% assert_phrase/4
% assert_lexeme( ?LexemeIDd,+LexemeString,+PennPos,+Ana )
% 	predicate used to assert a lexeme into db.
% 	if the lexeme be found in the repository, 
%	the system shows the available id to lexeme in other case it generates a unique id and assert the lexeme into db
%
% [struct([(type,`NP`)],[token([(pos,`JJ`),(position,0)],[`semi-automatic`]),token([(pos,`NN`),(position,1)],[`photo`]),token([(pos,`NNS`),(position,2)],[`annotation`])])]
/*
<simple_struct type="NP">
  <token pos="JJ" position="3">
    semi-automatic
  </token>
  <token pos="NN" position="4">
    photo
  </token>
  <token pos="NNS" position="5">
    annotation
  </token>
</simple_struct>
*/

% assert_phrase( -PhraseID, -PhraseType, -PhraseLength, +Phrase )
assert_phrase( PhraseID, PhraseType, PhraseLength, PhrasePosition, Phrase ):-
	is_phrase_indexed( Phrase, PhraseID ) 
	->	nl, write( `log: Phrase is already indexed; phrase id is: ` ), write( PhraseID ),

		index_phrase( PhraseID, _Type, _Length, Freq ),	
		NewFreq is Freq + 1,
	
		Phrase = struct( Attrib, Value ),	% extracting the phrase position for the future use
		member( (phrase_position, PhrasePosition ), Attrib ),


		% update the phrase counter
		(
		write( `UPDATE dbo_phrase SET phrase_frequency=` ), write(NewFreq), 
		write( ` WHERE phrase_id='` ), write( PhraseID ), write( `'` ) 
		) ~> SQLUpdateQuery, 
		db_sql( SQLUpdateQuery ),
		! ;
	Phrase = struct([(type,PhraseType), (phrase_position, PhrasePosition )],TokenList ),
	length( TokenList, PhraseLength ),
	gen_phrase_id( PhraseID ),
	db_add_record( dbo_phrase, [PhraseID, PhraseType,PhraseLength, 1] ),
	assert_phrase_lexemes( PhraseID, TokenList ).



gen_phrase_id( PhraseID ) :-
	hide( Atom, 0 ),
	cat( ['ph_', Atom], PhraseID, _ ).


assert_phrase_lexemes( PhraseID, PhraseTokens ):-
	dynamic(token_position_in_phrase/1 ),
	assert( token_position_in_phrase(1) ),
	forall( member( token([(pos,PoS),(position,_SentencePosition)],[Word] ),PhraseTokens), 
		( index_lexeme( LexemeID,Word,PoS,_ana,_freq ),	% this predicate gets ID for the token ( lexeme that has been asserted for this token )	
		retract( token_position_in_phrase(PositionInPhrase) ),
		db_add_record( dbo_phrase_lexeme, [PhraseID, LexemeID,PositionInPhrase] ),
		NextPosition is PositionInPhrase + 1,
		assert( token_position_in_phrase(NextPosition) )
		)),
		dynamic(token_position_in_phrase/1 ).  % this is a modification to memory management
























