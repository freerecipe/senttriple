% this predicate looks if a Phrase indexed before

is_phrase_indexed( PhraseStruct, PhraseID ):-
	PhraseStruct = struct( Attrib ,PhraseTokens ),
	dynamic( position_of_token_in_struct/1 ),
	assert( position_of_token_in_struct( 1 ) ),
	find_id_position_list_for_token_list( PhraseTokens, LexemeIDPositionList ),
	dynamic( position_of_token_in_struct/1 ),

	is_indexed_phrase( LexemeIDPositionList, PhraseID ).
		
find_id_position_list_for_token_list( [], [] ).
find_id_position_list_for_token_list( [token([(pos,TPoS)|_],[String])|PhraseTokens], [(LexemeID,Position ) |LexemeIDPositionList] ):-
	retract( position_of_token_in_struct( Position ) ),
      index_lexeme( LexemeID,String,TPoS,_ana, _freq ),
	NextPos is Position + 1,
	assert( position_of_token_in_struct( NextPos ) ),
%	sort_ascending_second_member_of_pair(UnsorterLexemeIDPositionList ,LexemeIDPositionList ),  %%% Akhtoon! this recursive way of writing make sure that tokens are sorted
	find_id_position_list_for_token_list( PhraseTokens, LexemeIDPositionList).


is_indexed_phrase( LookupList, IndexID ):-
	length( LookupList, Length ),	% Length of Phrase --> How many tokens are in the sentence 
	(
	Length = 1	
		->
		LookupList = [(LexemeID, 1)],
		findall( PhraseID, index_phrase_lexeme( PhraseID,LexemeID, 1), PhraseIDIDList ),
		choose_id_phrase( PhraseIDIDList , 1, FinalID ),
		IndexID = FinalID, 
		log_to_window( `found indexed phrase` ), !
		; fail, ! ),

	LookupList = [(FTokenID, FTokenPos)|RestLookupList],
	reverse( RestLookupList, ReverseLookupList),
	ReverseLookupList = [(LastTokenID, LastTokenPos)|RemainList],

	% findall the IDs for sentences with the lexemes above in the right position
	findall( XPhraseID, index_phrase_lexeme( XPhraseID, FTokenID, FTokenPos ), List1 ),
	findall( YPhraseID, index_phrase_lexeme( YPhraseID, LastTokenID, LastTokenPos), List2 ),
	intersection( List1, List2, PhraseIDIntersectList ),

	check_index_the_rest_phrase( RemainList, PhraseIDIntersectList, CandidIDList ),
	choose_id_phrase( CandidIDList, Length, IndexID )

	.


% this will do the above process recursively to get to the final intersect list
check_index_the_rest_phrase( [], CandidAnswerList, CandidAnswerList).
check_index_the_rest_phrase( [(LexemeID, LexemePosition )|T], PreviousIntersectionList, IDList ):-
	findall( PhraseID, index_phrase_lexeme( PhraseID, LexemeID, LexemePosition ), PhraseIDList ),
	intersection( PhraseIDList , PreviousIntersectionList, NewCandidList ),
	(
	NewCandidList = [] -> log_to_window( `new phrase found` ), fail,!;
	check_index_the_rest_phrase( T, NewCandidList, IDList )
	).


% choode_id(sentnece, +CandidIDList,+LengthofSentence, -FinalID)
choode_id_phrase( [],_, _) :- fail.
choose_id_phrase( [H|T],Length, ID ):-
	index_phrase( H, _, Length, _Freq ),
	H = ID,
	!;
	choose_id_phrase( T, Length, ID ).



