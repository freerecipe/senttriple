complex_index_of_phrase( PhraseID, PhraseType, PhraseLength, PhraseLexemeList ) :-
	index_phrase( PhraseID, PhraseType, PhraseLength, _Freq ),
	findall( (LexemeID, LexemePosition), index_phrase_lexeme( PhraseID,LexemeID, LexemePosition ), UnsortedList ),
	sort_ascending_second_member_of_pair(UnsortedList,PhraseLexemeList).


