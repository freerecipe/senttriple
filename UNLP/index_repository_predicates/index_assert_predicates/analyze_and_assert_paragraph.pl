 

% analyze_and_assert_paragraph( +Paragraph, -ParagraphID )
% this predicate analyze (Paraph2Sent and ParseSent) a paragraph and index it into data repository and give back the assigned paragraph ID
% alternative definition % analyze_paragraph( +Paragr, -ParagraphID, -SentenceIDs, -TokenIDs, -DependencyIDList )
analyze_and_assert_paragraph( Paragraph, ParagraphID ):-
	time( 0, StartTime ),

	trim( Paragraph, TrimmedParagraph ),
	paragraph_to_sentence( TrimmedParagraph, SentenceList ), 

	dynamic( paragraph_sentence_position/1 ),	% setting up a counter for assigning position to sentences in the list
	assert( paragraph_sentence_position( 1 ) ),
	parse_and_assert_sentence_list( SentenceList, SentenceIDPositionList, StructureIDList, TokenIDPositionList, DependencyIDList ),
	dynamic( paragraph_sentence_position/1 ),	% abolishing the counter

	length( SentenceIDPositionList, PSentenceLength ), 
	length( TokenIDPositionList, PWordLength), 

%	nl, write( `paragraph sentence length is ` ), write( PSentenceLength  ),
%	nl, write( `paragraph Word length is ` ), write( PWordLength ),

	log_to_window( `request for analyzing the paragraph` ),
	(
	is_indexed_paragraph( SentenceIDPositionList, ParagraphID) 
		-> 	( write( `The Paragraph has been already indexed in the system: ` ), write( ParagraphID ) )~> Log, log_to_window( Log),
			index_paragraph( ParagraphID, _, _, _, PFreq ),
			NewFreq is PFreq + 1, 
			(
			write( `UPDATE dbo_paragraph SET paragraph_frequency=` ), write(NewFreq), 
			write( ` WHERE paragraph_id='` ), write( ParagraphID ), write( `'` ) 
			) ~> SQLUpdateQuery, 
			db_sql( SQLUpdateQuery ), !

			% updating the frequency
			% still need to something like updating the frequency of the paragraph

	;
	gen_paragraph_id( NewParagraphID ), % I may need to see if the Paragraph is asserted before or not.
	db_add_record( dbo_paragraph, [NewParagraphID, `excerpt to appear here!`, PSentenceLength , PWordLength, 1] ),
	assert_dbo_paragraph_sentence( NewParagraphID, SentenceIDPositionList ),
	ParagraphID = NewParagraphID,
	log_to_window( `new paragraph paragraph was added to index repository` ),
	!
	),
	time( 0, EndTime ),
	StartTime = ( _Day1, T1 ),
	EndTime = ( _Day2, T2 ),
	Duration is T2 - T1,
	Seconds is Duration / 1000,
	 
	( write( `process and assertion of paragraph ID/` ), write(ParagraphID ), write( ` done in `), write( Seconds ), write( ` seconds...` ) ) ~> Flog,
	log_to_window( Flog ).


% parse_and_assert_sentence_list( +SentenceList, -SentenceIDPositionList, -StructureIDLists, -TokenIDPositionLists ,  -DependencyIDLists )
parse_and_assert_sentence_list( [],[] ,[], [] , []).
% parse_and_assert_sentence_list( [Sentence|Rest], SentList [(SentenceID, SentencePosition )|SIRest], StructList [StructureIDList|SRest], TokenList, DependencyList[DependencyIDs|DRest]) :-  	

parse_and_assert_sentence_list( [Sentence|Rest], SentList, StructList , TokenList, DependencyList) :-  	
	retract( paragraph_sentence_position( SentencePosition ) ),

	% log
	( write( `analyzing sentence #` ),write( SentencePosition ) ) ~> Text1, 
	log_to_window( Text1 ),

	NewSentPosition is SentencePosition  + 1,
	assert( paragraph_sentence_position( NewSentPosition ) ),
	trim( Sentence, TrimmedSentence ),

	log_to_window( `requesting parse for sentence` ),

	parse( TrimmedSentence, `stanford_parser`, SentenceParse ),

	log_to_window( `requesting for asserting parse results` ),
	assert_sentence_parse( SentenceParse, SentenceID, StructureIDList, TokenIDPositionList, DependencyIDs ),

	log_to_window( `requesting biolg parse for sentence` ),
	biolg_parse( SentenceID ),

	log_to_window( `requesting malt parse for sentence` ),
	malt_parse( SentenceID ),
	parse_and_assert_sentence_list( Rest, SIRest, StructRest, TRest, DepRest),

%	nl, write( `log: GOING FOR APPENDING SENTENCE PARSE RESULTS!` ),
	append([(SentenceID, SentencePosition )],SIRest, SentList ),
	append( StructureIDList, StructRest, StructList ),
	append( TokenIDPositionList, TRest,TokenList ),
	append( DependencyIDs,DepRest,DependencyList ).



gen_paragraph_id( ParagraphID ):-
	hide( Atom, 0 ),
	cat( ['pid_', Atom], ParagraphID, _ ).




% assert_dbo_paragraph_sentence( +NewParagraphID, +SentenceIDPositionList )
% this paragprah add list of sentence and position list to dbo_paragraph_sentence table 

assert_dbo_paragraph_sentence( NewParagraphID, [] ).
assert_dbo_paragraph_sentence( NewParagraphID, [(SentenceID, Position)|RestList] ):-
	(
	index_paragraph_sentence( NewParagraphID, SentenceID, Position ) 
	-> 	nl, write( `log: !! there should be some error somewhere! either the unique key generated is false or someother errors like complex indexing` ),
		assert_dbo_paragraph_sentence( NewParagraphID, RestList )
	;
	db_add_record( dbo_paragraph_sentence, [NewParagraphID, SentenceID, Position] ),
	assert_dbo_paragraph_sentence( NewParagraphID, RestList )
	).
	
	
	






































































