% assert_dbo_sentence_phrase( +SentenceID, +StructureIDList )
% this predicate gets a structlist and id and updates the relavant table, called dbo_sentence_phrase

assert_dbo_sentence_phrase( SentenceID, [] ).
assert_dbo_sentence_phrase( SentenceID, [(PhraseID, PhrasePosition)|T] ):-
	db_add_record( dbo_sentence_phrase, [SentenceID, PhraseID, PhrasePosition] ),
	assert_dbo_sentence_phrase( SentenceID, T ).
	
	


