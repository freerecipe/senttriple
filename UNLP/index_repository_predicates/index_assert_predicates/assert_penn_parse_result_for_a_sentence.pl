
% assert_penn_parse_result_for_a_sentence( +SentenceParse, -SentenceID,- StructureIDList, -TokenIDPositionList  )
% This predicate extract penn structures from a parse result, and assert that into index_repository
% and provide SentenceID, StructureIDlist and TokenIDList as outputs


assert_penn_parse_result_for_a_sentence( SentenceParse, SentenceID, StructureIDList, TokenIDPositionList  ) :-

	get_part_of_parse( penn_style, SentenceParse, PennParse ), 
	tokenize_penn_style_string( PennParse, PennParseTokenized),  
	look_for_penn_token( PennParseTokenized, TokenList ),
	assert_token_list_to_index_repository( TokenList, TokenIDPositionList ),
	look_for_simple_penn_struct( TokenList, StructtList ),
	assert_structure_list_to_index_repository( StructtList, StructureIDList ),
%	nl, write( `Token Position list:` ), write( TokenIDPositionList ),
%	nl, write( `Phrase Position list:` ), write( StructureIDList ),

	(
	is_indexed_sentence( TokenIDPositionList, SentenceID ) ->
		index_sentence( SentenceID, _Excerpt, _Length, Frequency ),
		NewFreq is Frequency + 1,
		(
			write( `UPDATE dbo_sentence SET frequency=` ), write(NewFreq), 
			write( ` WHERE sentence_id='` ), write( SentenceID ), write( `'` ) 
		) ~> SQLUpdateQuery, 
		db_sql( SQLUpdateQuery ),
		! ;
	generate_sentence_id( SentenceID ),
	length( TokenIDPositionList, SentenceLength ),
	db_add_record( dbo_sentence, [SentenceID, `still_not_provided`, SentenceLength, 1] ),
	assert_dbo_sentence_lexeme( SentenceID, TokenIDPositionList ),
	assert_dbo_sentence_phrase( SentenceID, StructureIDList )
	)
	
	.



%	assert_dbo_sentence_lexeme( SentenceID, TokenIDPositionList ),
%	assert_dbo_sentence_phrase( SentenceID, StructureIDList ).

%	dynamic( simple_struct/1 ),
%	forall( member( struct(A,B), StructtList ),
%		( assert( simple_struct( simple_struct(A,B)) ) )),
%	findall( Structures, simple_struct( Structures ), SimpleStructList ),
%	dynamic( simple_struct/1 ).
%	nl, write( StructtList ).
%	xml_write( 0, [s(A, B)]).

% is_sentence_indexed( SentenceID, TokenIDPositionList ):-
	

% generate_sentence_id( SentenceID )
% auxiliary predicate to generate ID for sentence
% may need to be replaced

generate_sentence_id( SentenceID ):-
	hide( Atom, 0 ),
	cat( ['se_le', Atom], SentenceID , _ ).





















