load_index_repository_predicates :-
	nl, write( `log: loading index repository predicates...`  ),

	chdir( Current ),
	chdir( 'index_assert_predicates' ),
	ensure_loaded( load_index_assert_predicates ),
	load_index_assert_predicate,
	
	chdir( Current ),
	chdir( index_retrieve_predicates ),
	ensure_loaded( load_index_retrieve_predicates ),
	load_index_retrieve_predicates,
	
	chdir( Current ).
