start_parsi:-
	ensure_loaded( full_analysis_for_setence ),
	chdir( 'UNLP' ),
	ensure_loaded( load_sub_systems ),
	ensure_loaded( init_system ),
	load_system( `Parsi` ),
	init_parsi.
