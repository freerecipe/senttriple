A small win-prolog system that extract a set of triples in the form of <Arg1, Relation, Arg2>.
To do so, the system consolidates syntactic information from several dependency parses and uses a set of patterns and the unification process.
Besides these codes, you need to run these parsers as TCP/IP-based agaents (See the config etc)